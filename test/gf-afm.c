#define _GF_PFB_C_

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <ctype.h>
#include "gf-afm.h"

guchar * gf_afm_strdup (const guchar * token);
const guchar * gf_afm_search (const guchar * buf, gint length, const guchar * text);
const guchar * gf_afm_lower_search (const guchar * buf, gint length, const guchar * text);

GFAFM *
gf_afm_open (const gchar * name)
{
	gint fh;
	struct stat s;
	guchar * buf;
	gint length;
	const guchar * fontname, * fullname, * familyname, * version;
	GFAFM * afm;

	g_return_val_if_fail (name != NULL, NULL);

	if (stat (name, &s) != 0) return NULL;

	fh = open (name, O_RDONLY);
	if (fh < 0) return NULL;

	buf = mmap (NULL, s.st_size, PROT_READ, MAP_SHARED, fh, 0);

	close (fh);

	if (buf == (gpointer) -1) return NULL;

	length = s.st_size;

	/* Well, we probably mess with some bytes */

	if (strncmp (buf, "StartFontMetrics", 16) != 0) {
		munmap (buf, s.st_size);
		return NULL;
	}

	fontname = gf_afm_search (buf, length, "FontName");
	fullname = gf_afm_search (buf, length, "FullName");
	familyname = gf_afm_search (buf, length, "FamilyName");
	version = gf_afm_lower_search (buf, length, "version");

	if ((!fontname) || (!fullname) || (!familyname)) {
		munmap (buf, s.st_size);
		return NULL;
	}

	afm = g_new0 (GFAFM, 1);

	afm->filename = g_strdup (name);

	afm->fontname = gf_afm_strdup (fontname);
	afm->fullname = gf_afm_strdup (fullname);
	afm->familyname = gf_afm_strdup (familyname);
	afm->version = gf_afm_strdup (version);

	munmap (buf, s.st_size);

	if ((!afm->fontname) || (!afm->fullname)) {
		gf_afm_close (afm);
		return NULL;
	}

	return afm;
}

void
gf_afm_close (GFAFM * afm)
{
	g_return_if_fail (afm != NULL);

	if (afm->filename) g_free (afm->filename);
	if (afm->fontname) g_free (afm->fontname);
	if (afm->fullname) g_free (afm->fullname);
	if (afm->familyname) g_free (afm->familyname);
	if (afm->version) g_free (afm->version);
	g_free (afm);
}

guchar *
gf_afm_strdup (const guchar * token)
{
	const guchar * p, * q;
	guchar * str;

	/* Fixme: This crashes for malformed files */

	p = token;

	while (*p > ' ') p++;
	if ((*p < ' ') && (*p != '\t')) return NULL;
	while ((*p == ' ') || (*p == '\t')) p++;
	if (*p <= ' ') return NULL;

	q = p;
	while (*q >= ' ') q++;

	str = g_new0 (guchar, q - p + 1);
	memcpy (str, p, q - p);

	return str;
}
	
const guchar *
gf_afm_search (const guchar * buf, gint length, const guchar * text)
{
	gint textlen;
	const guchar * p;
	gint i;
	
	textlen = strlen (text);

	p = buf;

	while (p < buf + length) {
		while ((*p >= ' ') && (p < buf + length)) p++;
		while ((*p <= ' ') && (p < buf + length)) p++;
		if (p >= buf + length) return NULL;

		if (*p == *text) {
			for (i = 1; i < textlen; i++) {
				if (p[i] != text[i]) break;
			}
			if (i == textlen) return p;
		}
	}

	return NULL;
}

const guchar *
gf_afm_lower_search (const guchar * buf, gint length, const guchar * text) {
	gint textlen;
	const guchar * p;
	gint i;
	
	textlen = strlen (text);

	p = buf;

	while (p < buf + length) {
		while ((*p >= ' ') && (p < buf + length)) p++;
		while ((*p <= ' ') && (p < buf + length)) p++;
		if (p >= buf + length) return NULL;

		if (tolower (*p) == *text) {
			for (i = 1; i < textlen; i++) {
				if (tolower (p[i]) != text[i]) break;
			}
			if (i == textlen) return p;
		}
	}

	return NULL;
}

