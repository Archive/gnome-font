#define _GF_PFB_C_

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <ctype.h>
#include "gf-pfb.h"

guchar * gf_pfb_strdup (const guchar * token);
guchar * gf_pfb_namedup (const guchar * token);
const guchar * gf_pfb_search (const guchar * buf, gint length, const guchar * text);
const guchar * gf_pfb_lower_search (const guchar * buf, gint length, const guchar * text);

GFPFB *
gf_pfb_open (const gchar * name)
{
	gint fh;
	struct stat s;
	guchar * buf;
	gint length;
	const guchar * fontname, * fullname, * version;
	GFPFB * pfb;

	g_return_val_if_fail (name != NULL, NULL);

	if (stat (name, &s) != 0) return NULL;

	fh = open (name, O_RDONLY);
	if (fh < 0) return NULL;

	buf = mmap (NULL, s.st_size, PROT_READ, MAP_SHARED, fh, 0);

	close (fh);

	if (buf == (gpointer) -1) return NULL;

	if ((buf[0] != 0x80) && (buf[1] != 0x01)) {
		munmap (buf, s.st_size);
		return NULL;
	}

	length = 0x100 * buf[3] + buf[2];

	/* Well, we probably mess with some bytes */

	if (strncmp (buf + 6, "%!PS-AdobeFont-1.0:", 19) != 0) {
		munmap (buf, s.st_size);
		return NULL;
	}

	fontname = gf_pfb_search (buf + 6, length, "/FontName");
	fullname = gf_pfb_search (buf + 6, length, "/FullName");
	version = gf_pfb_lower_search (buf + 6, length, "/version");

	if ((!fontname) || (!fullname)) {
		munmap (buf, s.st_size);
		return NULL;
	}

	pfb = g_new0 (GFPFB, 1);

	pfb->filename = g_strdup (name);

	pfb->fontname = gf_pfb_namedup (fontname);
	pfb->fullname = gf_pfb_strdup (fullname);
	pfb->version = gf_pfb_strdup (version);

	munmap (buf, s.st_size);

	if ((!pfb->fontname) || (!pfb->fullname)) {
		gf_pfb_close (pfb);
		return NULL;
	}

	return pfb;
}

void
gf_pfb_close (GFPFB * pfb)
{
	g_return_if_fail (pfb != NULL);

	if (pfb->filename) g_free (pfb->filename);
	if (pfb->fontname) g_free (pfb->fontname);
	if (pfb->fullname) g_free (pfb->fullname);
	if (pfb->version) g_free (pfb->version);
	g_free (pfb);
}

guchar *
gf_pfb_strdup (const guchar * token)
{
	guchar * p, * q;
	guchar * str;

	/* Fixme: This crashes for malformed pfb files */

	p = strchr (token, '(') + 1;
	q = strchr (p, ')');

	str = g_new0 (guchar, q - p + 1);
	memcpy (str, p, q - p);

	return str;
}
	
guchar *
gf_pfb_namedup (const guchar * token)
{
	guchar * p, * q;
	guchar * str;

	/* Fixme: This crashes for malformed pfb files */

	p = strchr (token, '/') + 1;
	q = p;
	while (*q > ' ') q++;

	str = g_new0 (guchar, q - p + 1);
	memcpy (str, p, q - p);

	return str;
}
	
const guchar *
gf_pfb_search (const guchar * buf, gint length, const guchar * text)
{
	gint textlen;
	const guchar * p;
	gint i;
	
	textlen = strlen (text);

	for (p = buf; p <= buf + length - textlen; p++) {
		if (*p == *text) {
			for (i = 1; i < textlen; i++) {
				if (p[i] != text[i]) break;
			}
			if (i == textlen) return p;
		}
	}

	return NULL;
}

const guchar *
gf_pfb_lower_search (const guchar * buf, gint length, const guchar * text) {
	gint textlen;
	const guchar * p;
	gint i;
	
	textlen = strlen (text);

	for (p = buf; p <= buf + length - textlen; p++) {
		if (tolower (*p) == *text) {
			for (i = 1; i < textlen; i++) {
				if (tolower (p[i]) != text[i]) break;
			}
			if (i == textlen) return p;
		}
	}

	return NULL;
}
