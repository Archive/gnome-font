#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <glib.h>
#include <gtk/gtkobject.h>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-util.h>
#include <libgnomefont/gnome-font.h>
#include <libgnomefont/ft2/gnome-font-face-ft2.h>
#include "gf-pfb.h"
#include "gf-afm.h"
#include "gf-fontmap.h"

#define GF_VERBOSE

typedef enum {
	GF_FILE_UNKNOWN,
	GF_FILE_TT,
	GF_FILE_PFB,
	GF_FILE_AFM
} GFFileType;

typedef struct {
	gchar * name;
	time_t mtime;
	size_t size;
	GFFileType type;
	guint modified : 1;
	gchar * fullname;
	gchar * version;
} GFFileData;

typedef struct {
	GFFileData * pfb_data;
	GFFileData * afm_data;
} GFType1Data;

typedef struct {
	GFFileData * tt_data;
} GFTTData;

typedef enum {
	GF_FONT_UNKNOWN,
	GF_FONT_TRUETYPE,
	GF_FONT_TYPE1
} GFFontType;

typedef struct {
	GFFontType type;
	gchar * ps_name;
	union {
		GFType1Data t1;
		GFTTData tt;
	} filedata;
} GFFontData;

static GFFileData * test_file (const gchar * name, GHashTable * files);
static GFFileType determine_file_type (const gchar * name, gchar ** fullname, gchar ** version);
static gboolean register_file (gpointer key, gpointer value, gpointer data);
static gboolean build_font (gpointer key, gpointer value, gpointer data);
static void build_type1 (GFFileData * pfb, GFFileData * afm, GSList ** fonts);
static void build_truetype (GFFileData * tt, GSList ** fonts);
static void gf_file_data_free (GFFileData * fd);
static void check_fontmap (GnomeFontMap * fm);
static void map_font (GnomeFontMap * fm, GFFontData * fd);
static void map_type1 (GnomeFontMap * fm, GFFontData * fd);
static void map_truetype (GnomeFontMap * fm, GFFontData * fd);

static GHashTable * fontname2filelist = NULL;

int main (int argc, char ** argv)
{
	GnomeFontMap * fontmap;
	GHashTable * file2data;
	DIR * dir;
	struct dirent * dent;
	gint i;
	GSList * fonts;

	if (argc < 2) {
		g_print ("usage: test-ttload font_directory...\n");
		exit (0);
	}

	gtk_type_init ();

	/* Load existing fontmap(s) */
	fontmap = gf_fontmap_load ();

#ifdef GF_VERBOSE
	g_print ("Loaded fontmap of %d\n", fontmap->num_fonts);
#endif

	/* Check integrity of all fontmap records */
	check_fontmap (fontmap);

#ifdef GF_VERBOSE
	g_print ("%d fonts are good\n", fontmap->num_fonts);
#endif

	/* fixme: during font building we should really check map too */

	/* Create file dictionary */
	file2data = g_hash_table_new (g_str_hash, g_str_equal);

	for (i = 1; i < argc; i++) {
		dir = opendir (argv[i]);
		if (dir) {
			while ((dent = readdir (dir))) {
				gchar * filename;
				filename = g_concat_dir_and_file (argv[i], dent->d_name);
				test_file (filename, file2data);
				g_free (filename);
			}
		} else {
			g_print ("Invalid directory: %s\n", argv[i]);
		}
	}

	/* Well - we have collected all meaningful files */

	fontname2filelist = g_hash_table_new (g_str_hash, g_str_equal);
	g_hash_table_foreach_remove (file2data, register_file, fontname2filelist);
	g_hash_table_destroy (file2data);

	/* Well - we have grouped files by font name */

	fonts = NULL;
	g_hash_table_foreach_remove (fontname2filelist, build_font, &fonts);
	g_hash_table_destroy (fontname2filelist);

	/* Well - fonts is now fresh list of GFFontData's */

	while (fonts) {
		GFFontData * fd;

		fd = (GFFontData *) fonts->data;

		/* fixme: We have to update fontdict */
		map_font (fontmap, fd);

		fonts = g_slist_remove (fonts, fd);
	}

	xmlSaveFile ("map", fontmap->doc);

	return 0;
}

/*
 * Test, whether file is registered, modified and is it
 * valid font file at all
 *
 * files is dictionary of registered files
 * - we test, whether file is modified
 * No - it is kept, as it was
 * Yes - it is removed from dictionary
 */

static GFFileData *
test_file (const gchar * name, GHashTable * files)
{
	struct stat s;
	GFFileData * filedata;
	GFFileType type;
	gchar * fullname, * version;

	g_print ("File: %s\n", name);

	if (stat (name, &s) != 0) {
		g_print ("Cannot stat file\n");
		return NULL;
	}

	if (!S_ISREG (s.st_mode)) {
		g_print ("Not a regular file\n");
		return NULL;
	}

	filedata = g_hash_table_lookup (files, name);

	if (filedata) {
		g_print ("Already registered - checking...");
		if ((s.st_size == filedata->size) && (s.st_mtime == filedata->mtime)) {
			g_print (" SAME\n");
			filedata->modified = FALSE;
			return filedata;
		}
		g_print (" MODIFIED\n");

		/* Forget entry */
		g_hash_table_remove (files, name);
		g_free (filedata->name);
		g_free (filedata->fullname);
		if (filedata->version) g_free (filedata->version);
		g_free (filedata);
	}

	type = determine_file_type (name, &fullname, &version);

	switch (type) {
	case GF_FILE_TT:
	case GF_FILE_PFB:
	case GF_FILE_AFM:
		g_print ("%s is valid font file :)\n", name);
		filedata = g_new (GFFileData, 1);
		filedata->name = g_strdup (name);
		filedata->mtime = s.st_mtime;
		filedata->size = s.st_size;
		filedata->type = type;
		filedata->modified = TRUE;
		filedata->fullname = fullname;
		filedata->version = version;
		g_hash_table_insert (files, filedata->name, filedata);
		break;
	case GF_FILE_UNKNOWN:
		g_print ("Is not a recognized font type\n");
		break;
	default:
		g_assert_not_reached ();
		break;
	}

	return filedata;
}

GFFileType
determine_file_type (const gchar * name, gchar ** fullname, gchar ** version)
{
	GnomeFontFace * face;
	GFFileType type;
	gint len;

	type = GF_FILE_UNKNOWN;

	len = strlen (name);

	if (strncmp (name + len - 4, ".pfb", 4) == 0) {
		GFPFB * pfb;
		pfb = gf_pfb_open (name);
		if (pfb) {
			/* Try, if face is loadable */
			face = gnome_font_face_ft2_new_from_file (name, 0);
			if (face) {
				gnome_font_face_unref (face);
				*fullname = g_strdup (pfb->fullname);
				*version = (pfb->version) ? g_strdup (pfb->version) : g_strdup ("0.0");
				type = GF_FILE_PFB;
			}
			gf_pfb_close (pfb);
			return type;
		}
	}

	if (strncmp (name + len - 4, ".afm", 4) == 0) {
		GFAFM * afm;
		afm = gf_afm_open (name);
		if (afm) {
			*fullname = g_strdup (afm->fullname);
			*version = (afm->version) ? g_strdup (afm->version) : g_strdup ("0.0");
			gf_afm_close (afm);
			return GF_FILE_AFM;
		}
	}

	/* Try to load it with FreeType loader */

	face = gnome_font_face_ft2_new_from_file (name, 0);

	if (face) {
		/* Good - we are recognized file type */
		if (FT_IS_SFNT (GNOME_FONT_FACE_FT2 (face)->ft_face)) {
			/* Better yet - we are 'sfnt' */
			*fullname = g_strdup (gnome_font_face_get_name (face));
			/* fixme: */
			*version = g_strdup ("0.0");
			type = GF_FILE_TT;
		} else {
			GFPFB * pfb;
			/* Not 'sfnt' - we have to test Type1 manually */
			pfb = gf_pfb_open (name);
			if (pfb) {
				*fullname = g_strdup (pfb->fullname);
				*version = (pfb->version) ? g_strdup (pfb->version) : g_strdup ("0.0");
				gf_pfb_close (pfb);
				type = GF_FILE_PFB;
			}
		}
		gnome_font_face_unref (face);
	} else {
		GFAFM * afm;
		afm = gf_afm_open (name);
		if (afm) {
			*fullname = g_strdup (afm->fullname);
			*version = (afm->version) ? g_strdup (afm->version) : g_strdup ("0.0");
			gf_afm_close (afm);
			type = GF_FILE_AFM;
		}
	}

	return type;
}

static gboolean
register_file (gpointer key, gpointer value, gpointer data)
{
	GFFileData * fd;
	GHashTable * fonts;
	GSList * fl, * nl;

	fd = (GFFileData *) value;
	fonts = (GHashTable *) data;

	g_print ("registering %s\n", fd->fullname);

	fl = g_hash_table_lookup (fonts, fd->fullname);
	/* We use dumb append here hoping, that there are few files for every font */
	nl = g_slist_append (fl, fd);
	if (fl != nl) {
		g_hash_table_insert (fontname2filelist, fd->fullname, nl);
	}

	return TRUE;
}

static gboolean
build_font (gpointer key, gpointer value, gpointer data)
{
	GFFileData * tt_data = NULL;
	GFFileData * pfb_data = NULL;
	GFFileData * afm_data = NULL;
	gdouble tt_v, pfb_v, afm_v;
	GSList * files;

	files = (GSList *) value;
	tt_v = pfb_v = afm_v = -10.0;

	g_print ("building font %s\n", (gchar *) key);

	while (files) {
		GSList * l;
		gdouble max;
		GFFileData * maxfd;

		max = -10.0;
		maxfd = NULL;
		for (l = files; l != NULL; l = l->next) {
			GFFileData * fd;
			gdouble v;
			/* fixme: locale! */
			fd = (GFFileData *) l->data;
			v = atof (fd->version);
			if (v > max) {
				max = v;
				maxfd = fd;
			}
		}

		g_print ("Max: %s %f\n", maxfd->name, max);

		/* fixme: We get confused for naegative numbers */
		files = g_slist_remove (files, maxfd);

		switch (maxfd->type) {
		case GF_FILE_TT:
			if (!tt_data) {
				tt_data = maxfd;
				tt_v = max;
			} else {
				/* We have already >= version of the same font */
				gf_file_data_free (maxfd);
			}
			break;
		case GF_FILE_PFB:
			if (pfb_data) {
				if (pfb_v == max) {
					/* We have exactly the same version */
					gf_file_data_free (maxfd);
					break;
				}
				if (!afm_data) {
					/* There is no afm for saved pfb */
					gf_file_data_free (pfb_data);
					pfb_data = maxfd;
					pfb_v = max;
					break;
				} else {
					/* There is aready pfb and afm with >= version */
					gf_file_data_free (maxfd);
					break;
				}
			} else {
				/* We do not have pfb yet */
				if (afm_data) {
					if (afm_v > max) {
						/* Saved afm is higher than we */
						gf_file_data_free (afm_data);
						afm_data = NULL;
					}
					pfb_data = maxfd;
					pfb_v = max;
					break;
				} else {
					pfb_data = maxfd;
					pfb_v = max;
					break;
				}
			}
			break;
		case GF_FILE_AFM:
			if (afm_data) {
				if (afm_v == max) {
					/* We have exactly the same version */
					gf_file_data_free (maxfd);
					break;
				}
				if (!pfb_data) {
					/* There is no pfb for saved afm */
					gf_file_data_free (afm_data);
					afm_data = maxfd;
					afm_v = max;
					break;
				} else {
					/* There are aready pfb and afm with >= version */
					gf_file_data_free (maxfd);
					break;
				}
			} else {
				/* We do not have afm yet */
				if (pfb_data) {
					if (pfb_v > max) {
						/* Saved pfb is higher than we */
						gf_file_data_free (pfb_data);
						pfb_data = NULL;
					}
					afm_data = maxfd;
					afm_v = max;
					break;
				} else {
					afm_data = maxfd;
					afm_v = max;
					break;
				}
			}
			break;
		default:
			g_assert_not_reached ();
			break;
		}
	}

	/* Here we are, having maximum reasonable versioned files */

	if ((pfb_data) && (afm_data)) {
		/* We can go with Type1 */
		if (tt_data) gf_file_data_free (tt_data);
		g_print ("%s [%s] is Type1\n", pfb_data->fullname, pfb_data->version);
		build_type1 (pfb_data, afm_data, (GSList **) data);
	} else {
		if (pfb_data) gf_file_data_free (pfb_data);
		if (afm_data) gf_file_data_free (afm_data);
		if (tt_data) {
			/* We are TrueType */
			g_print ("%s [%s] is TrueType\n", tt_data->fullname, tt_data->version);
			build_truetype (tt_data, (GSList **) data);
		} else {
			g_print ("No valid combination of font files\n");
			/* We do not exist :( */
			/* This can happen - for example for single afm files */
		}
	}

	return TRUE;
}

static void
build_type1 (GFFileData * pfb, GFFileData * afm, GSList ** fonts)
{
	GFFontData * fd;

	fd = g_new (GFFontData, 1);

	fd->type = GF_FONT_TYPE1;
	fd->ps_name = g_strdup_printf ("Test[%s]", pfb->fullname);
	fd->filedata.t1.pfb_data = pfb;
	fd->filedata.t1.afm_data = afm;

	*fonts = g_slist_prepend (*fonts, fd);
}

static void
build_truetype (GFFileData * tt, GSList ** fonts)
{
	GFFontData * fd;

	fd = g_new (GFFontData, 1);

	fd->type = GF_FONT_TRUETYPE;
	fd->ps_name = g_strdup_printf ("Test[%s]", tt->fullname);
	fd->filedata.tt.tt_data = tt;

	*fonts = g_slist_prepend (*fonts, fd);
}

static void
gf_file_data_free (GFFileData * fd)
{
	g_print ("Dropping %s [%s]\n", fd->fullname, fd->version);
	g_free (fd->name);
	g_free (fd->fullname);
	if (fd->version) g_free (fd->version);
	g_free (fd);
}

static void
check_fontmap (GnomeFontMap * fm)
{
	/* Nothing at moment */
}

static void
map_font (GnomeFontMap * fm, GFFontData * fd)
{
	switch (fd->type) {
	case GF_FONT_TYPE1:
		map_type1 (fm, fd);
		break;
	case GF_FONT_TRUETYPE:
		map_truetype (fm, fd);
		break;
	default:
		g_assert_not_reached ();
		break;
	}
}

static void
map_type1 (GnomeFontMap * fm, GFFontData * fd)
{
	GnomeFontMapEntry * e;
	GFFileData * filedata;

	/* We'll use afm as reference */

	filedata = fd->filedata.t1.afm_data;

	e = g_hash_table_lookup (fm->fontdict, filedata->fullname);

	if (e) {
		/* fixme: versioning stuff */
		g_print ("Font %s is already in map\n", filedata->fullname);
	} else {
		xmlNodePtr n, f;
		gchar c[128];
		GFAFM * afm;

		n = xmlNewDocNode (fm->doc, NULL, "font", NULL);
		xmlAddChild (fm->map, n);

		/* afm file */
		f = xmlNewDocNode (fm->doc, NULL, "file", NULL);
		xmlAddChild (n, f);
		xmlSetProp (f, "type", "afm");
		xmlSetProp (f, "path", fd->filedata.t1.afm_data->name);
		g_snprintf (c, 128, "%d", (gint) fd->filedata.t1.afm_data->size);
		xmlSetProp (f, "size", c);
		g_snprintf (c, 128, "%d", (gint) fd->filedata.t1.afm_data->mtime);
		xmlSetProp (f, "mtime", c);

		/* pfb file */
		f = xmlNewDocNode (fm->doc, NULL, "file", NULL);
		xmlAddChild (n, f);
		xmlSetProp (f, "type", "pfb");
		xmlSetProp (f, "path", fd->filedata.t1.pfb_data->name);
		g_snprintf (c, 128, "%d", (gint) fd->filedata.t1.pfb_data->size);
		xmlSetProp (f, "size", c);
		g_snprintf (c, 128, "%d", (gint) fd->filedata.t1.pfb_data->mtime);
		xmlSetProp (f, "mtime", c);

		xmlSetProp (n, "format", "type1");
		xmlSetProp (n, "name", filedata->fullname);
		xmlSetProp (n, "version", filedata->version);

		afm = gf_afm_open (filedata->name);
		xmlSetProp (n, "familyname", afm->familyname);
		xmlSetProp (n, "psname", afm->fontname);
		gf_afm_close (afm);
	}

	gf_file_data_free (fd->filedata.t1.afm_data);
	gf_file_data_free (fd->filedata.t1.pfb_data);

	/* fixme: remove this */
	g_free (fd->ps_name);
	g_free (fd);
}

static void
map_truetype (GnomeFontMap * fm, GFFontData * fd)
{
	GnomeFontMapEntry * e;
	GFFileData * filedata;

	/* We'll use afm as reference */

	filedata = fd->filedata.tt.tt_data;

	e = g_hash_table_lookup (fm->fontdict, filedata->fullname);

	if (e) {
		/* fixme: versioning stuff */
		g_print ("Font %s is already in map\n", filedata->fullname);
	} else {
		xmlNodePtr n, f;
		gchar c[128];
		GnomeFontFace * face;

		n = xmlNewDocNode (fm->doc, NULL, "font", NULL);
		xmlAddChild (fm->map, n);

		/* font file */
		f = xmlNewDocNode (fm->doc, NULL, "file", NULL);
		xmlAddChild (n, f);
		xmlSetProp (f, "type", "ttf");
		xmlSetProp (f, "path", fd->filedata.tt.tt_data->name);
		g_snprintf (c, 128, "%d", (gint) fd->filedata.tt.tt_data->size);
		xmlSetProp (f, "size", c);
		g_snprintf (c, 128, "%d", (gint) fd->filedata.tt.tt_data->mtime);
		xmlSetProp (f, "mtime", c);

		xmlSetProp (n, "format", "truetype");
		xmlSetProp (n, "name", filedata->fullname);
		xmlSetProp (n, "version", filedata->version);

		face = gnome_font_face_ft2_new_from_file (filedata->name, 0);
		xmlSetProp (n, "familyname", gnome_font_face_get_family_name (face));
		xmlSetProp (n, "psname", gnome_font_face_get_ps_name (face));
		gnome_font_face_unref (face);
	}

	gf_file_data_free (fd->filedata.tt.tt_data);

	/* fixme: remove this */
	g_free (fd->ps_name);
	g_free (fd);
}
