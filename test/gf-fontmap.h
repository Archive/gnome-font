#ifndef _GF_FONTMAP_H_
#define _GF_FONTMAP_H_

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

typedef struct _GnomeFontMap GnomeFontMap;
typedef struct _GnomeFontMapEntry GnomeFontMapEntry;

#include <glib.h>
#include <gnome-xml/tree.h>

struct _GnomeFontMap {
	gint refcount;
	gint num_fonts;
	GHashTable * fontdict;

	xmlDoc * doc;
	xmlNode * map;
};

struct _GnomeFontMapEntry {
	xmlChar * name;
	xmlChar * type;
	xmlChar * version;

	xmlNode * node;
};

GnomeFontMap * gf_fontmap_load (void);

void gf_fontmap_ref (GnomeFontMap * fontmap);
void gf_fontmap_unref (GnomeFontMap * fontmap);

END_GNOME_DECLS

#endif
