#include <locale.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <libgnomeui/gnome-canvas.h>
#if 0
#include <libgnomeprint/gnome-canvas-bpath.h>
#endif
#if 0
#include <libgnomeprint/gp-path.h>
#endif
#include <libgnomefont/gnome-font.h>
#include <libgnomefont/gnome-font-load.h>

static GnomeCanvasGroup *
create_canvas (void)
{
	GtkWidget * w, * sw, * c;

	w = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_signal_connect (GTK_OBJECT (w), "delete_event",
		GTK_SIGNAL_FUNC (gtk_main_quit), NULL);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_push_colormap (gdk_rgb_get_cmap ());
	gtk_widget_push_visual (gdk_rgb_get_visual ());
	c = gnome_canvas_new_aa ();
	gtk_widget_pop_visual ();
	gtk_widget_pop_colormap ();
	gnome_canvas_set_scroll_region (GNOME_CANVAS (c), 0, 0, 1024, 1024);
	gtk_container_add (GTK_CONTAINER (sw), c);
	gtk_container_add (GTK_CONTAINER (w), sw);
	gtk_widget_show_all (w);

	return gnome_canvas_root (GNOME_CANVAS (c));
}

#if 0
static GnomeCanvasItem *
create_bpath_item (GnomeFontGlyph * glyph, GnomeCanvasGroup * group)
{
	const ArtBpath * bpath;
	GPPath * path;
	GnomeCanvasItem * item;

	bpath = gnome_font_glyph_get_bpath (glyph);
	g_return_val_if_fail (bpath != NULL, NULL);

	path = gp_path_new_from_foreign_bpath ((ArtBpath *) bpath);

	item = gnome_canvas_item_new (group,
		GNOME_TYPE_CANVAS_BPATH,
		"bpath", path,
		"fill_color_rgba", 0xff0000ff,
		NULL);

	gp_path_unref (path);

	return item;
}
#endif


static GnomeCanvasItem *
create_pixbuf_item (GnomeFontGlyph * glyph, GnomeCanvasGroup * group)
{
	const GFGGrayMap * gmap;
	GdkPixbuf * pb;
	GnomeCanvasItem * item;
	gint x, y;

	gmap = gnome_font_glyph_get_graymap (glyph);
	g_return_val_if_fail (gmap != NULL, NULL);

	g_print ("-----glyph-----\n");
	for (y = 0; y < gmap->height; y++) {
	for (x = 0; x < gmap->width; x++) {
		g_print ("%x", gmap->pixels[y * gmap->rowstride + x]);
	}
	g_print ("\n");
	}

#if 0
	path = gp_path_new_from_foreign_bpath (bpath);

	item = gnome_canvas_item_new (group,
		GNOME_TYPE_CANVAS_BPATH,
		"bpath", path,
		"fill_color_rgba", 0xff0000ff,
		NULL);

	gp_path_unref (path);

	return item;
#else
	return NULL;
#endif
}


int main (int argc, char ** argv)
{
	GnomeFontFace * face;
	GnomeFont * font;
	GnomeRFont * rfont;
	GnomeFontGlyph * glyph;
	gdouble affine[6];
	gint x, y, c;
	GnomeCanvasGroup * group;
	GnomeCanvasItem * item;

	if (argc < 2) {
		g_print ("usage: test-ttdraw fontfile\n");
		exit (0);
	}

	gtk_init (&argc, &argv);

	face = (GnomeFontFace *) gnome_font_face_new_from_file (argv[1]);

	if (face == NULL) {
		g_print ("%s is not TrueType font file :(\n", argv[1]);
		exit (0);
	}

	g_print ("Font file: %s\n", argv[1]);

	if (!gnome_font_face_set_encoding (face, 0, NULL, "iso-8859-1")) {
		g_print ("Cannot display iso-8859-1 encoding\n");
		exit (0);
	}

	font = gnome_font_face_get_font (face, 36.0, 96.0, 96.0);

	art_affine_identity (affine);

	rfont = gnome_font_get_rfont (font, affine);

	group = create_canvas ();
	art_affine_scale (affine, 1.0, -1.0);

	for (y = 0; y < 16; y++) {
		for (x = 0; x < 16; x++) {
			c = gnome_font_face_lookup (face, 0, y * 16 + x);
			g_print ("trying %d\n", c);
			glyph = gnome_vfont_get_glyph (GNOME_VFONT (rfont), c);
#if 0
			item = create_bpath_item (glyph, group);
#else
			item = create_pixbuf_item (glyph, group);
#endif
			if (item != NULL) {
				affine[4] = x * 64.0 + 16.0;
				affine[5] = 1024 - 16.0 - y * 64.0;
				gnome_canvas_item_affine_absolute (item, affine);
			} else {
				g_print ("item is NULL\n");
			}
#if 0
			/* Glyph unref-ing is not yet implemented */
			gnome_font_glyph_unref (glyph);
#endif
		}
	}

	gtk_main ();

	return 0;
}


