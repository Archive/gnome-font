#define USE_C

/*
 * Test program to model semi real use of gnome-font library
 *
 */

#include <config.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <locale.h>
#include <libgnomeui/gnome-canvas.h>
#if 0
#include <libgnomeprint/gnome-canvas-bpath.h>
#include <libgnomeprint/gp-path.h>
#endif
#include <libgnomefont/gnome-font-private.h>
#include <libgnomefont/gnome-font-load.h>

#include "gnome-canvas-ttext.h"
#include "glyphlist.h"

#define F_SIZE 12.0

static void print_advances (GnomeFont * font, gchar * text);
static void show_text (GnomeCanvasGroup * group, GFGlyphList * text, gdouble x, gdouble y, gdouble scale, guint32 rgba);

static GnomeCanvasGroup *
create_canvas (gdouble width, gdouble height)
{
	GtkWidget * w, * sw, * c;

	w = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_widget_set_usize (w, 256, 256);
	gtk_signal_connect (GTK_OBJECT (w), "delete_event",
		GTK_SIGNAL_FUNC (gtk_main_quit), NULL);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_push_colormap (gdk_rgb_get_cmap ());
	gtk_widget_push_visual (gdk_rgb_get_visual ());
	c = gnome_canvas_new_aa ();
	gtk_widget_pop_visual ();
	gtk_widget_pop_colormap ();
	gnome_canvas_set_scroll_region (GNOME_CANVAS (c), 0, 0, width, height);
	gtk_container_add (GTK_CONTAINER (sw), c);
	gtk_container_add (GTK_CONTAINER (w), sw);
	gtk_widget_show_all (w);

	return gnome_canvas_root (GNOME_CANVAS (c));
}

int main (int argc, char ** argv)
{
	GnomeFontFace * face;
	GnomeFont * font72, * font150, * font300;
	GnomeCanvasGroup * group;
	GFGlyphList * l72, * l150, * l300;
	struct stat sbuf;
	int fd;
	guchar * b;

	if (argc < 3) {
		g_print ("usage: use fontfile textfile\n");
		exit (0);
	}

	gtk_init (&argc, &argv);
#if 1
	gf_freetype2_init ();
#endif

	/*
	 * Here should be some high-level font browsing, or simply
	 * accessing font face, given its canonical name
	 * Current function is shortcut to loading face from file,
	 * will be private and probably will go away
	 */

	face = (GnomeFontFace *) gnome_font_face_new_from_file (argv[1]);

	if (face == NULL) {
		g_print ("%s is not valid font file :(\n", argv[1]);
		exit (0);
	} else {
		g_print ("Font file: %s\n", argv[1]);
	}

	/*
	 * OK. Now we have resolution independent typeface handle,
	 * GnomeFontFace. Lets see, what we can do with it...
	 */

	/*
	 * This should be (re)moved. Hopefully we can stick with unicode
	 * encoding for glyphs + additional glyphs in private space.
	 * Still - maybe some recoding is preferred to load language
	 * specific unicode glyphs to places they belong
	 */

	if (!gnome_font_face_set_encoding (face, 0, NULL, "iso-8859-1")) {
		g_print ("Cannot display iso-8859-1 encoding\n");
		exit (0);
	}

	/*
	 * OK. Note that face is completely device independent beast.
	 * For it we can get GnomeFont - an object implementing specific
	 * pointsize on specific output device with certain resolution
	 * (we call that "master resolution")
	 * Please note, that if your final design will be printed, you only
	 * get maximum quality, if you request GnomeFont for intended
	 * printer resolution. On-screen previews can then be slightly
	 * distorted, but that's life. Still - fonts are vectorial, so the
	 * glyph shapes are quite good, but interglyph distances can be
	 * slightly distorted for other devices than master output.
	 */

	font72 = gnome_font_face_get_font (face, F_SIZE, 72.0, 72.0);
	font150 = gnome_font_face_get_font (face, F_SIZE, 1200.0, 1200.0);
	font300 = gnome_font_face_get_font (face, F_SIZE, 300.0, 300.0);

	/*
	 * Given certain GnomeFont, we have everything needed, to calculate
	 * exact layout of text (but we cannot yet output anything, at least
	 * not easily)
	 */

	/*
	 * Now we generate device independent glyphlists from input text
	 * In future that will be handled by specific toolkits, like Pango,
	 * current implementation is dumb and can do only unkerned latin
	 * text
	 */

	if (stat (argv[2], &sbuf)) exit (1);

	fd = open (argv[2], O_RDONLY);
	if (fd < 0) exit (1);

	b = mmap (NULL, sbuf.st_size, PROT_READ, MAP_SHARED, fd, 0);

	if (!b) exit (1);

	close (fd);


	l72 = gfgl_text_to_list_simple_sized (font72, b, sbuf.st_size);
#if 0
	l150 = gfgl_text_to_list_simple_sized (font150, b, sbuf.st_size);
	l300 = gfgl_text_to_list_simple (font300, argv[2]);

#if 0
	gfgl_list_describe (l72);
	art_affine_identity (affine);
	l = gfgl_list_to_pglyphs (l, affine);
	gfgl_pglyphs_describe (l);
#endif

	/* Create Canvas */
#endif
	group = create_canvas (2048, 8192);

	show_text (group, l72, 100.0, 100.0, 1.0, 0x0000ffff);
#if 0
	show_text (group, l150, 100.0, 100.0, 4.0, 0x0000007f);
	show_text (group, l300, 100.0, 100.0 + 2 * F_SIZE, 1.0);
#endif
	gtk_main ();

	return 0;
}

/*
 * Here we print GnomeFont and GnomeFontFace glyph advances,
 * to demonstrate, how font master resolution affects these
 */

static void
print_advances (GnomeFont * font, gchar * text)
{
	GnomeFontFace * face;
	GnomeFontGlyph * glyph;
	gdouble size;
	ArtPoint afont, aface;
	gchar * c;
	gint code;

	/*
	 * Every font has face and size attributes
	 */

	face = font->face;
	size = font->size;

	for (c = text; *c != '\0'; c++) {
		/*
		 * First thing to do, is to get glyph index from given
		 * character code. This is dependent of encoding, and
		 * can be done by powerful, multilanguage toolkits,
		 * like Pango. Still, at moment we rely on our
		 * iso-8859-1 encoding (which fortunately is has the same
		 * beginning as Unicode. Encoding id 0 is simply our handle,
		 * we used during encoding creation
		 */

		code = gnome_font_face_lookup (face, 0, *c);

		/*
		 * Code is simple integer and is guaranteed to be
		 * meaningful. For illegal characters it is usually
		 * 0 - empty glyph
		 */

		/*
		 * Now, given glyph code, we can get handle to specific
		 * GnomeFontGlyph object.
		 * Note that both GnomeFontFace and GnomeFont implement
		 * get_glyph method. Resulting glyphs have same interfaces,
		 * but have slightly different meanings. If doing device
		 * adjusted layout, we need glyph from GnomeFont, whose
		 * metrics are adjusted to master resolution.
		 * Glyph is actually requested through face and font
		 * common ancestor - GnomeVFont - interface
		 */

		glyph = gnome_vfont_get_glyph (GNOME_VFONT (font), code);

		/*
		 * Now we request horizontal advance distance
		 * For GnomeFont glyphs this is grid-fitted in base coords
		 * NB! The API is experimental and probably will change
		 */

		gnome_font_glyph_get_stdadvance (glyph, &afont);

		/*
		 * Now we get face glyph and corresponding advance
		 * For GnomeFontFace glyphs this is given in 1000 unit
		 * em square coordinates
		 */

		glyph = gnome_vfont_get_glyph (GNOME_VFONT (face), code);
		gnome_font_glyph_get_stdadvance (glyph, &aface);

		g_print ("Char %c font advance %f face advance %f\n", *c, afont.x, aface.x * size / 1000.0);
	}
}

static void
show_text (GnomeCanvasGroup * group, GFGlyphList * text, gdouble x, gdouble y, gdouble scale, guint32 rgba)
{
	GnomeCanvasItem * item;
	gdouble affine[6];

	item = gnome_canvas_item_new (group,
		gnome_canvas_ttext_get_type (),
		"text", text,
		"rgba", rgba,
		NULL);

	art_affine_scale (affine, scale, scale);
	affine[4] = x;
	affine[5] = y;
	gnome_canvas_item_affine_absolute (item, affine);
}

