#define _GF_FONTMAP_C_

#include <sys/stat.h>
#include <malloc.h>
#include <string.h>
#include <gnome-xml/parser.h>
#include <gnome-xml/xmlmemory.h>
#include "gf-fontmap.h"

#define GF_FONTMAP_FILE "/usr/share/fonts/fontmap"

static void gf_fontmap_translate_1_0 (GnomeFontMap * fm, xmlNodePtr map);
static void gf_fontmap_translate_1_0_node (GnomeFontMap * fm, xmlNodePtr node, GSList ** aliases);
static void gf_fontmap_write_file_data (xmlNodePtr n, const gchar * type, const gchar * path);
static void gf_fontmap_build_dict (GnomeFontMap * fm);

static GnomeFontMap * gf_create_empty_fontmap (gboolean xml);
static gboolean gf_fontmap_destroy_dict_entry (gpointer key, gpointer value, gpointer data);

GnomeFontMap *
gf_fontmap_load (void)
{
	xmlDocPtr doc;
	xmlNodePtr map;
	xmlChar * version;

	doc = xmlParseFile (GF_FONTMAP_FILE);
	if (!doc) {
		return gf_create_empty_fontmap (TRUE);
	}

	map = xmlDocGetRootElement (doc);
	if (!map) {
		xmlFreeDoc (doc);
		return gf_create_empty_fontmap (TRUE);
	}

	if (strcmp (map->name, "fontmap")) {
		xmlUnlinkNode (map);
		xmlFreeNode (map);
		xmlFreeDoc (doc);
		return gf_create_empty_fontmap (TRUE);
	}

	version = xmlGetProp (map, "version");
	if (!version) {
		GnomeFontMap * fm;
		/* We are 1.0 */
		xmlFree (version);
		fm = gf_create_empty_fontmap (TRUE);
		gf_fontmap_translate_1_0 (fm, map);
		xmlFreeDoc (doc);
		/* fixme: */
		gf_fontmap_build_dict (fm);
		return fm;
	} else if (!strcmp (version, "2.0")) {
		GnomeFontMap * fm;
		/* We are 2.0 */
		xmlFree (version);
		fm = gf_create_empty_fontmap (FALSE);
		fm->doc = doc;
		fm->map = map;
		gf_fontmap_build_dict (fm);
		return fm;
	} else {
		/* Versions > 2.0 not supported */
		xmlFree (version);
		xmlFreeDoc (doc);
		return gf_create_empty_fontmap (TRUE);
	}

	return NULL;
}

void
gf_fontmap_ref (GnomeFontMap * fontmap)
{
	g_return_if_fail (fontmap != NULL);

	fontmap->refcount++;
}

void
gf_fontmap_unref (GnomeFontMap * fontmap)
{
	g_return_if_fail (fontmap != NULL);

	if (--fontmap->refcount < 1) {
		g_hash_table_foreach_remove (fontmap->fontdict, gf_fontmap_destroy_dict_entry, fontmap);
		g_hash_table_destroy (fontmap->fontdict);
		xmlFreeDoc (fontmap->doc);
		g_free (fontmap);
	}
}

/*
 * NB! This assumes, that fontmap has empty 2.0 xml tree
 */

static void
gf_fontmap_translate_1_0 (GnomeFontMap * fm, xmlNodePtr map)
{
	xmlNodePtr child;
	GSList * aliases;

	child = map->xmlChildrenNode;
	aliases = NULL;

	while (child) {
		if (!strcmp (child->name, "font")) {
			gf_fontmap_translate_1_0_node (fm, child, &aliases);
		}
		child = child->next;
	}
}

/*
 * Bad-bad - we have to collect aliases into separate dict to do ps->canonical mapping
 * fixme:
 */

static void
gf_fontmap_translate_1_0_node (GnomeFontMap * fm, xmlNodePtr node, GSList ** aliases)
{
	xmlNodePtr n;
	xmlChar * p;

	p = xmlGetProp (node, "format");
	if (strcmp (p, "type1")) {
		xmlFree (p);
		return;
	}

	/* Type 1 OK */
	xmlFree (p);

	n = xmlNewDocNode (fm->doc, NULL, "font", NULL);
	xmlAddChild (fm->map, n);

	p = xmlGetProp (node, "alias");
	if (p) {
		/* We are alias */
		xmlSetProp (n, "format", "alias");
		xmlSetProp (n, "ref", p);
		xmlFree (p);
	} else {
		/* Just plain type1 */
		xmlSetProp (n, "format", "type1");

		p = xmlGetProp (node, "metrics");
		gf_fontmap_write_file_data (n, "afm", p);
		xmlFree (p);

		p = xmlGetProp (node, "glyphs");
		gf_fontmap_write_file_data (n, "pfb", p);
		xmlFree (p);
	}

	p = xmlGetProp (node, "fullname");
	xmlSetProp (n, "name", p);
	xmlFree (p);

	p = xmlGetProp (node, "version");
	xmlSetProp (n, "version", p);
	xmlFree (p);

	p = xmlGetProp (node, "familyname");
	xmlSetProp (n, "familyname", p);
	xmlFree (p);

	p = xmlGetProp (node, "name");
	xmlSetProp (n, "psname", p);
	xmlFree (p);
}

static void
gf_fontmap_write_file_data (xmlNodePtr n, const gchar * type, const gchar * path)
{
	struct stat s;
	xmlNodePtr f;
	guchar c[128];

	if (stat (path, &s) != 0) {
		return;
	}

	if (!S_ISREG (s.st_mode)) {
		return;
	}

	f = xmlNewDocNode (n->doc, NULL, "file", NULL);
	xmlAddChild (n, f);
	xmlSetProp (f, "type", type);
	xmlSetProp (f, "path", path);
	g_snprintf (c, 128, "%d", (gint) s.st_size);
	xmlSetProp (f, "size", c);
	g_snprintf (c, 128, "%d", (gint) s.st_mtime);
	xmlSetProp (f, "mtime", c);
}

static void
gf_fontmap_build_dict (GnomeFontMap * fm)
{
	xmlNodePtr child;

	child = fm->map->xmlChildrenNode;

	while (child) {
		GnomeFontMapEntry * e;
		e = g_new (GnomeFontMapEntry, 1);
		e->name = xmlGetProp (child, "name");
		e->type = xmlGetProp (child, "type");
		e->version = xmlGetProp (child, "version");
		e->node = child;
		/* fixme: */
		g_hash_table_insert (fm->fontdict, e->name, e);
		child = child->next;
	}
}

static GnomeFontMap *
gf_create_empty_fontmap (gboolean xml)
{
	GnomeFontMap * fm;

	fm = g_new (GnomeFontMap, 1);

	fm->refcount = 1;
	fm->num_fonts = 0;
	fm->fontdict = g_hash_table_new (g_str_hash, g_str_equal);

	if (xml) {
		fm->doc = xmlNewDoc ("1.0");
		fm->map = xmlNewDocNode (fm->doc, NULL, "fontmap", NULL);
		xmlDocSetRootElement (fm->doc, fm->map);
		xmlSetProp (fm->map, "version", "2.0");
	} else {
		fm->doc = NULL;
		fm->map = NULL;
	}

	return fm;
}

static gboolean
gf_fontmap_destroy_dict_entry (gpointer key, gpointer value, gpointer data)
{
	GnomeFontMap * fm;
	GnomeFontMapEntry * fme;

	fm = (GnomeFontMap *) data;
	fme = (GnomeFontMapEntry *) value;

	xmlFree (fme->name); /* Also clears key */
	xmlFree (fme->type);
	xmlFree (fme->version);

	/* fixme: We probably do not need that */
	xmlUnlinkNode (fme->node);
	xmlFreeNode (fme->node);

	g_free (fme);

	fm->num_fonts--;

	return TRUE;
}


