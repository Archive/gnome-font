#define GLYPHLIST_C

#include <libart_lgpl/art_affine.h>
#include <libgnomefont/gnome-font-private.h>
#include "glyphlist.h"

static GSList *
gfgl_append_font (GSList * l, GnomeFont * font)
{
	GFGLElement * el;
	GSList * n;

	el = g_new (GFGLElement, 1);

	el->code = GFGL_FONT;
	el->value.font = font;

	n = g_slist_append (l, el);

	return l ? l->next : n;
}

static GSList *
gfgl_append_double (GSList * l, GFGLCode code, gdouble value)
{
	GFGLElement * el;
	GSList * n;

	el = g_new (GFGLElement, 1);

	el->code = code;
	el->value.dval = value;

	n = g_slist_append (l, el);

	return l ? l->next : n;
}

static GSList *
gfgl_append_int (GSList * l, GFGLCode code, gint value)
{
	GFGLElement * el;
	GSList * n;

	el = g_new (GFGLElement, 1);

	el->code = code;
	el->value.ival = value;

	n = g_slist_append (l, el);

	return l ? l->next : n;
}

static GSList *
gfgl_append_code (GSList * l, GFGLCode code)
{
	GFGLElement * el;
	GSList * n;

	el = g_new (GFGLElement, 1);

	el->code = code;

	n = g_slist_append (l, el);

	return l ? l->next : n;
}

GFGlyphList *
gfgl_text_to_list_simple_sized (GnomeFont * font, const gchar * text, gint length)
{
	GSList * l, * last;
	gint gcode;
	gboolean lastcode;
	gdouble px, py, sx, sy;
	unsigned char * c;
	gint i;
g_print ("generating glyphlist... ");
	l = last = gfgl_append_font (NULL, font);

	lastcode = FALSE;
	px = py = sx = sy = 0.0;

	for (i = 0; i < length; i++) {
		GnomeFontGlyph * glyph, * lglyph;
		ArtPoint a;

		c = (unsigned char *) (text + i);
		gcode = gnome_font_face_lookup (font->face, 0, *c);
		glyph = gnome_vfont_get_glyph ((GnomeVFont *) font, gcode);
		if (*c > ' ') {
			if (!lastcode) {
				last = gfgl_append_double (last, GFGL_X, px);
				last = gfgl_append_double (last, GFGL_Y, py);
			} else {
				last = gfgl_append_code (last, GFGL_ADVANCE);
				last = gfgl_append_int (last, GFGL_KERNING, gcode);
				gnome_font_glyph_get_kerning (lglyph, glyph, &a);
				px += a.x;
				py += a.y;
			}
			last = gfgl_append_int (last, GFGL_GLYPH, gcode);
			lastcode = TRUE;
			lglyph = glyph;
			if (gnome_font_glyph_get_stdadvance (glyph, &a)) {
				/* fixme: this is crap */
				px += a.x;
				py += a.y;
			}
		} else {
			lastcode = FALSE;
			if (*c < ' ') {
				sy += font->size;
				px = sx;
				py = sy;
			} else {
				if (gnome_font_glyph_get_stdadvance (glyph, &a)) {
					/* fixme: this is crap */
					px += a.x;
					py += a.y;
				}
			}
		}
	}
g_print ("done\n");
	return l;
}

GFGlyphList *
gfgl_text_to_list_simple (GnomeFont * font, const gchar * text)
{
	gint len;

	for (len = 0; * (text + len) != '\0'; len++);

	return gfgl_text_to_list_simple_sized (font, text, len);
}

void
gfgl_free (GFGlyphList * list)
{
	while (list) {
		g_free (list->data);
		list = g_slist_remove_link (list, list);
	}
}

GFGlyphList *
gfgl_duplicate (GFGlyphList * list)
{
	GFGLElement * el;
	GSList * new, * l;

	new = NULL;

	for (l = list; l != NULL; l = l->next) {
		el = g_new (GFGLElement, 1);
		memcpy (el, l->data, sizeof (GFGLElement));
		new = g_slist_prepend (new, el);
	}

	return g_slist_reverse (new);
}

GSList *
gfgl_list_to_pglyphs (GSList * glyphlist, gdouble affine[])
{
	GSList * pgl;
	GFGLState state;
	GFGLPGlyph * pglyph;
	GnomeFontGlyph * glyph;
	ArtPoint p;
	GSList * l;
g_print ("generating positioned glyph list... ");
	state.rfont = NULL;
	state.glyph = NULL;
	state.letterspace = 0.0;
	state.p.x = state.p.y = 0.0;
	art_affine_point (&state.cp, &state.p, affine);
	state.cpstack = NULL;

	pgl = NULL;

	for (l = glyphlist; l != NULL; l = l->next) {
		GFGLElement * el;

		el = (GFGLElement *) l->data;
		g_return_val_if_fail (el != NULL, NULL);

		switch (el->code) {
		case GFGL_FONT:
			state.rfont = gnome_font_get_rfont (el->value.font, affine);
			break;
		case GFGL_LETTERSPACE:
			state.letterspace = el->value.letterspace;
			break;
		case GFGL_GLYPH:
			state.glyph = gnome_vfont_get_glyph (GNOME_VFONT (state.rfont), el->value.glyph);
			pglyph = g_new (GFGLPGlyph, 1);
			pglyph->glyph = state.glyph;
			pglyph->x = state.cp.x;
			pglyph->y = state.cp.y;
			pgl = g_slist_prepend (pgl, pglyph);
			break;
		case GFGL_X:
			state.p.x = el->value.dval;
			art_affine_point (&state.cp, &state.p, affine);
			break;
		case GFGL_Y:
			state.p.y = el->value.dval;
			art_affine_point (&state.cp, &state.p, affine);
			break;
		case GFGL_PUSH_CP:
			g_print ("PUSH_CP\n");
			break;
		case GFGL_MAX_CP:
			g_print ("MAX_CP\n");
			break;
		case GFGL_ADVANCE:
			gnome_font_glyph_get_stdadvance (state.glyph, &p);
			state.cp.x += p.x;
			state.cp.y += p.y;
			break;
		case GFGL_KERNING:
			/* fixme: glyph ref-ing */
			glyph = gnome_vfont_get_glyph (GNOME_VFONT (state.rfont), el->value.glyph);
			gnome_font_glyph_get_kerning (state.glyph, glyph, &p);
			state.cp.x += p.x;
			state.cp.y += p.y;
			break;
		default:
			g_assert_not_reached ();
			break;
		}
	}
g_print ("done\n");
	return g_slist_reverse (pgl);
}

void
gfgl_pglyphs_free (GSList * list)
{
	while (list) {
		g_free (list->data);
		list = g_slist_remove_link (list, list);
	}
}

/* Debug */

void
gfgl_list_describe (GFGlyphList * list)
{
	GSList * l;

	g_print ("----list----\n");

	for (l = list; l != NULL; l = l->next) {
		GFGLElement * el;

		el = (GFGLElement *) l->data;
		g_return_if_fail (el != NULL);

		switch (el->code) {
		case GFGL_FONT:
			g_print ("FONT %s %g\n",
				gnome_font_get_name (el->value.font),
				el->value.font->size);
			break;
		case GFGL_LETTERSPACE:
			g_print ("LETTERSPACE %g\n", el->value.letterspace);
			break;
		case GFGL_GLYPH:
			g_print ("GLYPH\n");
			break;
		case GFGL_X:
			g_print ("X %g\n", el->value.dval);
			break;
		case GFGL_Y:
			g_print ("Y %g\n", el->value.dval);
			break;
		case GFGL_PUSH_CP:
			g_print ("PUSH_CP\n");
			break;
		case GFGL_MAX_CP:
			g_print ("MAX_CP\n");
			break;
		case GFGL_ADVANCE:
			g_print ("ADVANCE\n");
			break;
		case GFGL_KERNING:
			g_print ("KERNING\n");
			break;
		default:
			g_assert_not_reached ();
			break;
		}
	}

	g_print ("----end-----\n");
}

void
gfgl_pglyphs_describe (GSList * list)
{
	GSList * l;

	for (l = list; l != NULL; l = l->next) {
		GFGLPGlyph * pglyph;

		pglyph = (GFGLPGlyph *) l->data;

		g_print ("Positioned glyph %g %g\n", pglyph->x, pglyph->y);
	}
}


