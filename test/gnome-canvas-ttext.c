#define GNOME_CANVAS_TEXT_C

#include <libgnomeui/gnome-canvas.h>
#include <libgnomeui/gnome-canvas-util.h>
#include <libgnomefont/gnome-font.h>
#include <libgnomefont/gnome-rfont.h>
#include "gnome-canvas-ttext.h"
#include "glyphlist.h"

struct _GnomeCanvasTTextPrivate {
	GFGlyphList * text;
	GSList * pglyphs;
	gdouble px, py;
	guint32 rgba;
};

enum {
	ARG_0,
	ARG_TEXT,
	ARG_RGBA
};

static void gnome_canvas_ttext_class_init (GnomeCanvasTTextClass * klass);
static void gnome_canvas_ttext_init (GnomeCanvasTText * ttext);

static void gnome_canvas_ttext_destroy (GtkObject * object);
static void gnome_canvas_ttext_set_arg (GtkObject * object, GtkArg * arg, guint arg_id);

static void gnome_canvas_ttext_update (GnomeCanvasItem * item, gdouble affine[], ArtSVP * clip_path, gint flags);
static void gnome_canvas_ttext_render (GnomeCanvasItem * item, GnomeCanvasBuf * buf);

static GnomeCanvasItemClass * parent_class;

GtkType
gnome_canvas_ttext_get_type (void)
{
	static GtkType ttext_type = 0;
	if (!ttext_type) {
		GtkTypeInfo ttext_info = {
			"GnomeCanvasTText",
			sizeof (GnomeCanvasTText),
			sizeof (GnomeCanvasTTextClass),
			(GtkClassInitFunc) gnome_canvas_ttext_class_init,
			(GtkObjectInitFunc) gnome_canvas_ttext_init,
			NULL, /* reserved_1 */
			NULL, /* reserved_2 */
			(GtkClassInitFunc) NULL
		};
		ttext_type = gtk_type_unique (gnome_canvas_item_get_type (), &ttext_info);
	}
	return ttext_type;
}

static void
gnome_canvas_ttext_class_init (GnomeCanvasTTextClass *class)
{
	GtkObjectClass *object_class;
	GnomeCanvasItemClass *item_class;

	object_class = (GtkObjectClass *) class;
	item_class = (GnomeCanvasItemClass *) class;

	parent_class = gtk_type_class (gnome_canvas_item_get_type ());

	gtk_object_add_arg_type ("GnomeCanvasTText::text", GTK_TYPE_POINTER, GTK_ARG_READWRITE, ARG_TEXT);
	gtk_object_add_arg_type ("GnomeCanvasTText::rgba", GTK_TYPE_UINT, GTK_ARG_READWRITE, ARG_RGBA);

	object_class->destroy = gnome_canvas_ttext_destroy;
	object_class->set_arg = gnome_canvas_ttext_set_arg;

	item_class->update = gnome_canvas_ttext_update;
	item_class->render = gnome_canvas_ttext_render;
}

static void
gnome_canvas_ttext_init (GnomeCanvasTText *ttext)
{
	ttext->private = g_new0 (GnomeCanvasTTextPrivate, 1);

	ttext->private->rgba = 0x000000ff;
};

static void
gnome_canvas_ttext_destroy (GtkObject *object)
{
	GnomeCanvasTText *ttext;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_IS_CANVAS_TTEXT (object));

	ttext = GNOME_CANVAS_TTEXT (object);

	if (ttext->private) {
		GnomeCanvasTTextPrivate * priv;
		priv = ttext->private;
		if (priv->text) gfgl_free (priv->text);
		if (priv->pglyphs) gfgl_pglyphs_free (priv->pglyphs);
		g_free (priv);
	}

	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gnome_canvas_ttext_set_arg (GtkObject * object, GtkArg * arg, guint arg_id)
{
	GnomeCanvasItem * item;
	GnomeCanvasTText * ttext;
	GnomeCanvasTTextPrivate * priv;
	GFGlyphList * text;

	item = (GnomeCanvasItem *) object;
	ttext = (GnomeCanvasTText *) object;
	priv = ttext->private;

	switch (arg_id) {
	case ARG_TEXT:
		text = GTK_VALUE_POINTER (* arg);
		if (priv->text) {
			gfgl_free (priv->text);
			priv->text = NULL;
		}
		if (text) {
			priv->text = gfgl_duplicate (text);
		}
		gnome_canvas_item_request_update (item);
		break;
	case ARG_RGBA:
		priv->rgba = GTK_VALUE_UINT (* arg);
		gnome_canvas_item_request_update (item);
		break;
	default:
		break;
	}
}

static void
gnome_canvas_ttext_update (GnomeCanvasItem * item, gdouble affine[], ArtSVP * clip_path, gint flags)
{
	GnomeCanvasTText * ttext;
	GnomeCanvasTTextPrivate * priv;
	GSList * l;
	gboolean have_bbox;
g_print ("updating...");
	ttext = (GnomeCanvasTText *) item;
	priv = ttext->private;

	if (((GnomeCanvasItemClass *) parent_class)->update)
		(* ((GnomeCanvasItemClass *) parent_class)->update) (item, affine, clip_path, flags);

#if 0
	/*
	 * So here we are. We know only GnomeFont, which is adjusted to
	 * master resolution and gives us glyph shapes in base coordinates
	 * To do something useful, we have to find GnomeRFont
	 * (Rasterized Font), from given font and affine transformation
	 */

	if (priv->rfont) {
		gnome_rfont_unref (priv->rfont);
		priv->rfont = NULL;
	}

	if (priv->font) {
		priv->rfont = gnome_font_get_rfont (priv->font, affine);
	}
#endif

	/*
	 * Experimental glyphlist handling
	 * NB! Using glyphlist allows us to get rid of handling fonts
	 * directly
	 */

	if (priv->pglyphs) {
		gfgl_pglyphs_free (priv->pglyphs);
		priv->pglyphs = NULL;
	}

	if (priv->text) {
		priv->pglyphs = gfgl_list_to_pglyphs (priv->text, affine);
	}

	/* Now calculate redraw region */
	/* Fixme: handle font == NULL */

	gnome_canvas_item_reset_bounds (item);

	have_bbox = FALSE;

	for (l = priv->pglyphs; l != NULL; l = l->next) {
		GFGLPGlyph * pgl;
		ArtDRect bbox;
		gint x0, y0, x1, y1;

		pgl = (GFGLPGlyph *) l->data;

		gnome_font_glyph_get_dimensions (pgl->glyph, &bbox);

		x0 = pgl->x + bbox.x0;
		y0 = pgl->y - bbox.y1;
		x1 = pgl->x + bbox.x1;
		y1 = pgl->y - bbox.y0;

		gnome_canvas_request_redraw (item->canvas, x0, y0, x1, y1);

		if (!have_bbox) {
			item->x1 = x0;
			item->y1 = y0;
			item->x2 = x1;
			item->y2 = y1;
			have_bbox = TRUE;
		} else {
			item->x1 = MIN (item->x1, x0);
			item->y1 = MIN (item->y1, y0);
			item->x2 = MAX (item->x2, x1);
			item->y2 = MAX (item->y2, y1);
		}

	}

#if 0
	px = priv->px = affine[4];
	py = priv->py = affine[5];

	for (c = priv->text; *c != '\0'; c++) {
		GnomeFontGlyph * glyph;
		const GFGGrayMap * gmap;
		gint code;
		gint x0, y0, x1, y1;

		/*
		 * Note, that although we are using GnomeFont and GnomeRFont
		 * here, glyph code lookups are still done via corresponding
		 * GnomeFontFace. It is general rule, that Font and RFont
		 * add only output/rasterization properties to face, but
		 * does not change anything in other areas
		 */

		code = gnome_font_face_lookup (priv->font->face, 0, * c);

		/*
		 * Now we get RFont glyph. This is like other glyphs, but
		 * it's metrics are fitted (at least should be) to real
		 * device coordinates and from it we can get prerendered
		 * bitmaps
		 */

		glyph = gnome_vfont_get_glyph (GNOME_VFONT (priv->rfont), code);
		g_assert (glyph != NULL);

		/*
		 * Until we have real bbox method, use graymap here
		 */

		gmap = gnome_font_glyph_get_graymap (glyph);
		g_assert (gmap != NULL);

		x0 = px + gmap->x0;
		y0 = py + gmap->y0 - gmap->height;
		x1 = x0 + gmap->width;
		y1 = y0 + gmap->height;

		gnome_canvas_request_redraw (item->canvas, x0, y0, x1, y1);

		if (!have_bbox) {
			item->x1 = x0;
			item->y1 = y0;
			item->x2 = x1;
			item->y2 = y1;
			have_bbox = TRUE;
		} else {
			item->x1 = MIN (item->x1, x0);
			item->y1 = MIN (item->y1, y0);
			item->x2 = MAX (item->x2, x1);
			item->y2 = MAX (item->y2, y1);
		}

		/* fixme: raster glyphs? */
		/*
		 * Theoretically should RFont metrics be given in
		 * real output device coordinates. I.e. horizontal
		 * advance can have bot x and y != 0 if font is rotated.
		 * This is still unimplemented.
		 */

		adv = gnome_font_glyph_get_advance_h (glyph);

		px += adv * affine[0];
		py += adv * affine[1];
	}
#endif
g_print (" done\n");
}

static void
gnome_canvas_ttext_render (GnomeCanvasItem * item, GnomeCanvasBuf * buf)
{
	GnomeCanvasTText * ttext;
	GnomeCanvasTTextPrivate * priv;
	GSList * l;
	guint bg_r, bg_g, bg_b;
	guint fg_r, fg_g, fg_b;
#if 0
	ArtPoint p, a;
#endif

	ttext = (GnomeCanvasTText *) item;
	priv = ttext->private;

#if 0
	p.x = priv->px;
	p.y = priv->py;

g_print ("render %d %d %d %d\n", buf->rect.x0, buf->rect.y0, buf->rect.x1, buf->rect.y1);
g_print ("pos %d %d\n", (gint) p.x, (gint) p.y);
#endif

	gnome_canvas_buf_ensure_buf (buf);
	buf->is_bg = FALSE;
	buf->is_buf = TRUE;

	fg_r = (priv->rgba >> 24) & 0xff;
	fg_g = (priv->rgba >> 16) & 0xff;
	fg_b = (priv->rgba >> 8) & 0xff;

	for (l = priv->pglyphs; l != NULL; l = l->next) {
		GFGLPGlyph * pgl;
		const GFGGrayMap * gmap;
		gint cx, cy, bx, by, x, y;
		guint alpha;
		guchar * b;

		pgl = (GFGLPGlyph *) l->data;

		gmap = gnome_font_glyph_get_graymap (pgl->glyph);
		g_assert (gmap != NULL);

		if ((pgl->y - gmap->y0 > buf->rect.y0 - 1) &&
			(pgl->x + gmap->x0 <= buf->rect.x1 + 1) &&
			(pgl->y - gmap->y0 - gmap->height <= buf->rect.y1 + 1) &&
			(pgl->x + gmap->x0 + gmap->width > buf->rect.x0 - 1)) {

		/* Now the rendering itself */

		for (y = 0; y < gmap->height; y++) {
			cy = pgl->y - gmap->y0 - y;
		for (x = 0; x < gmap->width; x++) {
			cx = x + pgl->x + gmap->x0;
			if ((cx >= buf->rect.x0) &&
				(cy >= buf->rect.y0) &&
				(cx < buf->rect.x1) &&
				(cy < buf->rect.y1)) {

			bx = cx - buf->rect.x0;
			by = cy - buf->rect.y0;

			alpha = gmap->pixels[y * gmap->rowstride + x];
			alpha = alpha * (priv->rgba & 0xff) / 255;
			b = &buf->buf[by * buf->buf_rowstride + bx * 3];

			bg_r = *b;
			bg_g = *(b + 1);
			bg_b = *(b + 2);

			*b = bg_r * (0xff - alpha) / 0xff + fg_r * alpha / 0xff;
			b++;
			*b = bg_g * (0xff - alpha) / 0xff + fg_g * alpha / 0xff;
			b++;
			*b = bg_b * (0xff - alpha) / 0xff + fg_b * alpha / 0xff;

			}
		}
		}
		}
	}
}


