#ifndef _GF_PFB_H_
#define _GF_PFB_H_

#include <glib.h>
#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

typedef struct _GFPFB GFPFB;

struct _GFPFB {
	gchar * filename;
	gchar * fontname;
	gchar * fullname;
	gchar * version;
};

GFPFB * gf_pfb_open (const gchar * name);

void gf_pfb_close (GFPFB * pfb);

END_GNOME_DECLS

#endif
