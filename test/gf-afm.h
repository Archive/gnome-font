#ifndef _GF_AFM_H_
#define _GF_AFM_H_

#include <glib.h>
#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

typedef struct _GFAFM GFAFM;

struct _GFAFM {
	gchar * filename;
	gchar * fontname;
	gchar * fullname;
	gchar * familyname;
	gchar * version;
};

GFAFM * gf_afm_open (const gchar * name);

void gf_afm_close (GFAFM * afm);

END_GNOME_DECLS

#endif
