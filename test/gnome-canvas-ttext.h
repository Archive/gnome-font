#ifndef GNOME_CANVAS_TEXT_H
#define GNOME_CANVAS_TEXT_H

#include <libgnomeui/gnome-canvas.h>

#define GNOME_TYPE_CANVAS_TTEXT            (gnome_canvas_ttext_get_type ())
#define GNOME_CANVAS_TTEXT(obj)            (GTK_CHECK_CAST ((obj), GNOME_TYPE_CANVAS_TTEXT, GnomeCanvasTText))
#define GNOME_CANVAS_TTEXT_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_CANVAS_TTEXT, GnomeCanvasTTextClass))
#define GNOME_IS_CANVAS_TTEXT(obj)         (GTK_CHECK_TYPE ((obj), GNOME_TYPE_CANVAS_TTEXT))
#define GNOME_IS_CANVAS_TTEXT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_CANVAS_TTEXT))


typedef struct _GnomeCanvasTText	GnomeCanvasTText;
typedef struct _GnomeCanvasTTextPrivate	GnomeCanvasTTextPrivate;
typedef struct _GnomeCanvasTTextClass	GnomeCanvasTTextClass;

struct _GnomeCanvasTText {
	GnomeCanvasItem item;
	GnomeCanvasTTextPrivate * private;
};

struct _GnomeCanvasTTextClass {
	GnomeCanvasItemClass parent_class;
};


GtkType gnome_canvas_ttext_get_type (void);

#endif
