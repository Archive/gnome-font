#define _GNOME_FONT_INSTALL_C_

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <ctype.h>
#include <glib.h>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-util.h>
#include <freetype/freetype.h>
#include <libgnomefont/gf-fontmap.h>
#include "parseAFM.h"
#include "gf-pfb.h"

/* Known file types */

typedef enum {
	GFI_FILE_UNKNOWN,
	GFI_FILE_PFB,
	GFI_FILE_AFM,
	GFI_FILE_TTF
} GFIFileType;

/* Known data about any font file */

typedef struct {
	GFIFileType type;
	GFFileEntry entry;
	gchar * fontname;
	gchar * familyname;
	gchar * psname;
	gchar * version;
} GFIFileData;

typedef struct {
	gchar * name;
	gchar * familyname;
	gchar * psname;
	GSList * afm_list;
	GSList * pfb_list;
	GSList * ttf_list;
} GFIFontData;

static void gfi_verify_font_entry (GFFontEntry * e);
static void gfi_verify_afm_file (GFFontEntry * e, GFFileEntry * f);
static void gfi_verify_pfb_file (GFFontEntry * e, GFFileEntry * f);
static void gfi_verify_ttf_file (GFFontEntry * e, GFFileEntry * f);
static GFIFileData * gfi_read_afm_file_data (const gchar * name);
static GFIFileData * gfi_read_pfb_file_data (const gchar * name);
static GFIFileData * gfi_read_ttf_file_data (const gchar * name);
static gboolean gfi_test_file_changed (GFFileEntry * f);
static void gfi_scan_directory (const gchar * name);
static void gfi_try_font_file (const gchar * name);
static void gfi_sort_fonts (void);
static void gfi_process_type1_aliases (void);
static void gfi_process_type1_alias (GFFontEntryT1Alias * t1a);
static void gfi_build_fonts (void);
static void gfi_build_font (GFIFontData * fd);
static void gfi_write_fontmap (FILE * f);
static void gfi_write_font (xmlNodePtr root, GFFontEntry * e);
static void gfi_write_font_tt (xmlNodePtr root, GFFontEntry * e);
static gchar * gfi_get_species_name (const gchar * fullname, const gchar * familyname);

static GSList * type1alias_list;
static GSList * goodafm_list;
static GHashTable * goodafm_dict;
static GSList * goodpfb_list;
static GHashTable * goodpfb_dict;
static GSList * goodttf_list;
static GHashTable * goodttf_dict;

static GSList * font_list;
static GHashTable * font_dict;
static GSList * goodfont_list;
static GHashTable * goodfont_dict;

int main (int argc, char ** argv)
{
	GFFontMap * map;
	GSList * l;
	gint i;

	/* Initialize lists */

	type1alias_list = NULL;
	goodafm_list = NULL;
	goodafm_dict = g_hash_table_new (g_str_hash, g_str_equal);
	goodpfb_list = NULL;
	goodpfb_dict = g_hash_table_new (g_str_hash, g_str_equal);
	goodttf_list = NULL;
	goodttf_dict = g_hash_table_new (g_str_hash, g_str_equal);
	font_list = NULL;
	font_dict = g_hash_table_new (g_str_hash, g_str_equal);
	goodfont_list = NULL;
	goodfont_dict = g_hash_table_new (g_str_hash, g_str_equal);

	/* Step 1: Read existing fontmap */

	map = gf_fontmap_get ();

	/* Verify fonts */
	/* ? maybe after reading other fontmaps? */

	for (l = map->fonts; l != NULL; l = l->next) {
		gfi_verify_font_entry ((GFFontEntry *) l->data);
	}

	/* Process directories */

	for (i = 1; i < argc; i++) {
		gfi_scan_directory (argv[i]);
	}

	/*
	 * Now we have:
	 *
	 * type1alias_list, pointing to existing FontEntries
	 * goodafm_list, goodpfb_list pointing to new FontData
	 * goodafm_dict, goodpfb_dict using list member names
	 *
	 */

	/* Sort all files into fonts */

	gfi_sort_fonts ();

	/*
	 * Now we are ready to process fonts
	 *
	 * We start of Type1Aliases
	 *
	 */

	gfi_process_type1_aliases ();

	/*
	 * And build fonts from remaining afm/pfb files
	 *
	 */

	gfi_build_fonts ();

	/*
	 * Write fontmap
	 *
	 */

	gfi_write_fontmap (stdout);

	gf_fontmap_release (map);

	return 0;
}

/*
 * Process GFFontEntry
 *
 * Tests, whether both afm and pfb files are valid
 * Saves file entries to good{$filetype}list
 * If it is Type1Alias, save it to type1aliaslist
 *
 */

static void
gfi_verify_font_entry (GFFontEntry * e)
{
	GFFontEntryTT * tt;
	GFFontEntryT1 * t1;
	GFFontEntryT1Alias * t1a;

	switch (e->type) {
	case GF_FONT_ENTRY_TYPE1_ALIAS:
		t1a = (GFFontEntryT1Alias *) e;
		type1alias_list = g_slist_prepend (type1alias_list, e);
	case GF_FONT_ENTRY_TYPE1:
		t1 = (GFFontEntryT1 *) e;
		gfi_verify_afm_file (e, &t1->afm);
		gfi_verify_pfb_file (e, &t1->pfb);
		break;
	case GF_FONT_ENTRY_TRUETYPE:
		tt = (GFFontEntryTT *) e;
		gfi_verify_ttf_file (e, &tt->ttf);
		break;
	case GF_FONT_ENTRY_ALIAS:
		/* No Alias support at moment */
		break;
	default:
		g_assert_not_reached ();
	}
}

/*
 * Verifies afm entry
 *
 * If good, create new FileData and save it to goodafm_list/goodafm_dict
 *
 */

static void
gfi_verify_afm_file (GFFontEntry * e, GFFileEntry * f)
{
	GFIFileData * fd;

	/* Test, whether we are already verified and registered */
	fd = g_hash_table_lookup (goodafm_dict, f->name);
	if (fd) return;

	if (!gfi_test_file_changed (f)) {
		fd = g_new0 (GFIFileData, 1);
		fd->type = GFI_FILE_AFM;
		fd->entry.name = g_strdup (f->name);
		fd->entry.size = f->size;
		fd->entry.mtime = f->mtime;
		fd->fontname = g_strdup (e->name);
		fd->familyname = g_strdup (e->familyname);
		fd->psname = g_strdup (e->psname);
		fd->version = g_strdup (e->version);
	} else {
		fd = gfi_read_afm_file_data (f->name);
	}

	if (fd) {
		goodafm_list = g_slist_prepend (goodafm_list, fd);
		g_hash_table_insert (goodafm_dict, fd->entry.name, fd);
	} else {
#ifdef GFI_VERBOSE
		g_print ("Not good: %s\n", f->name);
#endif
	}
}

static void
gfi_verify_pfb_file (GFFontEntry * e, GFFileEntry * f)
{
	GFIFileData * fd;

	/* Test, whether we are already verified and registered */
	fd = g_hash_table_lookup (goodpfb_dict, f->name);
	if (fd) return;

	if (!gfi_test_file_changed (f)) {
		fd = g_new0 (GFIFileData, 1);
		fd->type = GFI_FILE_PFB;
		fd->entry.name = g_strdup (f->name);
		fd->entry.size = f->size;
		fd->entry.mtime = f->mtime;
		fd->fontname = g_strdup (e->name);
		fd->familyname = g_strdup (e->familyname);
		fd->psname = g_strdup (e->psname);
		fd->version = g_strdup (e->version);
	} else {
		fd = gfi_read_pfb_file_data (f->name);
	}

	if (fd) {
		goodpfb_list = g_slist_prepend (goodpfb_list, fd);
		g_hash_table_insert (goodpfb_dict, fd->entry.name, fd);
	} else {
#ifdef GFI_VERBOSE
		g_print ("Not good: %s\n", f->name);
#endif
	}
}

static void
gfi_verify_ttf_file (GFFontEntry * e, GFFileEntry * f)
{
	GFIFileData * fd;

	/* Test, whether we are already verified and registered */
	fd = g_hash_table_lookup (goodttf_dict, f->name);
	if (fd) return;

	if (!gfi_test_file_changed (f)) {
		fd = g_new0 (GFIFileData, 1);
		fd->type = GFI_FILE_TTF;
		fd->entry.name = g_strdup (f->name);
		fd->entry.size = f->size;
		fd->entry.mtime = f->mtime;
		fd->fontname = g_strdup (e->name);
		fd->familyname = g_strdup (e->familyname);
		fd->psname = g_strdup (e->psname);
		fd->version = g_strdup (e->version);
	} else {
		fd = gfi_read_ttf_file_data (f->name);
	}

	if (fd) {
		goodttf_list = g_slist_prepend (goodttf_list, fd);
		g_hash_table_insert (goodttf_dict, fd->entry.name, fd);
	} else {
#ifdef GFI_VERBOSE
		g_print ("Not good: %s\n", f->name);
#endif
	}
}

/*
 * Return newly allocated GFIFileData or NULL if not valid afm file
 */

static GFIFileData *
gfi_read_afm_file_data (const gchar * name)
{
	GFIFileData * fd;
	FILE * f;
	int status;
	Font_Info * fi;
	struct stat s;

	fi = NULL;

	if (stat (name, &s) < 0) return NULL;

	f = fopen (name, "r");
	if (!f) return NULL;

	status = parseFile (f, &fi, P_G);

	fclose (f);
	if (status != AFM_ok) {
		if (fi) parseFileFree (fi);
		return NULL;
	}

	/* Loading afm succeeded, so go ahead */

	fd = g_new (GFIFileData, 1);

	fd->type = GFI_FILE_AFM;
	fd->entry.name = g_strdup (name);
	fd->entry.size = s.st_size;
	fd->entry.mtime = s.st_mtime;
	fd->fontname = g_strdup (fi->gfi->fullName);
	fd->familyname = g_strdup (fi->gfi->familyName);
	fd->psname = g_strdup (fi->gfi->fontName);
	fd->version = g_strdup (fi->gfi->version);

	parseFileFree (fi);

	return fd;
}

/*
 * Return newly allocated GFIFileData or NULL if not valid pfb file
 */

static GFIFileData *
gfi_read_pfb_file_data (const gchar * name)
{
	GFIFileData * fd;
	GFPFB * pfb;
	struct stat s;

	if (stat (name, &s) < 0) return NULL;

	pfb = gf_pfb_open (name);
	if (!pfb) return NULL;

	/* Loading pfb succeeded, so go ahead */

	fd = g_new (GFIFileData, 1);

	fd->type = GFI_FILE_PFB;
	fd->entry.name = g_strdup (name);
	fd->entry.size = s.st_size;
	fd->entry.mtime = s.st_mtime;
	fd->fontname = g_strdup (pfb->gfi.fullName);
	fd->familyname = g_strdup (pfb->gfi.familyName);
	fd->psname = g_strdup (pfb->gfi.fontName);
	fd->version = g_strdup (pfb->gfi.version);

	gf_pfb_close (pfb);

	return fd;
}

/*
 * Return newly allocated GFIFileData or NULL if not valid FreeType file
 */

static GFIFileData *
gfi_read_ttf_file_data (const gchar * name)
{
	static gboolean ftinit = FALSE;
	static FT_Library library;
	GFIFileData * fd;
	FT_Face face;
	FT_Error result;
	struct stat s;
	gchar * p;

	if (stat (name, &s) < 0) return NULL;

	if (!ftinit) {
		result = FT_Init_FreeType (&library);
		if (result != FT_Err_Ok) return NULL;
		ftinit = TRUE;
	}

	result = FT_New_Face (library, name, 0, &face);
	if (result != FT_Err_Ok) return NULL;

	/* Loading font succeeded, so go ahead */
	/* fixme: test scalable etc. */

	fd = g_new (GFIFileData, 1);

	fd->type = GFI_FILE_TTF;
	fd->entry.name = g_strdup (name);
	fd->entry.size = s.st_size;
	fd->entry.mtime = s.st_mtime;
	fd->fontname = g_strconcat (face->family_name, " ", face->style_name, NULL);
	fd->familyname = g_strdup (face->family_name);
	fd->psname = g_strdup (fd->fontname);
	for (p = fd->psname; *p; p++) {
		if (!isalnum(*p)) *p = '-';
	}
	fd->version = g_strdup ("0.1");

	FT_Done_Face (face);

	return fd;
}

static gboolean
gfi_test_file_changed (GFFileEntry * f)
{
	struct stat s;

	if (stat (f->name, &s) < 0) return TRUE;

	/* If we do not have file info, expect it to be changed */

	if ((f->size == 0) || (s.st_size == f->size)) return TRUE;
	if ((f->mtime == 0) || (s.st_mtime == f->mtime)) return TRUE;

	return FALSE;
}

/*
 * Scan directory for font files, update goodafm and goodpfb
 */

static void
gfi_scan_directory (const gchar * name)
{
	DIR * dir;
	struct dirent * dent;

	dir = opendir (name);

	if (dir) {
		while ((dent = readdir (dir))) {
			gchar * fn;
			fn = g_concat_dir_and_file (name, dent->d_name);
			gfi_try_font_file (fn);
			g_free (fn);
		}
	} else {
#ifdef GFI_VERBOSE
		g_print ("Invalid directory: %s\n", name);
#endif
	}
}

static void
gfi_try_font_file (const gchar * name)
{
	GFIFileData * fd;
	struct stat s;

	if (stat (name, &s) < 0) return;
	if (!S_ISREG (s.st_mode)) return;

	fd = g_hash_table_lookup (goodafm_dict, name);
	if (fd) return;
	fd = g_hash_table_lookup (goodpfb_dict, name);
	if (fd) return;

	/* Not registered, so try to determine file type */

	fd = gfi_read_afm_file_data (name);
	if (fd) {
		goodafm_list = g_slist_prepend (goodafm_list, fd);
		g_hash_table_insert (goodafm_dict, fd->entry.name, fd);
		return;
	}

	fd = gfi_read_pfb_file_data (name);
	if (fd) {
		goodpfb_list = g_slist_prepend (goodpfb_list, fd);
		g_hash_table_insert (goodpfb_dict, fd->entry.name, fd);
		return;
	}

	fd = gfi_read_ttf_file_data (name);
	if (fd) {
		goodttf_list = g_slist_prepend (goodttf_list, fd);
		g_hash_table_insert (goodttf_dict, fd->entry.name, fd);
		return;
	}

	/* Cannot read :( */
}

/*
 * Arranges all afm and pfb FileData into FontData structures
 * goodfont_list - list of new FontData entries
 * goodfont_dict - use FontData name strings
 * Original lists are cleaned
 *
 */

static void
gfi_sort_fonts (void)
{
	GFIFileData * file;
	GFIFontData * font;

	while (goodafm_list) {
		file = (GFIFileData *) goodafm_list->data;
		font = g_hash_table_lookup (font_dict, file->fontname);
		if (!font) {
			font = g_new (GFIFontData, 1);
			font->name = g_strdup (file->fontname);
			font->familyname = g_strdup (file->familyname);
			font->psname = g_strdup (file->psname);
			font->afm_list = font->pfb_list = font->ttf_list = NULL;
			font_list = g_slist_prepend (font_list, font);
			g_hash_table_insert (font_dict, font->name, font);
		}
		font->afm_list = g_slist_prepend (font->afm_list, file);
		goodafm_list = g_slist_remove (goodafm_list, file);
	}

	while (goodpfb_list) {
		file = (GFIFileData *) goodpfb_list->data;
		font = g_hash_table_lookup (font_dict, file->fontname);
		if (!font) {
			font = g_new (GFIFontData, 1);
			font->name = g_strdup (file->fontname);
			font->familyname = g_strdup (file->familyname);
			font->psname = g_strdup (file->psname);
			font->afm_list = font->pfb_list = font->ttf_list = NULL;
			font_list = g_slist_prepend (font_list, font);
			g_hash_table_insert (font_dict, font->name, font);
		}
		font->pfb_list = g_slist_prepend (font->pfb_list, file);
		goodpfb_list = g_slist_remove (goodpfb_list, file);
	}

	while (goodttf_list) {
		file = (GFIFileData *) goodttf_list->data;
		font = g_hash_table_lookup (font_dict, file->fontname);
		if (!font) {
			font = g_new (GFIFontData, 1);
			font->name = g_strdup (file->fontname);
			font->familyname = g_strdup (file->familyname);
			font->psname = g_strdup (file->psname);
			font->afm_list = font->pfb_list = font->ttf_list = NULL;
			font_list = g_slist_prepend (font_list, font);
			g_hash_table_insert (font_dict, font->name, font);
		}
		font->ttf_list = g_slist_prepend (font->ttf_list, file);
		goodttf_list = g_slist_remove (goodttf_list, file);
	}
}

static void
gfi_process_type1_aliases (void)
{
	while (type1alias_list) {
		gfi_process_type1_alias ((GFFontEntryT1Alias *) type1alias_list->data);
		type1alias_list = g_slist_remove (type1alias_list, type1alias_list->data);
	}
}

static void
gfi_process_type1_alias (GFFontEntryT1Alias * t1a)
{
	GFFontEntryT1Alias * new;
	GFIFileData * afmdata, * pfbdata;
	GFIFontData * fontdata;
	gdouble afmversion, pfbversion;
	GSList * l;

	/* Return if we are already registered */
	/* fixme: We should test versions here */
	if (g_hash_table_lookup (goodfont_dict, t1a->t1.entry.name)) return;

	afmdata = g_hash_table_lookup (goodafm_dict, t1a->t1.afm.name);
	/* If we do not have good afm, return */
	if (!afmdata) return;

	/* We should probably use assertion here */
	fontdata = g_hash_table_lookup (font_dict, afmdata->fontname);
	if (!fontdata) return;

	/* fixme: mess with locale */
	afmversion = atof (afmdata->version);

	/* Search, whether we have same or better pfb */
	for (l = fontdata->pfb_list; l != NULL; l = l->next) {
		gdouble pfbversion;
		pfbdata = (GFIFileData *) l->data;
		pfbversion = atof (pfbdata->version);
		/* If we have original pfb with same or higher version, we shouldn't use type1 alias */
		if (pfbversion >= afmversion) return;
	}

	/* So there wasn't original pfb file */

	pfbdata = g_hash_table_lookup (goodpfb_dict, t1a->t1.pfb.name);
	if (!pfbdata) return;

	/* Find pfb file with highest version */
	/* We should probably use assertion here */
	fontdata = g_hash_table_lookup (font_dict, pfbdata->fontname);
	if (!fontdata) return;

	/* fixme: mess with locale */
	pfbversion = atof (pfbdata->version);

	for (l = fontdata->pfb_list; l != NULL; l = l->next) {
		GFIFileData * d;
		gdouble v;
		d = (GFIFileData *) l->data;
		v = atof (d->version);
		if (v > pfbversion) {
			pfbversion = v;
			pfbdata = d;
		}
	}

	/* Now we should have everything we need */

	new = g_new (GFFontEntryT1Alias, 1);
	new->t1.entry.type = GF_FONT_ENTRY_TYPE1_ALIAS;
	new->t1.entry.refcount = 1;
	new->t1.entry.face = NULL;
	new->t1.entry.name = g_strdup (afmdata->fontname);
	new->t1.entry.version = g_strdup (afmdata->version);
	new->t1.entry.familyname = g_strdup (t1a->t1.entry.familyname);
	new->t1.entry.speciesname = g_strdup (t1a->t1.entry.speciesname);
	new->t1.entry.psname = g_strdup (t1a->t1.entry.psname);
	new->t1.entry.weight = g_strdup (t1a->t1.entry.weight);
	/* AFM */
	new->t1.afm.name = g_strdup (afmdata->entry.name);
	new->t1.afm.size = afmdata->entry.size;
	new->t1.afm.mtime = afmdata->entry.mtime;
	/* PFB */
	new->t1.pfb.name = g_strdup (pfbdata->entry.name);
	new->t1.pfb.size = pfbdata->entry.size;
	new->t1.pfb.mtime = pfbdata->entry.mtime;
	/* Misc */
	new->t1.Weight = t1a->t1.Weight;
	new->t1.ItalicAngle = t1a->t1.ItalicAngle;
	new->alias = g_strdup (pfbdata->psname);

	/* Register it */

#ifdef GFI_VERBOSE
	g_print ("Done %s\n", new->t1.entry.name);
#endif

	goodfont_list = g_slist_prepend (goodfont_list, new);
	g_hash_table_insert (goodfont_dict, new->t1.entry.name, new);
}

static void
gfi_build_fonts (void)
{
	while (font_list) {
		gfi_build_font ((GFIFontData *) font_list->data);
		font_list = g_slist_remove (font_list, font_list->data);
	}
}

static void
gfi_build_font (GFIFontData * fd)
{
	GFFontEntryT1 * new;
	GFIFileData * afmdata, * pfbdata, * ttfdata;
	gdouble afmversion, pfbversion, ttfversion;
	GSList * l;
	FILE * f;
	int status;
	Font_Info * fi;

	/* Return if we are already registered */
	/* Fixme: We should free structs */
	if (g_hash_table_lookup (goodfont_dict, fd->name)) return;

	pfbdata = NULL;
	pfbversion = -1e18;

	/* Find pfb vith highest version */
	for (l = fd->pfb_list; l != NULL; l = l->next) {
		GFIFileData * d;
		gdouble v;
		d = (GFIFileData *) l->data;
		v = atof (d->version);
		if (v > pfbversion) {
			pfbversion = v;
			pfbdata = d;
		}
	}

	if (pfbdata) {
		/* We try Type1 first */

		afmdata = NULL;
		afmversion = -1e18;

		/* Find afm vith highest version <= pfb version */
		for (l = fd->afm_list; l != NULL; l = l->next) {
			GFIFileData * d;
			gdouble v;
			d = (GFIFileData *) l->data;
			v = atof (d->version);
			if ((v > afmversion) && (v <= pfbversion)) {
				afmversion = v;
				afmdata = d;
				if (afmversion == pfbversion) break;
			}
		}

		if (afmdata) {
			/* We have to read afm to get weight and italicangle */

			fi = NULL;
			f = fopen (afmdata->entry.name, "r");
			/* This shouldn't happen */
			if (!f) return;
			status = parseFile (f, &fi, P_G);
			fclose (f);
			/* This shouldn't happen! */
			if (status != AFM_ok) {
				if (fi) parseFileFree (fi);
				return;
			}

			/* Now we should have everything we need */

			new = g_new (GFFontEntryT1, 1);
			new->entry.type = GF_FONT_ENTRY_TYPE1;
			new->entry.refcount = 1;
			new->entry.face = NULL;
			new->entry.name = g_strdup (fd->name);
			new->entry.version = g_strdup (afmdata->version);
			new->entry.familyname = g_strdup (fd->familyname);
			new->entry.speciesname = gfi_get_species_name (fd->name, fd->familyname);
			new->entry.psname = g_strdup (fd->psname);
			new->entry.weight = g_strdup (fi->gfi->weight);
			/* AFM */
			new->afm.name = g_strdup (afmdata->entry.name);
			new->afm.size = afmdata->entry.size;
			new->afm.mtime = afmdata->entry.mtime;
			/* PFB */
			new->pfb.name = g_strdup (pfbdata->entry.name);
			new->pfb.size = pfbdata->entry.size;
			new->pfb.mtime = pfbdata->entry.mtime;
			/* Misc */
			new->Weight = gf_fontmap_lookup_weight (new->entry.weight);
			new->ItalicAngle = fi->gfi->italicAngle;

			/* Release AFM info */
			parseFileFree (fi);

			/* Register it */

#ifdef GFI_VERBOSE
			g_print ("Done %s\n", new->entry.name);
#endif

			goodfont_list = g_slist_prepend (goodfont_list, new);
			g_hash_table_insert (goodfont_dict, new->entry.name, new);

			return;
		}
	}

	/* No Type1, try ttf */

	ttfdata = NULL;
	ttfversion = -1e18;

	/* Find pfb vith highest version */
	for (l = fd->ttf_list; l != NULL; l = l->next) {
		GFIFileData * d;
		gdouble v;
		d = (GFIFileData *) l->data;
		v = atof (d->version);
		if (v > ttfversion) {
			ttfversion = v;
			ttfdata = d;
		}
	}

	if (ttfdata) {
		GFFontEntryTT * new;

		/* Now we should have everything we need */

		new = g_new (GFFontEntryTT, 1);
		new->entry.type = GF_FONT_ENTRY_TRUETYPE;
		new->entry.refcount = 1;
		new->entry.face = NULL;
		new->entry.name = g_strdup (fd->name);
		new->entry.version = g_strdup (ttfdata->version);
		new->entry.familyname = g_strdup (fd->familyname);
		new->entry.speciesname = gfi_get_species_name (fd->name, fd->familyname);
		new->entry.psname = g_strdup (fd->psname);
		/* fixme: */
		new->entry.weight = g_strdup ("Book");
		/* TTF */
		new->ttf.name = g_strdup (ttfdata->entry.name);
		new->ttf.size = ttfdata->entry.size;
		new->ttf.mtime = ttfdata->entry.mtime;
		/* Misc */
		new->Weight = gf_fontmap_lookup_weight (new->entry.weight);
		/* fixme: */
		new->ItalicAngle = 0.0;

		/* Register it */

#ifdef GFI_VERBOSE
		g_print ("Done %s\n", new->entry.name);
#endif

		goodfont_list = g_slist_prepend (goodfont_list, new);
		g_hash_table_insert (goodfont_dict, new->entry.name, new);
	}
}

static void
gfi_write_fontmap (FILE * f)
{
	xmlDocPtr doc;
	xmlNodePtr root;

	doc = xmlNewDoc ("1.0");
	root = xmlNewDocNode (doc, NULL, "fontmap", NULL);
	xmlDocSetRootElement (doc, root);
	xmlSetProp (root, "version", "2.0");

	while (goodfont_list) {
		GFFontEntry * e;
		e = (GFFontEntry *) goodfont_list->data;
		switch (e->type) {
		case GF_FONT_ENTRY_TYPE1:
		case GF_FONT_ENTRY_TYPE1_ALIAS:
			gfi_write_font (root, e);
			break;
		case GF_FONT_ENTRY_TRUETYPE:
			gfi_write_font_tt (root, e);
			break;
		default:
			break;
		}
		goodfont_list = g_slist_remove (goodfont_list, goodfont_list->data);
	}

	xmlDocDump (f, doc);
}

static void
gfi_write_font (xmlNodePtr root, GFFontEntry * e)
{
	GFFontEntryT1 * t1;
	xmlNodePtr n, f;
	gchar c[128];

	t1 = (GFFontEntryT1 *) e;

	n = xmlNewDocNode (root->doc, NULL, "font", NULL);
	xmlAddChild (root, n);

	/* Set format */
	if (e->type == GF_FONT_ENTRY_TYPE1_ALIAS) {
		xmlSetProp (n, "format", "type1alias");
	} else {
		xmlSetProp (n, "format", "type1");
	}

	/* afm file */
	f = xmlNewDocNode (root->doc, NULL, "file", NULL);
	xmlAddChild (n, f);
	xmlSetProp (f, "type", "afm");
	xmlSetProp (f, "path", t1->afm.name);
	g_snprintf (c, 128, "%d", (gint) t1->afm.size);
	xmlSetProp (f, "size", c);
	g_snprintf (c, 128, "%d", (gint) t1->afm.mtime);
	xmlSetProp (f, "mtime", c);

	/* pfb file */
	f = xmlNewDocNode (root->doc, NULL, "file", NULL);
	xmlAddChild (n, f);
	xmlSetProp (f, "type", "pfb");
	xmlSetProp (f, "path", t1->pfb.name);
	g_snprintf (c, 128, "%d", (gint) t1->pfb.size);
	xmlSetProp (f, "size", c);
	g_snprintf (c, 128, "%d", (gint) t1->pfb.mtime);
	xmlSetProp (f, "mtime", c);

	/* Other properties */
	xmlSetProp (n, "name", e->name);
	xmlSetProp (n, "version", e->version);
	xmlSetProp (n, "familyname", e->familyname);
	xmlSetProp (n, "speciesname", e->speciesname);
	xmlSetProp (n, "psname", e->psname);
	xmlSetProp (n, "weight", e->weight);
	g_snprintf (c, 128, "%g", t1->ItalicAngle);
	xmlSetProp (n, "italicangle", c);

	if (e->type == GF_FONT_ENTRY_TYPE1_ALIAS) {
		GFFontEntryT1Alias * t1a;
		t1a = (GFFontEntryT1Alias *) e;
		xmlSetProp (n, "alias", t1a->alias);
	}
}

static void
gfi_write_font_tt (xmlNodePtr root, GFFontEntry * e)
{
	GFFontEntryTT * tt;
	xmlNodePtr n, f;
	gchar c[128];

	tt = (GFFontEntryTT *) e;

	n = xmlNewDocNode (root->doc, NULL, "font", NULL);
	xmlAddChild (root, n);

	/* Set format */
	xmlSetProp (n, "format", "truetype");

	/* ttf file */
	f = xmlNewDocNode (root->doc, NULL, "file", NULL);
	xmlAddChild (n, f);
	xmlSetProp (f, "type", "ttf");
	xmlSetProp (f, "path", tt->ttf.name);
	g_snprintf (c, 128, "%d", (gint) tt->ttf.size);
	xmlSetProp (f, "size", c);
	g_snprintf (c, 128, "%d", (gint) tt->ttf.mtime);
	xmlSetProp (f, "mtime", c);

	/* Other properties */
	xmlSetProp (n, "name", e->name);
	xmlSetProp (n, "version", e->version);
	xmlSetProp (n, "familyname", e->familyname);
	xmlSetProp (n, "speciesname", e->speciesname);
	xmlSetProp (n, "psname", e->psname);
	xmlSetProp (n, "weight", e->weight);
	g_snprintf (c, 128, "%g", tt->ItalicAngle);
	xmlSetProp (n, "italicangle", c);
}

static gchar *
gfi_get_species_name (const gchar * fullname, const gchar * familyname)
{
	gchar * p;

	p = strstr (fullname, familyname);

	if (!p) return g_strdup ("Normal");

	p = p + strlen (familyname);

	while (*p && (*p < 'A')) p++;

	if (!*p) return g_strdup ("Normal");

	return g_strdup (p);
}


