AC_INIT(libgnomefont/gnome-font.c)
AM_CONFIG_HEADER(config.h)
AM_INIT_AUTOMAKE(gnome-font,0.2)
AM_ACLOCAL_INCLUDE(macros)

dnl libtool versioning

dnl increment if the interface has additions, changes, removals.
GNOMEFONT_CURRENT=0

dnl increment any time the source changes; set to
dnl  0 if you increment CURRENT
GNOMEFONT_REVISION=4

dnl increment if any interfaces have been added; set to 0
dnl  if any interfaces have been removed. removal has    
dnl  precedence over adding, so set to 0 if both happened.
GNOMEFONT_AGE=2

AC_SUBST(GNOMEFONT_CURRENT)
AC_SUBST(GNOMEFONT_REVISION)
AC_SUBST(GNOMEFONT_AGE)

AM_MAINTAINER_MODE

AC_ISC_POSIX
AC_PROG_CC
AC_PROG_CPP
AM_PROG_LIBTOOL

ALL_LINGUAS=""

GNOME_INIT

GNOME_COMPILE_WARNINGS
GNOME_X_CHECKS
GNOME_XML_CHECK
AM_GNOME_GETTEXT

dnl ====================================
dnl = Begin tests for FreeType2
dnl ====================================
FREETYPE2_CFLAGS=
FREETYPE2_LIBS=

_found_freetype2_lib="no"
_found_freetype2_header="no"
_found_freetype2="no"

AC_ARG_WITH(freetype2-libraries,
[  --with-freetype2-libraries=PATH		freetype2 library path],
freetype2_lib_prefix=$withval)

AC_ARG_WITH(freetype2-includes,
[  --with-freetype2-includes=PATH		freetype2 include path],
freetype2_include_prefix=$withval)

AC_ARG_WITH(freetype2-prefix,
[  --with-freetype2-prefix=PATH			freetype2 install prefix],
freetype2_prefix=$withval, freetype2_prefix="/usr")

if test -z "$freetype2_include_prefix"; then
	freetype2_include_prefix="${freetype2_prefix}/include/freetype2"
fi
if test -z "$freetype2_lib_prefix"; then
	freetype2_lib_prefix="${freetype2_prefix}/lib"
fi

_save_cpp="$CPP"
_save_ldflags="$LDFLAGS"
CPP="$_save_cpp -I$freetype2_include_prefix"
LDFLAGS="$_save_ldflags -L$freetype2_lib_prefix"
AC_CHECK_HEADER(freetype/freetype.h, _found_freetype2_header="yes")
AC_CHECK_LIB(freetype, FT_Init_FreeType, [_found_freetype2_lib="yes"])
CPP="$_save_cpp"
LDFLAGS="$_save_ldflags"

if test "$_found_freetype2_lib" = "yes" -a \
	"$_found_freetype2_header" = "yes"
then
  _found_freetype2="yes"
fi

if test "$_found_freetype2" != "yes" 
then
AC_MSG_ERROR([
*** You need FreeType2 library (in std location) and headers in /usr/freetype2
*** You can probably find packaged version of FreeType2
*** over here: http://developer.eazel.com/eazel-hacking/updates/freetype2])
fi

FREETYPE2_CFLAGS="-I$freetype2_include_prefix"
FREETYPE2_LIBS="-L$freetype2_lib_prefix -lfreetype"

AC_SUBST(FREETYPE2_CFLAGS)
AC_SUBST(FREETYPE2_LIBS)

dnl =======================
dnl = End tests for FreeType2
dnl =======================

GNOMEFONT_LIBS="`$GNOME_CONFIG --libs gtk libart` `xml-config --libs` -lfreetype"

AC_SUBST(GNOMEFONT_LIBS)

FONT_LIBDIR='-L${libdir}'
FONT_INCLUDEDIR='-I${includedir}'
FONT_INCLUDEDIR="$FONT_INCLUDEDIR `xml-config --cflags`"
FONT_LIBS="-lgnomefont $GNOMEFONT_LIBS"

AC_SUBST(FONT_LIBDIR)
AC_SUBST(FONT_INCLUDEDIR)
AC_SUBST(FONT_LIBS)

AC_SUBST(CFLAGS)
AC_SUBST(CPPFLAGS)
AC_SUBST(LDFLAGS)

AC_OUTPUT([
Makefile
intl/Makefile
libgnomefont/Makefile
libgnomefont/ft2/Makefile
installer/Makefile
test/Makefile
])

