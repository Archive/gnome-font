#ifndef _GNOME_FONT_FACE_H_
#define _GNOME_FONT_FACE_H_

/*
 * GnomeFontFace
 *
 * Abstract, unsized typeface handle. Note, that this is also abstract base
 * class for inheriting implementations of different font file formats
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#define GNOME_TYPE_FONT_FACE		(gnome_font_face_get_type ())
#define GNOME_FONT_FACE(obj)		(GTK_CHECK_CAST ((obj), GNOME_TYPE_FONT_FACE, GnomeFontFace))
#define GNOME_FONT_FACE_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_FONT_FACE, GnomeFontFaceClass))
#define GNOME_IS_FONT_FACE(obj)		(GTK_CHECK_TYPE ((obj), GNOME_TYPE_FONT_FACE))
#define GNOME_IS_FONT_FACE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_FONT_FACE))

typedef struct _GnomeFontFace		GnomeFontFace;
typedef struct _GnomeFontFaceClass	GnomeFontFaceClass;

typedef enum {
	GNOME_FONT_LIGHTEST = -3,
	GNOME_FONT_EXTRA_LIGHT = -3,
	GNOME_FONT_THIN = -2,
	GNOME_FONT_LIGHT = -1,
	GNOME_FONT_BOOK = 0, /* also known as "regular" or "roman" */
	/* gap here so that if book is missing, light wins over medium */ 
	GNOME_FONT_MEDIUM = 2,
	GNOME_FONT_SEMI = 3, /* also known as "demi" */
	GNOME_FONT_BOLD = 4,
	/* gap here so that if bold is missing, semi wins over heavy */
	GNOME_FONT_HEAVY = 6,
	GNOME_FONT_EXTRABOLD = 7,
	GNOME_FONT_BLACK = 8,
	GNOME_FONT_EXTRABLACK = 9, /* also known as "ultra" */
	GNOME_FONT_HEAVIEST = 9
} GnomeFontWeight;

#include "gnome-font-family.h"
#include "gnome-vfont.h"
#include "gnome-font.h"

GtkType gnome_font_face_get_type (void);

#define gnome_font_face_ref(f) gtk_object_ref (GTK_OBJECT (f))
#define gnome_font_face_unref(f) gtk_object_unref (GTK_OBJECT (f))

const GnomeFontFamily * gnome_font_face_get_family (const GnomeFontFace * face);

/*
 * Naming
 *
 * gnome_font_face_get_name () should return one "true" name for font, that
 * does not have to be its PostScript name.
 * These names will be localized, and there will be _full versions of the
 * same methods (except _get_ps_name)
 * All names are UTF-8
 */

const gchar * gnome_font_face_get_name (const GnomeFontFace * face);
const gchar * gnome_font_face_get_family_name (const GnomeFontFace * face);
const gchar * gnome_font_face_get_species_name (const GnomeFontFace * face);
const gchar * gnome_font_face_get_ps_name (const GnomeFontFace * face);
/* Returns list of const strings in order of preference */
GList * gnome_font_face_get_ps_synonyms (const GnomeFontFace * face);

/*
 * Get glyph number from unicode char
 *
 * In future there can probably be several different unicode to glyph
 * character mappings per font (for different languages), current function
 * uses the default one :)
 */
   
gint gnome_font_face_lookup_default (const GnomeFontFace * face, gint unicode);
gint gnome_font_face_lookup (const GnomeFontFace * face, gint encoding, gint code);

/*
 * Metrics
 *
 * Currently those return standard values for left to right, horizontal script
 * The prefix std means, that there (a) will hopefully be methods to extract
 * different metric values and (b) for given face one text direction can
 * be defined as "default"
 * All face metrics are given in 0.001 em units
 */

ArtDRect * gnome_font_face_get_stdbbox (const GnomeFontFace * face, ArtDRect * bbox);

ArtPoint * gnome_font_face_get_glyph_stdadvance (const GnomeFontFace * face, gint glyphnum, ArtPoint * advance);
ArtDRect * gnome_font_face_get_glyph_stdbbox (const GnomeFontFace * face, gint glyphnum, ArtDRect * bbox);
const ArtBpath * gnome_font_face_get_glyph_stdoutline (const GnomeFontFace * face, gint glyphnum);

/*
 * Create font
 *
 * Those allow one to get sized font object from given typeface. Theoretically
 * GnomeFont should be optimized for certain resolution (resx, resy). If that 
 * resolution is not known, get_font_default should give reasonably well-
 * looking font for most occasions
 */

GnomeFont * gnome_font_face_get_font_default (GnomeFontFace * face, gdouble size);
GnomeFont * gnome_font_face_get_font (GnomeFontFace * face, gdouble size, gdouble xres, gdouble yres);
GnomeFont * gnome_font_face_get_font_full (GnomeFontFace * face, gdouble size, gdouble transform[]);

/* Encode */

/* NB! This is experimental and probably will be removed */
/* Also we may want to get listing of TT/OT charmaps before */

gboolean gnome_font_face_set_encoding (GnomeFontFace * face, gint id, const gchar * name, const gchar * encoding);

/* Experimental */

guchar * gnome_font_face_get_ps_object (const GnomeFontFace * face, const guchar * psname);

/*
 * convenience and compatibility methods from gnome-print
 * THESE ARE DEPRECATED by default, although some probably will be preserved
 */

GnomeFontFace * gnome_font_face_new (const gchar * name);
gint gnome_font_face_get_num_glyphs (const GnomeFontFace * face);
gchar * gnome_font_face_get_pfa (const GnomeFontFace * face);
gdouble gnome_font_face_get_ascender (const GnomeFontFace * face);
gdouble gnome_font_face_get_descender (const GnomeFontFace * face);
gdouble gnome_font_face_get_underline_position (const GnomeFontFace * face);
gdouble gnome_font_face_get_underline_thickness (const GnomeFontFace * face);
gdouble gnome_font_face_get_glyph_width (const GnomeFontFace * face, gint glyph);
gdouble gnome_font_face_get_glyph_kerning (const GnomeFontFace * face, gint glyph1, gint glyph2);
const gchar * gnome_font_face_get_glyph_ps_name (const GnomeFontFace * face, gint glyph);
GnomeFontWeight gnome_font_face_get_weight_code (const GnomeFontFace * face);
gboolean gnome_font_face_is_italic (const GnomeFontFace * face);
gboolean gnome_font_face_is_fixed_width (const GnomeFontFace * face);
const gchar * gnome_font_face_get_sample (const GnomeFontFace * face);
GnomeFontFace * gnome_font_unsized_closest (const char *family_name,  
                                            GnomeFontWeight weight,
                                            gboolean italic);


END_GNOME_DECLS

#endif
