#define _GNOME_FONT_FACE_FT2_C_

/*
 * GnomeFontFaceFT2
 *
 * Standard TTF font file handler
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include "gf-freetype2.h"
#include "../gnome-font-glyph-private.h"
#include "gnome-font-face-ft2.h"
#include "gnome-font-ft2.h"
#include "gnome-font-glyph-ft2.h"

#define GFF_FT2_VERBOSE

static void gft_class_init (GnomeFontFaceFT2Class * klass);
static void gft_init (GnomeFontFaceFT2 * face);

static void gft_destroy (GtkObject * object);

/* GnomeVFont methods */

static gboolean gft_info (GnomeVFont * vfont);
static GnomeFontGlyph * gft_get_glyph (GnomeVFont * vfont, gint code);

/* GnomeFontFace methods */

static GnomeFontFaceDesc * gft_description (GnomeFontFace * face, const gchar * language);
static GnomeFont * gft_get_font (GnomeFontFace * face, gdouble size, gdouble transform[]);
static gboolean gft_set_encoding (GnomeFontFace * face, gint id, const gchar * encoding);
static gint gft_lookup (GnomeFontFace * face, gint encoding, gint code);
guchar * gffft2_get_ps_object (const GnomeFontFace * face, const guchar * psname);
guchar * gffft2_encapsulate_type42 (GnomeFontFaceFT2 * fft2, const gchar * reference);

static GHashTable * pathdict = NULL;
static GnomeFontFaceClass * parent_class = NULL;

GtkType
gnome_font_face_ft2_get_type (void)
{
	static GtkType face_type = 0;
	if (!face_type) {
		GtkTypeInfo face_info = {
			"GnomeFontFaceFT2",
			sizeof (GnomeFontFaceFT2),
			sizeof (GnomeFontFaceFT2Class),
			(GtkClassInitFunc) gft_class_init,
			(GtkObjectInitFunc) gft_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};
		face_type = gtk_type_unique (gnome_font_face_get_type (), &face_info);
	}
	return face_type;
}

static void
gft_class_init (GnomeFontFaceFT2Class * klass)
{
	GtkObjectClass * object_class;
	GnomeVFontClass * vfont_class;
	GnomeFontFaceClass * face_class;

	object_class = (GtkObjectClass *) klass;
	vfont_class = (GnomeVFontClass *) klass;
	face_class = (GnomeFontFaceClass *) klass;

	parent_class = gtk_type_class (gnome_font_face_get_type ());

	object_class->destroy = gft_destroy;

	vfont_class->info = gft_info;
	vfont_class->glyph = gft_get_glyph;

	face_class->description = gft_description;
	face_class->get_font = gft_get_font;
	face_class->set_encoding = gft_set_encoding;
	face_class->lookup = gft_lookup;
	face_class->get_ps_object = gffft2_get_ps_object;
}

static void
gft_init (GnomeFontFaceFT2 * face)
{
	/* Nothing interesting */
}

static void
gft_destroy (GtkObject * object)
{
	GnomeFontFaceFT2 * face;

	face = (GnomeFontFaceFT2 *) object;

	if (face->path) {
		g_hash_table_remove (pathdict, face->path);
		g_free (face->path);
	}

	FT_Done_Face (face->ft_face);

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

/* GnomeVFont methods */

static gboolean
gft_info (GnomeVFont * vfont)
{
	GnomeFontFaceFT2 * ftface;
	FT_Face ftf;
	gdouble scale;

	ftface = GNOME_FONT_FACE_FT2 (vfont);
	ftf = ftface->ft_face;
	scale = 1000.0 / ftf->units_per_EM;

	vfont->num_glyphs = ftface->ft_face->num_glyphs;

	vfont->bbox.x0 = ftf->bbox.xMin * scale;
	vfont->bbox.y0 = ftf->bbox.yMin * scale;
	vfont->bbox.x1 = ftf->bbox.xMax * scale;
	vfont->bbox.y1 = ftf->bbox.yMax * scale;

	vfont->ascender = ftf->ascender * scale;
	vfont->descender = ftf->descender * scale;
	vfont->height = ftf->height * scale;

	return TRUE;
}

static GnomeFontGlyph *
gft_get_glyph (GnomeVFont * vfont, gint code)
{
	GnomeFontFaceFT2 * ftface;
	GnomeFontGlyph * glyph;

	ftface = (GnomeFontFaceFT2 *) vfont;

	g_return_val_if_fail (code < ftface->ft_face->num_glyphs, NULL);

	glyph = gnome_font_glyph_ft2_new (vfont,
		ftface->ft_face,
		code,
		GFGFT_TYPE_FACE);

	return glyph;
}

static GnomeFontFaceDesc *
gft_description (GnomeFontFace * face, const gchar * language)
{
	GnomeFontFaceFT2 * ftface;
	GnomeFontFaceDesc * desc;

	ftface = (GnomeFontFaceFT2 *) face;

	desc = g_new0 (GnomeFontFaceDesc, 1);

	desc->language = g_strdup (language);
	desc->family = g_strdup (ftface->ft_face->family_name);
	desc->species = g_strdup (ftface->ft_face->style_name);
	desc->name = g_strconcat (desc->family, " ", desc->species, NULL);

	return desc;
}

static GnomeFont *
gft_get_font (GnomeFontFace * face, gdouble size, gdouble transform[])
{
	GnomeFontFaceFT2 * ftface;
	GnomeFont * font;

	ftface = (GnomeFontFaceFT2 *) face;

	/* fixme: implement caching */

	font = gnome_font_ft2_new (face, size, transform);
	g_return_val_if_fail (font != NULL, NULL);

	return font;
}

/* NB! HIGHLY EXPERIMENTAL */

static gboolean
gft_set_encoding (GnomeFontFace * face, gint id, const gchar * encoding)
{
	GnomeFontFaceFT2 * ftface;
#if 0
	FT_UShort pid, eid;
	gint i;
#endif

	ftface = (GnomeFontFaceFT2 *) face;

	if (strcmp (encoding, "iso-8859-1") != 0) return FALSE;

#if 0
	for (i = 0; i < priv->ft_face.num_charMaps; i++) {
		FT_Get_CharMap_ID (priv->tt_face, i, &pid, &eid);
		g_print ("charmap %d %d %d\n", i, pid, eid);
		if ((pid == 3) && (eid == 1)) {
			/* MS Unicode? */
			g_print ("found\n");
			FT_Get_CharMap (priv->tt_face, i, &priv->isomap);
			return TRUE;
		}
	}
#endif

	return TRUE;
}

/* fixme: API test */

static gint
gft_lookup (GnomeFontFace * face, gint encoding, gint code)
{
	GnomeFontFaceFT2 * ftface;

	ftface = (GnomeFontFaceFT2 *) face;

	return FT_Get_Char_Index (ftface->ft_face, code);
}

/* Private & temporary methods */

GnomeFontFace *
gnome_font_face_ft2_new_from_file (const gchar * path, gint index)
{
	GnomeFontFaceFT2 * face;
	FT_Face ft_face;
	FT_Error result;

	g_return_val_if_fail (path != NULL, NULL);
	g_return_val_if_fail (index >= 0, NULL);
	g_return_val_if_fail (GFFT, NULL);

	/* Test, if given path is already loaded */

	if (pathdict == NULL) pathdict = g_hash_table_new (g_str_hash, g_str_equal);

	face = g_hash_table_lookup (pathdict, path);

	if (face != NULL) {
		g_assert (GNOME_IS_FONT_FACE_FT2 (face));
#ifdef GFF_FT2_VERBOSE
		g_print ("Found already loaded face: %s\n", path);
#endif
		gnome_font_face_ref ((GnomeFontFace *) face);
		return (GnomeFontFace *) face;
	}

	/* Open FreeType face handle */

	result = FT_New_Face (GFFT_LIBRARY, path, index, &ft_face);

	if (result != FT_Err_Ok) {
		g_warning ("Cannot open typeface file %s", path);
		return NULL;
	}

	/* Test face */

	if (ft_face == NULL) {
		g_warning ("Problem with FreeType (no error but face is NULL)");
		return NULL;
	}

	if (!FT_IS_SCALABLE (ft_face)) {
		FT_Done_Face (face->ft_face);
		return NULL;
	}

#if 0
	{
		FT_UInt nn;
		nn = FT_Get_Sfnt_Name_Count (ft_face);
		g_print ("Num names: %d\n", nn);
	}
#endif

	/* Set attributes */
#if 0
	FT_Set_Char_Size (ft_face, 6400, 6400, 720, 720);
#endif
	FT_Select_Charmap (ft_face, ft_encoding_unicode);

	/* Create Gnome face object */

	face = gtk_type_new (GNOME_TYPE_FONT_FACE_FT2);

	face->ft_face = ft_face;
	face->path = g_strdup (path);

	g_hash_table_insert (pathdict, face->path, face);

	/* fixme: register loaded face */
	/* fixme: handle collections */
	/* fixme: close face on destroy */
	/* fixme: kill glyphs on destroy */

	return (GnomeFontFace *) face;
}

gint
gnome_font_face_ft2_get_num_faces (GnomeFontFaceFT2 * face)
{
	g_return_val_if_fail (face != NULL, 0);
	g_return_val_if_fail (GNOME_IS_FONT_FACE_FT2 (face), 0);

	return face->ft_face->num_faces;
}

/*
 * fixme: Should be able to subset (probably additional arguments)
 */

guchar *
gffft2_get_ps_object (const GnomeFontFace * face, const guchar * psname)
{
	GnomeFontFaceFT2 * fft2;

	fft2 = GNOME_FONT_FACE_FT2 (face);

	switch (face->entry->type) {
	case GF_FONT_ENTRY_TYPE1:
	case GF_FONT_ENTRY_TYPE1_ALIAS:
		/* Encapsulate pfa */
		break;
	case GF_FONT_ENTRY_TRUETYPE:
		return gffft2_encapsulate_type42 (GNOME_FONT_FACE_FT2 (face), psname);
		/* Encapsulate type42 */
		break;
	default:
		return NULL;
		break;
	}

	return NULL;
}

/*
 * References:
 * Adobe Inc., PostScript Language Reference, 3rd edition, Addison Wesley 1999
 * Adobe Inc., The Type42 Font Format Specification, 1998 <http://partners.adobe.com/asn/developer/PDFS/TN/5012.Type42_Spec.pdf>
 */

/*
 * fixme: This is simply example adapted for gnome-font
 */

#define EOL "\n"

guchar *
gffft2_encapsulate_type42 (GnomeFontFaceFT2 * fft2, const gchar * reference)
{
	struct stat st;
	gint fh;
	guchar * fbuf;
	guchar * b, * s;
	const gchar * psname;
	gdouble TTVersion, MfrRevision;
	ArtDRect bbox;
	gint i;
	gint nglyphs, nfonts;

	if (stat (fft2->path, &st) < 0) return NULL;
	if (st.st_size > 0xfff0) {
		/* fixme: */
		g_print ("Downloading TrueType fonts bigger than 65K not supported yet\n");
		return NULL;
	}
	fh = open (fft2->path, O_RDONLY);
	if (fh < 0) return NULL;
	fbuf = mmap (NULL, st.st_size, PROT_READ, MAP_SHARED, fh, 0);
	close (fh);
	if (fbuf == (gpointer) -1) return NULL;



	/* fixme: */
	b = s = g_new (guchar, 2 * st.st_size + 0x10000);

	/* fixme: */
	/* fixme: locale */
	psname = gnome_font_face_get_ps_name (GNOME_FONT_FACE (fft2));
	TTVersion = 1.0;
	MfrRevision = 1.0;
	s += sprintf (s, "%%!PS-TrueTypeFont-%g-%g\n", TTVersion, MfrRevision);
	s += sprintf (s, "11 dict begin\n");
	s += sprintf (s, "/FontName /%s def\n", psname);
	s += sprintf (s, "/Encoding 256 array\n");
	s += sprintf (s, "0 1 255 {1 index exch /.notdef put} for\n");
	s += sprintf (s, "readonly def\n");
	s += sprintf (s, "/PaintType 0 def\n");
	/* fixme: Which matrix to use? */
	s += sprintf (s, "/FontMatrix [1 0 0 1 0 0] def\n");
	/* fixme: */
	gnome_font_face_get_stdbbox (GNOME_FONT_FACE (fft2), &bbox);
	s += sprintf (s, "/FontBBox [%g %g %g %g] def\n", bbox.x0, bbox.y0, bbox.x1, bbox.y1);
	s += sprintf (s, "/FontType 42 def\n");
	/* fixme: XUID */
	/* fixme: */
	s += sprintf (s, "/sfnts [<\n");
	for (i = 0; i < st.st_size; i+= 32) {
		gint e, j;
		e = MIN (i + 32, st.st_size);
		for (j = i; j < e; j++) {
			s += sprintf (s, "%.2x", *(fbuf + j));
		}
		s += sprintf (s, "\n");
	}
	s += sprintf (s, "00>] def\n");
	/* fixme: */
	s += sprintf (s, "/CharStrings %d dict dup begin\n", (gint) fft2->ft_face->num_glyphs);
	s += sprintf (s, "/.notdef 0 def\n");
	for (i = 1; i < fft2->ft_face->num_glyphs; i++) {
		s += sprintf (s, "/g%d %d def\n", i, i);
	}
	s += sprintf (s, "end readonly def\n");
	s += sprintf (s, "FontName currentdict end definefont pop\n");

	munmap (fbuf, st.st_size);

	/*
	 * We have font downloaded (hopefully)
	 *
	 * With CharStrings: .nodef g1 g2 g3 g4 g5...
	 *
	 * Now we have to build usable 2-byte encoded font from it
	 *
	 */

	/* fixme: That is crap. We should use CID */

	/* Bitch 'o' bitches (Lauris) ! */

	nglyphs = fft2->ft_face->num_glyphs;
	nfonts = (nglyphs + 255) >> 8;

	s += sprintf (s, "32 dict begin" EOL);

	/* Common entries */

	s += sprintf (s, "/FontType 0 def" EOL);
	s += sprintf (s, "/FontMatrix [1 0 0 1 0 0] def" EOL);
	s += sprintf (s, "/FontName /%s-Glyph-Composite def" EOL, psname);
	s += sprintf (s, "/LanguageLevel 2 def" EOL);

	/* Type 0 entries */

	s += sprintf (s, "/FMapType 2 def" EOL);

	/* Bitch 'o' bitches */

	s += sprintf (s, "/FDepVector [" EOL);

	for (i = 0; i < nfonts; i++) {
		gint j;

		s += sprintf (s, "/%s findfont" EOL, psname);
		s += sprintf (s, "dup length dict begin {1 index /FID ne {def} {pop pop} ifelse} forall" EOL);
		s += sprintf (s, "/Encoding [" EOL);

		for (j = 0; j < 256; j++) {
			gint glyph;
			glyph = 256 * i + j;
			if ((glyph > 0) && (glyph < nglyphs)) {
				s += sprintf (s, "/g%d ", glyph);
			} else {
				s += sprintf (s, "/.notdef ");
			}
			if ((j & 0x0f) == 0x0f) s += sprintf (s, EOL);
		}

		s += sprintf (s, "] def" EOL);
		s += sprintf (s, "currentdict end /%s-Glyph-Page-%d exch definefont" EOL, psname, i);
	}

	s += sprintf (s, "] def" EOL);
	s += sprintf (s, "/Encoding [" EOL);

	for (i = 0; i < 256; i++) {
		gint fn;
		fn = (i < nfonts) ? i : 0;
		s += sprintf (s, ((i & 0x0f) == 0x0f) ? "%d" EOL : "%d  ", fn);
	}

	s += sprintf (s, "] def" EOL);
	s += sprintf (s, "currentdict end" EOL);
	s += sprintf (s, "/Uni-%s exch definefont pop" EOL, psname);

	return b;
}


