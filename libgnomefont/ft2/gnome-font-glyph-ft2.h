#ifndef GNOME_FONT_GLYPH_FT2_H
#define GNOME_FONT_GLYPH_FT2_H

/*
 * GnomeFontGlyphFT2
 *
 * FreeType glyph
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#define GNOME_TYPE_FONT_GLYPH_FT2		(gnome_font_glyph_ft2_get_type ())
#define GNOME_FONT_GLYPH_FT2(obj)		(GTK_CHECK_CAST ((obj), GNOME_TYPE_FONT_GLYPH_FT2, GnomeFontGlyphFT2))
#define GNOME_FONT_GLYPH_FT2_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_FONT_GLYPH_FT2, GnomeFontGlyphFT2Class))
#define GNOME_IS_FONT_GLYPH_FT2(obj)		(GTK_CHECK_TYPE ((obj), GNOME_TYPE_FONT_GLYPH_FT2))
#define GNOME_IS_FONT_GLYPH_FT2_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_FONT_GLYPH_FT2))

typedef struct _GnomeFontGlyphFT2	GnomeFontGlyphFT2;
typedef struct _GnomeFontGlyphFT2Class	GnomeFontGlyphFT2Class;

#include "../gnome-font-glyph-private.h"

typedef enum {
	GFGFT_TYPE_NONE,
	GFGFT_TYPE_FACE,
	GFGFT_TYPE_FONT,
	GFGFT_TYPE_RFONT
} GFGFTType;

struct _GnomeFontGlyphFT2 {
	GnomeFontGlyph glyph;

	GFGFTType type;
	guint has_metrics : 1;
	guint err_metrics : 1;
	guint can_outline : 1;
	guint err_outline : 1;
	guint can_graymap : 1;
	guint err_graymap : 1;
#if 0
	FT_Glyph tt_glyph;
	FT_Big_Glyph_Metrics tt_bigmetrics;
#endif
	ArtDRect bbox;
	ArtPoint advance;
	ArtBpath * bpath;

	/*
	 * This is remainder matrix, transforming already scaled glyph
	 * to real raster (rotation etc.)
	 */

	GFGGrayMap graymap;
	GFGGdkMap gdkmap;
};

struct _GnomeFontGlyphFT2Class {
	GnomeFontGlyphClass parent_class;
};

GtkType gnome_font_glyph_ft2_get_type (void);

GnomeFontGlyph * gnome_font_glyph_ft2_new (GnomeVFont * vfont, FT_Face face, gint code, GFGFTType type);

END_GNOME_DECLS

#endif
