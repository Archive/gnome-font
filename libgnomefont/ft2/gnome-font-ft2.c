#define _GNOME_FONT_FT2_C_

/*
 * GnomeFontFT2
 *
 * FreeType font
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <math.h>
#include <string.h>
#include "../gnome-font-glyph-private.h"
#include "gf-freetype2.h"
#include "gnome-font-face-ft2.h"
#include "gnome-font-ft2.h"
#include "gnome-rfont-ft2.h"
#include "gnome-font-glyph-ft2.h"

static void gnome_font_ft2_class_init (GnomeFontFT2Class * klass);
static void gnome_font_ft2_init (GnomeFontFT2 * group);

static void gnome_font_ft2_destroy (GtkObject * object);

static gboolean gft2_info (GnomeVFont * vfont);
static GnomeFontGlyph * gnome_font_ft2_get_glyph (GnomeVFont * vfont, gint code);
#if 0
static ArtPoint * gnome_font_ft2_kerning (GnomeVFont * vfont, gint c1, gint c2, ArtPoint * p);
#endif

static GnomeRFont * gnome_font_ft2_get_rfont (GnomeFont * font, gdouble affine[]);

static GnomeFontClass * parent_class = NULL;

GtkType
gnome_font_ft2_get_type (void)
{
	static GtkType group_type = 0;
	if (!group_type) {
		GtkTypeInfo group_info = {
			"GnomeFontFT2",
			sizeof (GnomeFontFT2),
			sizeof (GnomeFontFT2Class),
			(GtkClassInitFunc) gnome_font_ft2_class_init,
			(GtkObjectInitFunc) gnome_font_ft2_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};
		group_type = gtk_type_unique (gnome_font_get_type (), &group_info);
	}
	return group_type;
}

static void
gnome_font_ft2_class_init (GnomeFontFT2Class * klass)
{
	GtkObjectClass * object_class;
	GnomeVFontClass * vfont_class;
	GnomeFontClass * font_class;

	object_class = (GtkObjectClass *) klass;
	vfont_class = (GnomeVFontClass *) klass;
	font_class = (GnomeFontClass *) klass;

	parent_class = gtk_type_class (gnome_font_get_type ());

	object_class->destroy = gnome_font_ft2_destroy;

	vfont_class->info = gft2_info;
	vfont_class->glyph = gnome_font_ft2_get_glyph;
#if 0
	vfont_class->kerning = gnome_font_ft2_kerning;
#endif

	font_class->get_rfont = gnome_font_ft2_get_rfont;
}

static void
gnome_font_ft2_init (GnomeFontFT2 * font)
{
	/* Nothing */
}

/* fixme: fixme fixme fixme */

static void
gnome_font_ft2_destroy (GtkObject * object)
{
	GnomeFontFT2 * font;

	font = (GnomeFontFT2 *) object;

	FT_Done_Face (font->ft_face);

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

/* fixme: */

static gboolean
gft2_info (GnomeVFont * vfont)
{
	GnomeFontFaceFT2 * ftface;

	ftface = GNOME_FONT_FACE_FT2 (GNOME_FONT (vfont)->face);

	vfont->num_glyphs = ftface->ft_face->num_glyphs;
	vfont->bbox.x0 = vfont->bbox.y0 = 0.0;
	vfont->bbox.x1 = vfont->bbox.y1 = 1000.0;

	return TRUE;
}

static GnomeFontGlyph *
gnome_font_ft2_get_glyph (GnomeVFont * vfont, gint code)
{
	GnomeFontFT2 * ftfont;
	GnomeFontGlyph * glyph;

	ftfont = (GnomeFontFT2 *) vfont;

	g_return_val_if_fail (code < ftfont->ft_face->num_glyphs, NULL);

	glyph = gnome_font_glyph_ft2_new (vfont,
		ftfont->ft_face,
		code,
		GFGFT_TYPE_FONT);

	return glyph;
}

#if 0
static ArtPoint *
gnome_font_ft2_kerning (GnomeVFont * vfont, gint c1, gint c2, ArtPoint * p)
{
	GnomeFontFT2 * ftfont;
	GnomeFontFT2Private * priv;
	FT_Vector kv;

	ftfont = (GnomeFontFT2 *) vfont;
	priv = ftfont->private;

	FT_Get_Kerning (priv->ft_face, c1, c2, ft_kerning_default, &kv);

#if 0
	p->x = (gdouble) kv.x * priv->dsize / priv->ft_face->units_per_EM;
	p->y = (gdouble) kv.y * priv->dsize / priv->ft_face->units_per_EM;
#else
	/* fixme: */
	p->x = (gdouble) kv.x;
	p->y = (gdouble) kv.y;
#endif

	return p;
}
#endif

/* fixme: */

static GnomeRFont *
gnome_font_ft2_get_rfont (GnomeFont * font, gdouble affine[])
{
	GnomeFontFT2 * ftfont;
	GnomeRFont * rfont;

	ftfont = (GnomeFontFT2 *) font;

	rfont = gnome_rfont_ft2_new (font, affine);
	g_return_val_if_fail (rfont != NULL, NULL);

	return rfont;
}

/* Methods */

GnomeFont *
gnome_font_ft2_new (GnomeFontFace * face, gdouble size, gdouble transform[])
{
	GnomeFontFaceFT2 * ft2face;
	GnomeFont * font;
	GnomeFontFT2 * ft2font;
	FT_Error result;

	g_return_val_if_fail (size >= (1 / 72.0), NULL);

	ft2face = (GnomeFontFaceFT2 *) face;

	font = gtk_type_new (GNOME_TYPE_FONT_FT2);
	ft2font = (GnomeFontFT2 *) font;

	/* fixme: add rounding remainder to transformation matrix */

	ft2font->size = (gint) (font->size * 64.0 + 0.5);
	ft2font->dsize = (gdouble) ft2font->size / 64.0;

	/* Find best approximation of resolution */

	ft2font->dresx = rint (hypot (transform[0], transform[1]) * 72.0);
	ft2font->dresy = rint (hypot (transform[2], transform[3]) * 72.0);
	ft2font->resx = (gint) ft2font->dresx;
	ft2font->resy = (gint) ft2font->dresy;

	/* Find remainder matrix */

	ft2font->drm[0] = transform[0] * 72.0 / ft2font->dresx;
	ft2font->drm[1] = transform[1] * 72.0 / ft2font->dresx;
	ft2font->drm[2] = transform[2] * 72.0 / ft2font->dresy;
	ft2font->drm[3] = transform[3] * 72.0 / ft2font->dresy;
	ft2font->drm[4] = ft2font->drm[5] = 0.0;

	ft2font->tt_m.xx = rint (ft2font->drm[0] * 65536.0);
	ft2font->tt_m.xy = rint (ft2font->drm[1] * 65536.0);
	ft2font->tt_m.yx = rint (ft2font->drm[2] * 65536.0);
	ft2font->tt_m.yy = rint (ft2font->drm[3] * 65536.0);

	/* Now we have font transformation matrixes set up :) */

	/* fixme: error checking */

	result = FT_New_Face (GFFT_LIBRARY,
		ft2face->path,
		ft2face->ft_face->face_index,
		&ft2font->ft_face);

	FT_Set_Char_Size (ft2font->ft_face, ft2font->size, ft2font->size, ft2font->resx, ft2font->resy);
	FT_Select_Charmap (ft2font->ft_face, ft_encoding_unicode);

	return font;
}


