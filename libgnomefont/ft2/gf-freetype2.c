#define GF_FREETYPE2_C

/*
 * FreeType library plug
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <string.h>
#include <unicode.h>
#include <libart_lgpl/art_affine.h>
#include "gf-freetype2.h"

#include <freetype/ttnameid.h>

#if 0
static gchar * gfft_strdup_mac_utf8 (FT_String * src, FT_UShort length, GFFT_Locale * loc);
static gchar * gfft_strdup_utf16_utf8 (FT_String * src, FT_UShort length, GFFT_Locale * loc);
#endif

gboolean
gf_freetype2_init (void)
{
	FT_Error result;

	if (gfft2_initialized) return TRUE;

	result = FT_Init_FreeType (&gnome_font_FT_Library);
	if (result != FT_Err_Ok) return FALSE;

	gfft2_initialized = TRUE;

	return TRUE;
}

#if 0
/* fixme: either implement or rethink this */

static GFFT_Locale gfl[] = {
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_DEFAULT, TT_MAC_LANGID_ENGLISH},
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_UNICODE_2_0, TT_MAC_LANGID_ENGLISH},
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_UNICODE_1_1, TT_MAC_LANGID_ENGLISH},
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_ISO_10646, TT_MAC_LANGID_ENGLISH},

	{TT_PLATFORM_MACINTOSH, TT_MAC_ID_ROMAN, TT_MAC_LANGID_ENGLISH},

	{TT_PLATFORM_MICROSOFT, TT_MS_ID_UNICODE_CS, TT_MS_LANGID_ENGLISH_UNITED_STATES}
};
gint gfl_length = (sizeof (gfl) / sizeof (gfl[0]));

static GFFT_Locale gfl_fr_FR[] = {
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_DEFAULT, TT_MAC_LANGID_FRENCH},
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_UNICODE_2_0, TT_MAC_LANGID_FRENCH},
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_UNICODE_1_1, TT_MAC_LANGID_FRENCH},
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_ISO_10646, TT_MAC_LANGID_FRENCH},

	{TT_PLATFORM_MACINTOSH, TT_MAC_ID_ROMAN, TT_MAC_LANGID_FRENCH},

	{TT_PLATFORM_MICROSOFT, TT_MS_ID_UNICODE_CS, TT_MS_LANGID_FRENCH_FRANCE}
};
gint gfl_fr_FR_length = (sizeof (gfl_fr_FR) / sizeof (gfl_fr_FR[0]));

const GSList *
gf_freetype_locale_preference_list (const gchar * language)
{
	static GSList * l = NULL;
	gint i;

	/* Fake locale preferences */

	if (strcmp (language, "fr_FR") == 0) {
		for (i = gfl_fr_FR_length - 1; i >= 0; i--) {
			l = g_slist_prepend (l, &gfl_fr_FR[i]);
		}
		return l;
	}

	if (l == NULL) {
		for (i = gfl_length - 1; i >= 0; i--) {
			l = g_slist_prepend (l, &gfl[i]);
		}
	}

	return l;
}

/* fixme: */

gchar *
gf_freetype_strdup_utf8 (FT_String * src, FT_UShort length, GFFT_Locale * loc)
{
	g_assert (loc != NULL);

	switch (loc->PlatformID) {
	case TT_PLATFORM_APPLE_UNICODE:
	case TT_PLATFORM_MICROSOFT:
	case TT_PLATFORM_ISO:
		return gfft_strdup_utf16_utf8 (src, length, loc);
		break;
	case TT_PLATFORM_MACINTOSH:
		return gfft_strdup_mac_utf8 (src, length, loc);
		break;
	default:
		g_assert_not_reached ();
		break;
	}

	return NULL;
}

/* fixme: do real thing */

static gchar *
gfft_strdup_mac_utf8 (FT_String * src, FT_UShort length, GFFT_Locale * loc)
{
	gchar * dst;

	dst = g_new (gchar, (length + 1) * sizeof (gchar));
	memcpy (dst, src, length * sizeof (gchar));
	* (dst + length) = '\0';

	return dst;
}

/* fixme: */

static gchar *
gfft_strdup_utf16_utf8 (FT_String * src, FT_UShort length, GFFT_Locale * loc)
{
	static gboolean uni_initialized = FALSE;
	static unicode_iconv_t uiconv = NULL;
	gchar * tmp, * dst;
	const char * ip;
	char * op;
	size_t il, ol;

	if (!uni_initialized) {
		unicode_init ();
		uiconv = unicode_iconv_open ("UTF-8", "UTF-16");
		if (uiconv == (unicode_iconv_t) -1) {
			g_warning ("Error creating UTF-16 -> UTF-8 converter");
			return NULL;
		}
		uni_initialized = TRUE;
	}

	ip = src;
	il = length;

	tmp = g_new0 (gchar, length * 8 + 1);
	op = tmp;
	ol = length * 8;

	unicode_iconv (uiconv, &ip, &il, &op, &ol);

	dst = g_strdup (tmp);

	g_free (tmp);

	return dst;
}
#endif

/* Bpath methods */

typedef struct {
	ArtBpath * bp;
	gint start, end;
	gdouble * t;
} GFFT2OutlineData;

#if 0
static void gfft2_close_path (GFFT2OutlineData * od)
{
	if ((od->bp[od->end - 1].x3 != od->bp[od->start].x3) || (od->bp[od->end - 1].y3 != od->bp[od->start].y3)) {
		od->bp[od->end].code = ART_LINETO;
		od->bp[od->end].x3 = od->bp[od->start].x3;
		od->bp[od->end].y3 = od->bp[od->start].y3;
		od->start = ++od->end;
	}
}
#endif

static int gfft2_move_to (FT_Vector * to, void * user)
{
	GFFT2OutlineData * od;
	ArtBpath * s;
	ArtPoint p;

	od = (GFFT2OutlineData *) user;

	s = &od->bp[od->end - 1];

	p.x = to->x * od->t[0] + to->y * od->t[2];
	p.y = to->x * od->t[1] + to->y * od->t[3];

	if ((p.x != s->x3) || (p.y != s->y3)) {
		od->bp[od->end].code = ART_MOVETO;
		od->bp[od->end].x3 = to->x * od->t[0] + to->y * od->t[2];
		od->bp[od->end].y3 = to->x * od->t[1] + to->y * od->t[3];
		od->end++;
	}

	return 0;
}

static int gfft2_line_to (FT_Vector * to, void * user)
{
	GFFT2OutlineData * od;
	ArtBpath * s;
	ArtPoint p;

	od = (GFFT2OutlineData *) user;

	s = &od->bp[od->end - 1];

	p.x = to->x * od->t[0] + to->y * od->t[2];
	p.y = to->x * od->t[1] + to->y * od->t[3];

	if ((p.x != s->x3) || (p.y != s->y3)) {
		od->bp[od->end].code = ART_LINETO;
		od->bp[od->end].x3 = to->x * od->t[0] + to->y * od->t[2];
		od->bp[od->end].y3 = to->x * od->t[1] + to->y * od->t[3];
		od->end++;
	}

	return 0;
}

static int gfft2_conic_to (FT_Vector * control, FT_Vector * to, void * user)
{
	GFFT2OutlineData * od;
	ArtBpath * s, * e;
	ArtPoint c;

	od = (GFFT2OutlineData *) user;
	g_return_val_if_fail (od->end > 0, -1);

	s = &od->bp[od->end - 1];
	e = &od->bp[od->end];

	e->code = ART_CURVETO;

	c.x = control->x * od->t[0] + control->y * od->t[2];
	c.y = control->x * od->t[1] + control->y * od->t[3];
	e->x3 = to->x * od->t[0] + to->y * od->t[2];
	e->y3 = to->x * od->t[1] + to->y * od->t[3];

	od->bp[od->end].x1 = c.x - (c.x - s->x3) / 3;
	od->bp[od->end].y1 = c.y - (c.y - s->y3) / 3;
	od->bp[od->end].x2 = c.x + (e->x3 - c.x) / 3;
	od->bp[od->end].y2 = c.y + (e->y3 - c.y) / 3;
	od->end++;

	return 0;
}

static int gfft2_cubic_to (FT_Vector * control1, FT_Vector * control2, FT_Vector * to, void * user)
{
	GFFT2OutlineData * od;

	od = (GFFT2OutlineData *) user;

	od->bp[od->end].code = ART_CURVETO;
	od->bp[od->end].x1 = control1->x * od->t[0] + control1->y * od->t[2];
	od->bp[od->end].y1 = control1->x * od->t[1] + control1->y * od->t[3];
	od->bp[od->end].x2 = control2->x * od->t[0] + control2->y * od->t[2];
	od->bp[od->end].y2 = control2->x * od->t[1] + control2->y * od->t[3];
	od->bp[od->end].x3 = to->x * od->t[0] + to->y * od->t[2];
	od->bp[od->end].y3 = to->x * od->t[1] + to->y * od->t[3];
	od->end++;

	return 0;
}

FT_Outline_Funcs gfft2_outline_funcs = {
	gfft2_move_to,
	gfft2_line_to,
	gfft2_conic_to,
	gfft2_cubic_to,
	0, 0
};

/*
 * We support only 4x4 matrix here (do you need more?)
 */

ArtBpath *
gf_freetype2_bpath_from_outline (FT_Outline * ol, gdouble transform[])
{
	GFFT2OutlineData od;

	od.bp = g_new (ArtBpath, ol->n_points * 2 + ol->n_contours + 1);
	od.start = od.end = 0;
	od.t = transform;

	FT_Outline_Decompose (ol, &gfft2_outline_funcs, &od);

#if 0
	if (od.end > 0) gfft2_close_path (&od);
#endif

	od.bp[od.end].code = ART_END;

	/* fixme: g_renew */

	return od.bp;
}




