#ifndef GNOME_FONT_FT2_H
#define GNOME_FONT_FT2_H

/*
 * GnomeFontFT2
 *
 * Freetype font
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#define GNOME_TYPE_FONT_FT2		(gnome_font_ft2_get_type ())
#define GNOME_FONT_FT2(obj)		(GTK_CHECK_CAST ((obj), GNOME_TYPE_FONT_FT2, GnomeFontFT2))
#define GNOME_FONT_FT2_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_FONT_FT2, GnomeFontFT2Class))
#define GNOME_IS_FONT_FT2(obj)	(GTK_CHECK_TYPE ((obj), GNOME_TYPE_FONT_FT2))
#define GNOME_IS_FONT_FT2_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_FONT_FT2))

typedef struct _GnomeFontFT2		GnomeFontFT2;
typedef struct _GnomeFontFT2Class	GnomeFontFT2Class;

#include <freetype/freetype.h>
#include "../gnome-font-private.h"

struct _GnomeFontFT2 {
	GnomeFont font;
	/* FreeType Face object */
	FT_Face ft_face;
	/*
	 * Font size for FT2 library
	 */
	gdouble dsize;
	FT_F26Dot6 size;
	/*
	 * Resolution values for FreeType library
	 * pixels_size = font_size * resolution / 72.0
	 */
	gdouble dresx, dresy;
	FT_UShort resx, resy;
	/*
	 * Remainder transformation matrix
	 * pixel_size * reminder = actual transform
	 */
	gdouble drm[6];
	FT_Matrix tt_m;
};

struct _GnomeFontFT2Class {
	GnomeFontClass parent_class;
};

GtkType gnome_font_ft2_get_type (void);

GnomeFont * gnome_font_ft2_new (GnomeFontFace * face, gdouble size, gdouble transform[]);

#define GF_FONT_FT2_FT_FACE(f) (((GnomeFontFaceFT2 *)((GnomeFont *) f)->face)->ft_face)

END_GNOME_DECLS

#endif


