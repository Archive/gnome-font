#ifndef _GNOME_FONT_FACE_FT2_H_
#define _GNOME_FONT_FACE_FT2_H_

/*
 * GnomeFontFaceFT2
 *
 * Standard TTF file handler
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#define GNOME_TYPE_FONT_FACE_FT2		(gnome_font_face_ft2_get_type ())
#define GNOME_FONT_FACE_FT2(obj)		(GTK_CHECK_CAST ((obj), GNOME_TYPE_FONT_FACE_FT2, GnomeFontFaceFT2))
#define GNOME_FONT_FACE_FT2_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_FONT_FACE_FT2, GnomeFontFaceFT2Class))
#define GNOME_IS_FONT_FACE_FT2(obj)		(GTK_CHECK_TYPE ((obj), GNOME_TYPE_FONT_FACE_FT2))
#define GNOME_IS_FONT_FACE_FT2_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_FONT_FACE_FT2))

typedef struct _GnomeFontFaceFT2 GnomeFontFaceFT2;
typedef struct _GnomeFontFaceFT2Class GnomeFontFaceFT2Class;

#include <freetype/freetype.h>
#include "../gnome-font-face-private.h"

struct _GnomeFontFaceFT2 {
	GnomeFontFace face;

	/* Font filename */
	gchar * path;

	/* FT handle */
	FT_Face ft_face;
};

struct _GnomeFontFaceFT2Class {
	GnomeFontFaceClass parent_class;
};

GtkType gnome_font_face_ft2_get_type (void);

GnomeFontFace * gnome_font_face_ft2_new_from_file (const gchar * path, gint face);

/*
 * Methods for font install and query
 */

gint gnome_font_face_ft2_get_num_faces (GnomeFontFaceFT2 * face);

END_GNOME_DECLS

#endif


