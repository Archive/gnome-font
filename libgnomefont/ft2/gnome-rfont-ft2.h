#ifndef GNOME_RFONT_FT2_H
#define GNOME_RFONT_FT2_H

/*
 * GnomeRFontFT2
 *
 * Freetype rasterized font implementation
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#define GNOME_TYPE_RFONT_FT2		(gnome_rfont_ft2_get_type ())
#define GNOME_RFONT_FT2(obj)		(GTK_CHECK_CAST ((obj), GNOME_TYPE_RFONT_FT2, GnomeRFontFT2))
#define GNOME_RFONT_FT2_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_RFONT_FT2, GnomeRFontFT2Class))
#define GNOME_IS_RFONT_FT2(obj)		(GTK_CHECK_TYPE ((obj), GNOME_TYPE_RFONT_FT2))
#define GNOME_IS_RFONT_FT2_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_RFONT_FT2))

typedef struct _GnomeRFontFT2		GnomeRFontFT2;
typedef struct _GnomeRFontFT2Class	GnomeRFontFT2Class;

#include <freetype/freetype.h>
#include "../gnome-rfont-private.h"

struct _GnomeRFontFT2 {
	GnomeRFont rfont;

	/* FreeType Face object */
	FT_Face ft_face;

	/*
	 * Font size for FT2 library
	 */
	gdouble dsize;
	FT_F26Dot6 size;

	/*
	 * Resolution values for for FreeType library
	 * pixel_size = font->size * resolution / 72.0
	 */
	gdouble dresx, dresy;
	FT_UShort resx, resy;

	/*
	 * Remainder transformation matrix
	 * pixel_size * reminder = actual transform
	 */
	gdouble drm[6];
	FT_Matrix tt_m;
};

struct _GnomeRFontFT2Class {
	GnomeRFontClass parent_class;
};

GtkType gnome_rfont_ft2_get_type (void);

GnomeRFont * gnome_rfont_ft2_new (GnomeFont * font, gdouble affine[]);

END_GNOME_DECLS

#endif


