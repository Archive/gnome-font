#ifndef GF_FREETYPE2_H
#define GF_FREETYPE2_H

/*
 * FreeType library plug
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <glib.h>
#include <freetype/freetype.h>
#include <libart_lgpl/art_bpath.h>

#ifndef GF_FREETYPE2_C
	extern FT_Library gnome_font_FT_Library;
	extern gboolean gfft2_initialized;
#else
	FT_Library gnome_font_FT_Library;
	gboolean gfft2_initialized = FALSE;
#endif

#define GFFT (gfft2_initialized | gf_freetype2_init ())
#define GFFT_LIBRARY gnome_font_FT_Library

gboolean gf_freetype2_init (void);

/*
 * Here we have to do unusual locale chemistry, to extract available relevant
 * information from multiplatform TrueType fonts. The idea is to request
 * GSList of PlatforID/SpecificID/LanguageID preferences for given locale,
 * and process it, until we find all required strings
 */
 
typedef struct _GFFT_Locale GFFT_Locale;

struct _GFFT_Locale {
	FT_UShort PlatformID;
	FT_UShort PlatformSpecificID;
	FT_UShort LanguageID;
};

const GSList * gf_freetype_locale_preference_list (const gchar * language);

/* Duplicates string in one of TrueType encodings to UTF-8 encoded string */

gchar * gf_freetype_strdup_utf8 (FT_String * src, FT_UShort length, GFFT_Locale * loc);

/* Converts FreeType glyph outline to ArtBpath, given scaling */

ArtBpath * gf_freetype2_bpath_from_outline (FT_Outline * ol, gdouble transform[]);

#endif
