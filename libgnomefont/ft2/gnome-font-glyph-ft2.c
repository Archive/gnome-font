#define GNOME_FONTGLYPH_FT2_C

/*
 * GnomeFontGlyphFT2
 *
 * FreeType glyph
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <freetype/ftoutln.h>
#include <libart_lgpl/art_affine.h>
#include "../gnome-font-glyph-private.h"
#include "gf-freetype2.h"
#include "gnome-font-glyph-ft2.h"
#include "gnome-font-face-ft2.h"
#include "gnome-font-ft2.h"
#include "gnome-rfont-ft2.h"

static void gnome_font_glyph_ft2_class_init (GnomeFontGlyphFT2Class * klass);
static void gnome_font_glyph_ft2_init (GnomeFontGlyphFT2 * glyph);

static void gnome_font_glyph_ft2_destroy (GtkObject * object);

static const ArtBpath * gfgft2_bpath (GnomeFontGlyph * glyph);
static void gfgft2_metrics (GnomeFontGlyph * glyph, ArtDRect * bbox, ArtPoint * a);
static const GFGGrayMap * gfgft2_graymap (GnomeFontGlyph * glyph);

/* Private */

static FT_Face gfgft2_ft_face (GnomeFontGlyphFT2 * glyph);
static void gfgft2_transform (GnomeFontGlyphFT2 * glyph, gdouble transform[]);

static GnomeFontGlyphClass * parent_class = NULL;

GtkType
gnome_font_glyph_ft2_get_type (void)
{
	static GtkType glyph_type = 0;
	if (!glyph_type) {
		GtkTypeInfo glyph_info = {
			"GnomeFontGlyphFT2",
			sizeof (GnomeFontGlyphFT2),
			sizeof (GnomeFontGlyphFT2Class),
			(GtkClassInitFunc) gnome_font_glyph_ft2_class_init,
			(GtkObjectInitFunc) gnome_font_glyph_ft2_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};
		glyph_type = gtk_type_unique (gnome_font_glyph_get_type (), &glyph_info);
	}
	return glyph_type;
}

static void
gnome_font_glyph_ft2_class_init (GnomeFontGlyphFT2Class * klass)
{
	GtkObjectClass * object_class;
	GnomeFontGlyphClass * glyph_class;

	object_class = (GtkObjectClass *) klass;
	glyph_class = (GnomeFontGlyphClass *) klass;

	parent_class = gtk_type_class (gnome_font_glyph_get_type ());

	object_class->destroy = gnome_font_glyph_ft2_destroy;

	glyph_class->bpath = gfgft2_bpath;
	glyph_class->metrics = gfgft2_metrics;
	glyph_class->graymap = gfgft2_graymap;
}

static void
gnome_font_glyph_ft2_init (GnomeFontGlyphFT2 * glyph)
{
	/* Nothing */
}

static void
gnome_font_glyph_ft2_destroy (GtkObject * object)
{
	GnomeFontGlyphFT2 * glyph;

	glyph = (GnomeFontGlyphFT2 *) object;

	/* fixme: */

	if (glyph->bpath) g_free (glyph->bpath);

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

static const ArtBpath *
gfgft2_bpath (GnomeFontGlyph * glyph)
{
	GnomeFontGlyphFT2 * gft2;

	gft2 = (GnomeFontGlyphFT2 *) glyph;

	if (gft2->err_outline) return NULL;

	if (gft2->bpath == NULL) {
		FT_Face ft_face;
		gdouble transform[6];

		ft_face = gfgft2_ft_face (gft2);
		g_assert (ft_face != NULL);

		switch (gft2->type) {
		case GFGFT_TYPE_FACE:
			FT_Load_Glyph (ft_face, glyph->code,
				FT_LOAD_NO_SCALE | FT_LOAD_NO_HINTING | FT_LOAD_NO_BITMAP);
			gfgft2_transform (gft2, transform);
			break;
		case GFGFT_TYPE_FONT:
			FT_Load_Glyph (ft_face, glyph->code,
				FT_LOAD_NO_BITMAP);
			gfgft2_transform (gft2, transform);
			break;
		case GFGFT_TYPE_RFONT:
			FT_Load_Glyph (ft_face, glyph->code,
				FT_LOAD_NO_BITMAP);
			gfgft2_transform (gft2, transform);
			break;
		default:
			g_assert_not_reached ();
			break;
		}

		gft2->bpath = gf_freetype2_bpath_from_outline (&ft_face->glyph->outline, transform);

		if (!gft2->bpath) gft2->err_outline = TRUE;
	}

	return gft2->bpath;
}

static void
gfgft2_metrics (GnomeFontGlyph * glyph, ArtDRect * bbox, ArtPoint * a)
{
	GnomeFontGlyphFT2 * gft2;

	gft2 = (GnomeFontGlyphFT2 *) glyph;

	if (!gft2->has_metrics) {
		FT_Face ft_face;
		gdouble transform[6];
		ArtPoint p0, p1, p;

		ft_face = gfgft2_ft_face (gft2);
		g_assert (ft_face != NULL);

		switch (gft2->type) {
		case GFGFT_TYPE_FACE:
			FT_Load_Glyph (ft_face, glyph->code,
				FT_LOAD_NO_SCALE | FT_LOAD_NO_HINTING | FT_LOAD_NO_BITMAP);
			gfgft2_transform (gft2, transform);
			break;
		case GFGFT_TYPE_FONT:
			FT_Load_Glyph (ft_face, glyph->code,
				FT_LOAD_NO_BITMAP);
			gfgft2_transform (gft2, transform);
			break;
		case GFGFT_TYPE_RFONT:
			FT_Load_Glyph (ft_face, glyph->code,
				FT_LOAD_NO_BITMAP);
			gfgft2_transform (gft2, transform);
			break;
		default:
			g_assert_not_reached ();
			break;
		}

		p0.x = -ft_face->glyph->metrics.horiBearingX;
		p0.y = ft_face->glyph->metrics.horiBearingY - ft_face->glyph->metrics.height;
		p1.x = ft_face->glyph->metrics.width - ft_face->glyph->metrics.horiBearingX;
		p1.y = ft_face->glyph->metrics.horiBearingY;
		art_affine_point (&p0, &p0, transform);
		art_affine_point (&p1, &p1, transform);
		gft2->bbox.x0 = MIN (p0.x, p1.x);
		gft2->bbox.y0 = MIN (p0.y, p1.y);
		gft2->bbox.x1 = MAX (p0.x, p1.x);
		gft2->bbox.y1 = MAX (p0.y, p1.y);
		p.x = ft_face->glyph->metrics.horiAdvance;
		p.y = 0.0;
		art_affine_point (&gft2->advance, &p, transform);
		gft2->has_metrics = TRUE;
	}

	*bbox = gft2->bbox;
	*a = gft2->advance;
}

static const GFGGrayMap *
gfgft2_graymap (GnomeFontGlyph * glyph)
{
	GnomeFontGlyphFT2 * gft2;
	GnomeRFontFT2 * rfont;

	gft2 = (GnomeFontGlyphFT2 *) glyph;

	if (gft2->err_graymap) return NULL;
	if (gft2->type != GFGFT_TYPE_RFONT) return NULL;

	rfont = (GnomeRFontFT2 *) glyph->vfont;

	if (gft2->graymap.pixels == NULL) {
		FT_Outline ol;
		FT_BBox cb;
		FT_Bitmap bm;
		gint x0, y0, x1, y1, w, h;

		/* Get outline */

		FT_Load_Glyph (rfont->ft_face, glyph->code, FT_LOAD_NO_BITMAP);

		FT_Outline_New (GFFT_LIBRARY,
			rfont->ft_face->glyph->outline.n_points,
			rfont->ft_face->glyph->outline.n_contours,
			&ol);

		FT_Outline_Copy (&rfont->ft_face->glyph->outline, &ol);

		/* Transform outline with remainder matrix */

		FT_Outline_Transform (&ol, &rfont->tt_m);

		/* Find new bbox */

		FT_Outline_Get_CBox (&ol, &cb);

		/* Get outline bbox in pixels */

		x0 = (cb.xMin & -64) >> 6;
		y0 = (cb.yMin & -64) >> 6;
		x1 = (cb.xMax & -64) >> 6;
		y1 = (cb.yMax & -64) >> 6;

		/* Blah blah - setup raster map */

		w = ((x1 - x0) + 4) & 0xfffffffc;
		h = (y1 - y0) + 1;

		gft2->graymap.pixels = g_new0 (gchar, w * h);
		gft2->graymap.width = w;
		gft2->graymap.height = h;
		gft2->graymap.rowstride = w;
		gft2->graymap.x0 = x0;
		gft2->graymap.y0 = y0;

		/* Blah blah - setup FreeType raster map */

		bm.rows = h;
		bm.width = w;
		bm.pitch = -w;
		bm.buffer = gft2->graymap.pixels;
		bm.num_grays = 256;
		bm.pixel_mode = ft_pixel_mode_grays;
		bm.palette_mode = 0;
		bm.palette = NULL;

		/* Blah, blah - translate outline to bbox */

		FT_Outline_Translate (&ol, (-x0) * 64, (-y0) * 64);

		FT_Outline_Get_Bitmap (GFFT_LIBRARY, &ol, &bm);

		FT_Outline_Done (GFFT_LIBRARY, &ol);
	}

	return &gft2->graymap;
}

/* Constructor */

GnomeFontGlyph *
gnome_font_glyph_ft2_new (GnomeVFont * vfont, FT_Face face, gint code, GFGFTType type)
{
	GnomeFontGlyphFT2 * glyph;

	glyph = gtk_type_new (GNOME_TYPE_FONT_GLYPH_FT2);

	/* fixme: */

	glyph->glyph.vfont = vfont;
	glyph->glyph.code = code;

	glyph->type = type;

	glyph->can_outline = TRUE;
	glyph->can_graymap = TRUE;

	return (GnomeFontGlyph *) glyph;
}

/* Private */

static FT_Face
gfgft2_ft_face (GnomeFontGlyphFT2 * glyph)
{
	switch (glyph->type) {
	case GFGFT_TYPE_FACE:
		return ((GnomeFontFaceFT2 *)glyph->glyph.vfont)->ft_face;
	case GFGFT_TYPE_FONT:
		return ((GnomeFontFT2 *)glyph->glyph.vfont)->ft_face;
	case GFGFT_TYPE_RFONT:
		return ((GnomeRFontFT2 *)glyph->glyph.vfont)->ft_face;
	default:
		g_assert_not_reached ();
		break;
	}

	return NULL;
}

static void
gfgft2_transform (GnomeFontGlyphFT2 * glyph, gdouble transform[])
{
	FT_Face ft_face;
	gdouble ds[] = {1 / 64.0, 0.0, 0.0, 1 / 64.0, 0.0, 0.0};

	switch (glyph->type) {
	case GFGFT_TYPE_FACE:
		ft_face = ((GnomeFontFaceFT2 *) glyph->glyph.vfont)->ft_face;
		art_affine_scale (transform,
			1000.0 / ft_face->units_per_EM,
			1000.0 / ft_face->units_per_EM);
		break;
	case GFGFT_TYPE_FONT:
#if 0
		art_affine_multiply (transform, ds,
			((GnomeFontFT2 *) glyph->glyph.vfont)->private->drm);
#endif
		art_affine_scale (transform,
			72.0 / 64.0 / ((GnomeFontFT2 *) glyph->glyph.vfont)->dresx,
			72.0 / 64.0 / ((GnomeFontFT2 *) glyph->glyph.vfont)->dresy);
		break;
	case GFGFT_TYPE_RFONT:
		art_affine_multiply (transform, ds,
			((GnomeRFontFT2 *) glyph->glyph.vfont)->drm);
		break;
	default:
		g_assert_not_reached ();
		break;
	}
}


