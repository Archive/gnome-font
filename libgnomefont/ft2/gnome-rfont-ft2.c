#define GNOME_RFONT_FT2_C

/*
 * GnomeRFontFT2
 *
 * Freetype rasterized font implementation
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <math.h>
#include <string.h>
#include <libart_lgpl/art_affine.h>
#include "gf-freetype2.h"
#include "gnome-font-face-ft2.h"
#include "gnome-font-ft2.h"
#include "gnome-rfont-ft2.h"
#include "gnome-font-glyph-ft2.h"

static void gnome_rfont_ft2_class_init (GnomeRFontFT2Class * klass);
static void gnome_rfont_ft2_init (GnomeRFontFT2 * group);

static void gnome_rfont_ft2_destroy (GtkObject * object);

static gboolean grft_info (GnomeVFont * vfont);
static GnomeFontGlyph * gnome_rfont_ft2_get_glyph (GnomeVFont * vfont, gint code);
#if 0
static ArtPoint * gnome_rfont_ft2_kerning (GnomeVFont * vfont, gint c1, gint c2, ArtPoint * p);
#endif

static GnomeRFontClass * parent_class = NULL;

GtkType
gnome_rfont_ft2_get_type (void)
{
	static GtkType group_type = 0;
	if (!group_type) {
		GtkTypeInfo group_info = {
			"GnomeRFontFT2",
			sizeof (GnomeRFontFT2),
			sizeof (GnomeRFontFT2Class),
			(GtkClassInitFunc) gnome_rfont_ft2_class_init,
			(GtkObjectInitFunc) gnome_rfont_ft2_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};
		group_type = gtk_type_unique (gnome_rfont_get_type (), &group_info);
	}
	return group_type;
}

static void
gnome_rfont_ft2_class_init (GnomeRFontFT2Class * klass)
{
	GtkObjectClass * object_class;
	GnomeVFontClass * vfont_class;

	object_class = (GtkObjectClass *) klass;
	vfont_class = (GnomeVFontClass *) klass;

	parent_class = gtk_type_class (gnome_rfont_get_type ());

	object_class->destroy = gnome_rfont_ft2_destroy;

	vfont_class->info = grft_info;
	vfont_class->glyph = gnome_rfont_ft2_get_glyph;
#if 0
	vfont_class->kerning = gnome_rfont_ft2_kerning;
#endif
}

static void
gnome_rfont_ft2_init (GnomeRFontFT2 * font)
{
	/* Nothing */
}

static void
gnome_rfont_ft2_destroy (GtkObject * object)
{
	GnomeRFontFT2 * rfont;

	rfont = (GnomeRFontFT2 *) object;

	FT_Done_Face (rfont->ft_face);

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

/* fixme: */

static gboolean
grft_info (GnomeVFont * vfont)
{
	GnomeFontFaceFT2 * ftface;

	ftface = GNOME_FONT_FACE_FT2 (GNOME_RFONT (vfont)->font->face);

	vfont->num_glyphs = ftface->ft_face->num_glyphs;
	vfont->bbox.x0 = vfont->bbox.y0 = 0.0;
	vfont->bbox.x1 = vfont->bbox.y1 = 1000.0;

	return TRUE;
}

static GnomeFontGlyph *
gnome_rfont_ft2_get_glyph (GnomeVFont * vfont, gint code)
{
	GnomeRFontFT2 * rfontft2;
	GnomeFontGlyph * glyph;

	/* We can rely on having instance, hopefully :) */
	/* But we NEED dictionary still :( */
	/* And here we need grid-fitting too */

	rfontft2 = (GnomeRFontFT2 *) vfont;

	g_return_val_if_fail (code < rfontft2->ft_face->num_glyphs, NULL);

	glyph = gnome_font_glyph_ft2_new (vfont,
		rfontft2->ft_face,
		code,
		GFGFT_TYPE_RFONT);

	return glyph;
}

#if 0
static ArtPoint *
gnome_rfont_ft2_kerning (GnomeVFont * vfont, gint c1, gint c2, ArtPoint * p)
{
	GnomeRFontFT2 * rfontft2;
	GnomeRFontFT2Private * priv;
	FT_Vector kv;
	ArtPoint k;

	rfontft2 = (GnomeRFontFT2 *) vfont;
	priv = rfontft2->private;

	FT_Get_Kerning (priv->ft_face, c1, c2, ft_kerning_default, &kv);

#if 0
	k.x = (gdouble) kv.x * priv->dsize * priv->dresx / 72.0 / priv->ft_face->units_per_EM;
	k.y = (gdouble) kv.y * priv->dsize * priv->dresx / 72.0 / priv->ft_face->units_per_EM;
	art_affine_point (p, &k, priv->drm);
#else
	/* fixme: */
	k.x = (gdouble) kv.x;
	k.y = (gdouble) kv.y;
#endif

	return p;
}
#endif

/* Methods */

GnomeRFont *
gnome_rfont_ft2_new (GnomeFont * font, gdouble affine[])
{
	GnomeRFont * rfont;
	GnomeRFontFT2 * rfontft2;
	FT_Error result;

	rfontft2 = gtk_type_new (GNOME_TYPE_RFONT_FT2);
	rfont = (GnomeRFont *) rfontft2;

	/* fixme: ref font */

	/* Fill device values in GnomeRFont struct */

	rfont->font = font;
	memcpy (rfont->affine, affine, 4 * sizeof (gdouble));
	rfont->affine[4] = rfont->affine[5] = 0.0;

	/* Fill FT2 device values in GnomeRFontFT2 struct */

	/* Find best approximation of font size */

	/* fixme: add rounding remainder to transformation matrix */

	rfontft2->size = (gint) (font->size * 64.0 + 0.5);
	rfontft2->dsize = (gdouble) rfontft2->size / 64.0;

	/* Find best approximation of resolution */

	rfontft2->dresx = rint (hypot (affine[0], affine[1]) * 72.0);
	rfontft2->dresy = rint (hypot (affine[2], affine[3]) * 72.0);
	rfontft2->resx = (gint) rfontft2->dresx;
	rfontft2->resy = (gint) rfontft2->dresy;

	/* Find remainder matrix */

	rfontft2->drm[0] = affine[0] * 72.0 / rfontft2->dresx;
	rfontft2->drm[1] = affine[1] * 72.0 / rfontft2->dresx;
	rfontft2->drm[2] = affine[2] * 72.0 / rfontft2->dresy;
	rfontft2->drm[3] = affine[3] * 72.0 / rfontft2->dresy;
	rfontft2->drm[4] = rfontft2->drm[5] = 0.0;

	rfontft2->tt_m.xx = rint (rfontft2->drm[0] * 65536.0);
	rfontft2->tt_m.xy = rint (rfontft2->drm[1] * 65536.0);
	rfontft2->tt_m.yx = rint (rfontft2->drm[2] * 65536.0);
	rfontft2->tt_m.yy = rint (rfontft2->drm[3] * 65536.0);

	/* Now we have font transformation matrixes set up :) */

	/* fixme: error checking */

	result = FT_New_Face (GFFT_LIBRARY,
		((GnomeFontFaceFT2 *) font->face)->path,
		((GnomeFontFaceFT2 *) font->face)->ft_face->face_index,
		&rfontft2->ft_face);

	FT_Set_Char_Size (rfontft2->ft_face, rfontft2->size, rfontft2->size, rfontft2->resx, rfontft2->resy);
	FT_Select_Charmap (rfontft2->ft_face, ft_encoding_unicode);

	return rfont;
}


