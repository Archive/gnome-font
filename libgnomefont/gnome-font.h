#ifndef _GNOME_FONT_H_
#define _GNOME_FONT_H_

/*
 * GnomeFont
 *
 * Base class for user visible font
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#define GNOME_TYPE_FONT		(gnome_font_get_type ())
#define GNOME_FONT(obj)		(GTK_CHECK_CAST ((obj), GNOME_TYPE_FONT, GnomeFont))
#define GNOME_FONT_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_FONT, GnomeFontClass))
#define GNOME_IS_FONT(obj)	(GTK_CHECK_TYPE ((obj), GNOME_TYPE_FONT))
#define GNOME_IS_FONT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_FONT))

typedef struct _GnomeFont GnomeFont;
typedef struct _GnomeFontClass GnomeFontClass;

#include "gnome-font-family.h"
#include "gnome-font-face.h"
#include "gnome-rfont.h"

GtkType gnome_font_get_type (void);

#define gnome_font_ref(f) gtk_object_ref (GTK_OBJECT (f))
#define gnome_font_unref(f) gtk_object_unref (GTK_OBJECT (f))

/* Methods */

const GnomeFontFamily * gnome_font_get_family (const GnomeFont * font);

/* These are UTF-8 and try to return name in current LC_MESSAGES locale */

const gchar * gnome_font_get_name (const GnomeFont * font);
const gchar * gnome_font_get_family_name (const GnomeFont * font);
const gchar * gnome_font_get_species_name (const GnomeFont * font);

/* This is ASCII (and not localized :) */

const gchar * gnome_font_get_ps_name (const GnomeFont * font);

gint gnome_font_lookup_default (const GnomeFont * font, gint unicode);

ArtDRect * gnome_font_get_stdbbox (const GnomeFont * font, ArtDRect * bbox);

ArtPoint * gnome_font_get_glyph_stdadvance (const GnomeFont * font, gint glyphnum, ArtPoint * advance);
ArtDRect * gnome_font_get_glyph_stdbbox (const GnomeFont * font, gint glyphnum, ArtDRect * bbox);
const ArtBpath * gnome_font_get_glyph_stdoutline (const GnomeFont * font, gint glyphnum);

/* Get Rasterized font handler, given base -> device transformation */

GnomeRFont * gnome_font_get_rfont (GnomeFont * font, gdouble affine[]);

/*
 * Convenience and compatibility methods from gnome-print
 * THESE ARE DEPRECATED BY DEFAULT, although some will probably be preserved
 *
 */

gdouble gnome_font_get_ascender (const GnomeFont * font); 
gdouble gnome_font_get_descender (const GnomeFont * font);
gdouble gnome_font_get_underline_position (const GnomeFont * font); 
gdouble gnome_font_get_underline_thickness (const GnomeFont * font);
gdouble gnome_font_get_glyph_width (const GnomeFont * font, gint glyph);
gdouble gnome_font_get_glyph_kerning (const GnomeFont * font, gint glyph1, gint glyph2);
GnomeFontWeight gnome_font_get_weight_code (const GnomeFont * font);
gboolean gnome_font_is_italic (const GnomeFont * font);
gchar * gnome_font_get_pfa (const GnomeFont * font);
const GnomeFontFace * gnome_font_get_face (const GnomeFont * font);
gdouble gnome_font_get_size (const GnomeFont * font);
GnomeFont * gnome_font_new (const char * name, double size);
GnomeFont * gnome_font_new_closest (const char *family_name,
                                   GnomeFontWeight weight,
                                   gboolean italic,
                                   double size);
GnomeFont *gnome_font_new_from_full_name (const char *string);
char * gnome_font_get_full_name (const GnomeFont *font);
double gnome_font_get_width_string (const GnomeFont *font, const char *s);
double gnome_font_get_width_string_n (const GnomeFont *font, const char *s, int n);
  
typedef struct _GnomeRFont GnomeDisplayFont;

GnomeDisplayFont * gnome_font_get_display_font (GnomeFont *font);
const char * gnome_font_weight_to_string (GnomeFontWeight gfw);  
GnomeFontWeight string_to_gnome_font_weight (const char *weight);
GnomeDisplayFont * gnome_get_display_font (const char * family,  
                                           GnomeFontWeight weight,
                                           gboolean italic,
                                           double points,  
                                           double scale);  
int gnome_display_font_height (GnomeDisplayFont * gdf);    
const GnomeFontFace * gnome_display_font_get_face (const GnomeDisplayFont * gdf);
const GnomeFont * gnome_display_font_get_font (const GnomeDisplayFont * gdf);  
gdouble gnome_display_font_get_scale (const GnomeDisplayFont * gdf);
const gchar * gnome_display_font_get_x_font_name (const GnomeDisplayFont * gdf);
GdkFont * gnome_display_font_get_gdk_font (const GnomeDisplayFont * gdf);
void gnome_display_font_ref (GnomeDisplayFont * gdf);
void gnome_display_font_unref (GnomeDisplayFont * gdf);

END_GNOME_DECLS

#endif
