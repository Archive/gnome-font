#ifndef GNOME_FONT_CONFIG_H
#define GNOME_FONT_CONFIG_H

#include <glib.h>

void gnome_font_config_try_file (const gchar * path);

#endif
