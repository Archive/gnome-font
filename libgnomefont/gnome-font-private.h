#ifndef _GNOME_FONT_PRIVATE_H_
#define _GNOME_FONT_PRIVATE_H_

/*
 * GnomeFont
 *
 * Base class for user visible font
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include "gnome-vfont-private.h"
#include "gnome-font.h"

BEGIN_GNOME_DECLS

struct _GnomeFont {
	GnomeVFont vfont;
	GnomeFontFace * face;
	GSList * RFonts;
	gdouble size;
	gdouble affine[6];
};

struct _GnomeFontClass {
	GnomeVFontClass parent_class;

	GnomeRFont * (* get_rfont) (GnomeFont * font, gdouble affine[]);
};

END_GNOME_DECLS

#endif
