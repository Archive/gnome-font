#define _GNOME_FONT_FACE_C_

/*
 * GnomeFontFace
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <locale.h>
/* fixme: */
#include "gnome-font-private.h"
#include "gnome-font-face-private.h"

#define unimpl() g_print ("%s:%d UNIMPLEMENTED METHOD %s\n", __FILE__, __LINE__, __FUNCTION__)

static void gff_class_init (GnomeFontFaceClass * klass);
static void gff_init (GnomeFontFace * face);

static void gff_destroy (GtkObject * object);

static GnomeFontFaceDesc * gff_req_description (GnomeFontFace * face, const gchar * language);
static gboolean gff_free_dict_desc (gpointer key, gpointer value, gpointer data);

static GnomeVFontClass * parent_class = NULL;

GtkType
gnome_font_face_get_type (void)
{
	static GtkType gff_type = 0;
	if (!gff_type) {
		GtkTypeInfo gff_info = {
			"GnomeFontFace",
			sizeof (GnomeFontFace),
			sizeof (GnomeFontFaceClass),
			(GtkClassInitFunc) gff_class_init,
			(GtkObjectInitFunc) gff_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};
		gff_type = gtk_type_unique (gnome_vfont_get_type (), &gff_info);
	}
	return gff_type;
}

static void
gff_class_init (GnomeFontFaceClass * klass)
{
	GtkObjectClass * object_class;

	object_class = (GtkObjectClass *) klass;

	parent_class = gtk_type_class (gnome_vfont_get_type ());

	object_class->destroy = gff_destroy;
}

static void
gff_init (GnomeFontFace * face)
{
	face->descriptions = g_hash_table_new (g_str_hash, g_str_equal);

	face->entry = NULL;
}

static void
gff_destroy (GtkObject * object)
{
	GnomeFontFace * face;

	face = (GnomeFontFace *) object;

	/*
	 * We can safely destroy dictionary here, cause all names
	 * should be freed by implementations
	 */

	if (face->descriptions) {
		g_hash_table_foreach_remove (face->descriptions, gff_free_dict_desc, NULL);
		face->descriptions = NULL;
	}

	if (face->entry) {
		face->entry->face = NULL;
		gf_font_entry_unref (face->entry);
		face->entry = NULL;
	}

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

/* Basic methods */

/* fixme: */

const GnomeFontFamily *
gnome_font_face_get_family (const GnomeFontFace * face)
{
	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);

	return NULL;
}

/* fixme: use intelligence here :) */

const gchar *
gnome_font_face_get_name (const GnomeFontFace * face)
{
	GnomeFontFaceDesc * desc;
	gchar * locale;

	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);

	locale = setlocale (LC_MESSAGES, NULL);

	desc = gff_req_description ((GnomeFontFace *) face, locale);
	g_assert (desc != NULL);

	return desc->name;
}

/* fixme: */

const gchar *
gnome_font_face_get_family_name (const GnomeFontFace * face)
{
	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);

	return face->entry->familyname;
}

/* fixme: use intelligence here :) */

const gchar *
gnome_font_face_get_species_name (const GnomeFontFace * face)
{
	GnomeFontFaceDesc * desc;
	gchar * locale;

	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);

	locale = setlocale (LC_MESSAGES, NULL);

	desc = gff_req_description ((GnomeFontFace *) face, locale);
	g_assert (desc != NULL);

	return desc->species;
}

const gchar *
gnome_font_face_get_ps_name (const GnomeFontFace * face)
{
	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);

	return face->entry->psname;
}

/* Returns list of const strings in order of preference */

GList *
gnome_font_face_get_ps_synonyms (const GnomeFontFace * face)
{
	GList * l;

	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);

	l = g_list_prepend (NULL, face->entry->psname);
	if (face->entry->type == GF_FONT_ENTRY_TYPE1_ALIAS) {
		l = g_list_append (l, ((GFFontEntryT1Alias *) face->entry)->alias);
	}

	return l;
}

/* fixme: */

gint
gnome_font_face_lookup_default (const GnomeFontFace * face, gint code)
{
	return gnome_font_face_lookup (face, 0, code);
}

gint
gnome_font_face_lookup (const GnomeFontFace * face, gint encoding, gint code)
{
	g_return_val_if_fail (face != NULL, FALSE);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), FALSE);

	if (GNOME_FONT_FACE_CLASS (GTK_OBJECT (face)->klass)->lookup) {
		return (* GNOME_FONT_FACE_CLASS (GTK_OBJECT (face)->klass)->lookup) ((GnomeFontFace *) face, encoding, code);
	} else {
		g_warning ("Face does not implement lookup method!");
	}

	return 0;
}

/*
 * Metrics
 *
 * Currently those return standard values for left to right, horizontal script
 * The prefix std means, that there (a) will hopefully be methods to extract
 * different metric values and (b) for given face one text direction can
 * be defined as "default"
 * All face metrics are given in 0.001 em units
 */

ArtDRect *
gnome_font_face_get_stdbbox (const GnomeFontFace * face, ArtDRect * bbox)
{
	const ArtDRect * b;

	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);
	g_return_val_if_fail (bbox != NULL, NULL);

	b = gnome_vfont_get_stdbbox (GNOME_VFONT (face));

	if (!b) return NULL;

	*bbox = *b;

	return bbox;
}

ArtPoint *
gnome_font_face_get_glyph_stdadvance (const GnomeFontFace * face, gint glyphnum, ArtPoint * advance)
{
	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);

	return gnome_vfont_get_glyph_stdadvance (GNOME_VFONT (face), glyphnum, advance);
}

ArtDRect *
gnome_font_face_get_glyph_stdbbox (const GnomeFontFace * face, gint glyphnum, ArtDRect * bbox)
{
	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);

	return gnome_vfont_get_glyph_stdbbox (GNOME_VFONT (face), glyphnum, bbox);
}

const ArtBpath *
gnome_font_face_get_glyph_stdoutline (const GnomeFontFace * face, gint glyphnum)
{
	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);

	return gnome_vfont_get_glyph_stdoutline (GNOME_VFONT (face), glyphnum);
}

GnomeFont *
gnome_font_face_get_font_default (GnomeFontFace * face, gdouble size)
{
	gdouble transform[4];

	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);

	transform[0] = 600.0 / 72.0;
	transform[3] = 600.0 / 72.0;
	transform[1] = transform[2] = 0.0;

	return gnome_font_face_get_font_full (face, size, transform);
}

GnomeFont *
gnome_font_face_get_font (GnomeFontFace * face, gdouble size, gdouble xres, gdouble yres)
{
	gdouble transform[4];

	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);

	transform[0] = xres / 72.0;
	transform[3] = yres / 72.0;
	transform[1] = transform[2] = 0.0;

	return gnome_font_face_get_font_full (face, size, transform);
}

GnomeFont *
gnome_font_face_get_font_full (GnomeFontFace * face, gdouble size, gdouble transform[])
{
	GnomeFont * font;

	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);

	if (GNOME_FONT_FACE_CLASS (GTK_OBJECT (face)->klass)->get_font) {
		font = (* GNOME_FONT_FACE_CLASS (GTK_OBJECT (face)->klass)->get_font) (face, size, transform);
	} else {
		g_warning ("Face does not implement get_font method");
		return NULL;
	}

	/* fixme: */
	gnome_font_face_ref (face);
	font->face = face;
	font->size = size;
	memcpy (font->affine, transform, 4 * sizeof (double));
	font->affine[4] = font->affine[5] = 0.0;

	return font;
}

/* EXPERIMENTAL */

gboolean
gnome_font_face_set_encoding (GnomeFontFace * face, gint id, const gchar * name, const gchar * encoding)
{
	gboolean ret;

	g_return_val_if_fail (face != NULL, FALSE);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), FALSE);
	g_return_val_if_fail (id >= 0, FALSE);
	/* NB! 256 encodings per font is arbitrary - hope you do not need more */
	g_return_val_if_fail (id < 256, FALSE);
	g_return_val_if_fail (encoding != NULL, FALSE);

	ret = FALSE;

	if (((GnomeFontFaceClass *)(((GtkObject *) face)->klass))->set_encoding) {
		ret = (* ((GnomeFontFaceClass *)(((GtkObject *) face)->klass))->set_encoding) (face, id, encoding);
	} else {
		g_warning ("Face does not implement set_encoding method!");
	}

	/* fixme: define name->id mapping */

	return ret;
}

/*
 * Our basic constructor
 *
 * If building unmapped fonts, you have to manually fill entry and
 * take care refcounting that is done right
 *
 */

GnomeFontFace *
gnome_font_face_construct (GnomeFontFace * face, GFFontEntry * entry)
{
	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);
	g_return_val_if_fail (entry != NULL, NULL);

	gf_font_entry_ref (entry);

	face->entry = entry;
	entry->face = face;

	return face;
}

/* Helpers */

static GnomeFontFaceDesc *
gff_req_description (GnomeFontFace * face, const gchar * language)
{
	GnomeFontFaceClass * klass;
	GnomeFontFaceDesc * desc;

	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);
	g_return_val_if_fail (language != NULL, NULL);

	desc = g_hash_table_lookup (face->descriptions, language);

	if (desc != NULL) return desc;

	klass = (GnomeFontFaceClass *)(((GtkObject *) face)->klass);

	if (!klass->description) return NULL;

	desc = (* klass->description) (face, language);

	if (desc == NULL) desc = (* klass->description) (face, "C");

	g_assert (desc != NULL);
	g_assert (desc->language);
	g_assert (desc->species);
	g_assert (desc->name);

	g_hash_table_insert (face->descriptions, desc->language, desc);

	return desc;
}

static gboolean
gff_free_dict_desc (gpointer key, gpointer value, gpointer data)
{
	GnomeFontFaceDesc * desc;

	desc = (GnomeFontFaceDesc *) value;

	/* Language menber serves also as key */

	g_free (desc->language);
	g_free (desc->species);
	g_free (desc->name);

	g_free (desc);

	return TRUE;
}

/* Experimental */

guchar *
gnome_font_face_get_ps_object (const GnomeFontFace * face, const guchar * psname)
{
	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);
	g_return_val_if_fail (psname != NULL, NULL);

	if (GNOME_FONT_FACE_CLASS (GTK_OBJECT (face)->klass)->get_ps_object) {
		return (* GNOME_FONT_FACE_CLASS (GTK_OBJECT (face)->klass)->get_ps_object) (face, psname);
	} else {
		g_warning ("Face does not implement get_ps_object method!");
	}

	return NULL;
}

/*
 * convenience and compatibility methods from gnome-print
 * THESE ARE DEPRECATED by default, although some probably will be preserved
 */

/*
 * Implement, what deserves implementation
 */

gint
gnome_font_face_get_num_glyphs (const GnomeFontFace * face)
{
	unimpl();
	return 1;
}

gchar *
gnome_font_face_get_pfa (const GnomeFontFace * face)
{
	unimpl();
	return NULL;
}

gdouble
gnome_font_face_get_ascender (const GnomeFontFace * face)
{
	unimpl();
	return 0.0;
}

gdouble
gnome_font_face_get_descender (const GnomeFontFace * face)
{
	unimpl();
	return 0.0;
}

gdouble
gnome_font_face_get_underline_position (const GnomeFontFace * face)
{
	unimpl();
	return 0.0;
}

gdouble
gnome_font_face_get_underline_thickness (const GnomeFontFace * face)
{
	unimpl();
	return 0.0;
}

gdouble
gnome_font_face_get_glyph_width (const GnomeFontFace * face, gint glyphnum)
{
	ArtPoint p;

	if (gnome_font_face_get_glyph_stdadvance (face, glyphnum, &p)) {
		return p.x;
	} else {
		return 0.0;
	}
}

gdouble
gnome_font_face_get_glyph_kerning (const GnomeFontFace * face, gint glyph1, gint glyph2)
{
	unimpl();
	return 0.0;
}

const gchar *
gnome_font_face_get_glyph_ps_name (const GnomeFontFace * face, gint glyph)
{
	unimpl();
	return NULL;
}

GnomeFontWeight
gnome_font_face_get_weight_code (const GnomeFontFace * face)
{
	unimpl();
	return GNOME_FONT_BOOK;
}

gboolean
gnome_font_face_is_italic (const GnomeFontFace * face)
{
	unimpl();
	return FALSE;
}

gboolean
gnome_font_face_is_fixed_width (const GnomeFontFace * face)
{
	unimpl();
	return FALSE;
}

const gchar *
gnome_font_face_get_sample (const GnomeFontFace * face)
{
	unimpl();
	return "FooBar";
}




