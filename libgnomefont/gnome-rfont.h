#ifndef _GNOME_RFONT_H_
#define _GNOME_RFONT_H_

/*
 * GnomeRFont
 *
 * Base class for rasterized fonts, i.e. the ones scaled to real output device
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#define GNOME_TYPE_RFONT		(gnome_rfont_get_type ())
#define GNOME_RFONT(obj)		(GTK_CHECK_CAST ((obj), GNOME_TYPE_RFONT, GnomeRFont))
#define GNOME_RFONT_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_RFONT, GnomeRFontClass))
#define GNOME_IS_RFONT(obj)		(GTK_CHECK_TYPE ((obj), GNOME_TYPE_RFONT))
#define GNOME_IS_RFONT_CLASS(klass) 	(GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_RFONT))

typedef struct _GnomeRFont		GnomeRFont;
typedef struct _GnomeRFontClass		GnomeRFontClass;

#include <libart_lgpl/art_svp.h>
#include <libgnomefont/gnome-pgl.h>
#include <libgnomefont/gnome-font.h>

GtkType gnome_rfont_get_type (void);

#define gnome_rfont_ref(f) gtk_object_ref (GTK_OBJECT (f))
#define gnome_rfont_unref(f) gtk_object_unref (GTK_OBJECT (f))

ArtDRect * gnome_rfont_get_stdbbox (const GnomeRFont * rfont, ArtDRect * bbox);

ArtPoint * gnome_rfont_get_glyph_stdadvance (const GnomeRFont * rfont, gint glyphnum, ArtPoint * advance);
ArtDRect * gnome_rfont_get_glyph_stdbbox (const GnomeRFont * rfont, gint glyphnum, ArtDRect * bbox);
const ArtBpath * gnome_rfont_get_glyph_stdoutline (const GnomeRFont * rfont, gint glyphnum);

/*
 * convenience and compatibility methods from gnome-print
 * THESE ARE DEPRECATED by default, although some probably will be preserved
 */

ArtPoint * gnome_rfont_get_stdadvance (const GnomeRFont * rfont, ArtPoint * advance);
const GnomeFont * gnome_rfont_get_font (const GnomeRFont * rfont);
const GnomeFontFace * gnome_rfont_get_face (const GnomeRFont * rfont);
gdouble * gnome_rfont_get_matrix (const GnomeRFont * rfont, gdouble * matrix);
const ArtBpath * gnome_rfont_get_glyph_bpath (const GnomeRFont * rfont, gint glyph);
const ArtSVP * gnome_rfont_get_glyph_svp (const GnomeRFont * rfont, gint glyph);
void gnome_rfont_render_glyph_rgba8 (const GnomeRFont * rfont, gint glyph,
                                     guint32 rgba,
                                     gdouble x, gdouble y,
                                     guchar * buf,
                                     gint width, gint height, gint rowstride,
                                     guint flags);
void gnome_rfont_render_glyph_rgb8 (const GnomeRFont * rfont, gint glyph,
                                    guint32 rgba,
                                    gdouble x, gdouble y,
                                    guchar * buf,
                                    gint width, gint height, gint rowstride,
                                    guint flags);
void gnome_rfont_render_pgl_rgb8 (const GnomePosGlyphList * pgl,
                                    gdouble x, gdouble y,
                                    guchar * buf,
                                    gint width, gint height, gint rowstride,
                                    guint flags);
void gnome_rfont_render_glyph_gdk_drawable (const GnomeRFont * rfont, gint glyph,
                                guint32 rgba,
                                gdouble x, gdouble y,
                                GdkDrawable * drawable,
                                guint32 background,
                                guint flags);
void gnome_rfont_render_pgl_gdk_drawable (const GnomePosGlyphList * pgl,
                                gdouble x, gdouble y,
                                GdkDrawable * drawable,
                                guint32 background,
                                guint flags);



END_GNOME_DECLS

#endif
