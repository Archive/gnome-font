#ifndef GNOME_FONT_GLYPH_H
#define GNOME_FONT_GLYPH_H

/*
 * GnomeFontGlyph
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

typedef struct _GnomeFontGlyph GnomeFontGlyph;

#include <gtk/gtk.h>
#include <libart_lgpl/art_rect.h>
#include <libart_lgpl/art_bpath.h>
#include <libgnomefont/gnome-vfont.h>

typedef struct _GFGGrayMap GFGGrayMap;
typedef struct _GFGGdkMap GFGGdkMap;

enum {
	GFG_CACHED = 1 << 4
};

/* NB! These are experimental and probably will change */

struct _GFGGrayMap {
	guchar * pixels;
	gint width, height, rowstride;
	gint x0, y0;
};

struct _GFGGdkMap {
	GdkPixmap * pixmap;
	GdkBitmap * mask;
	gint x0, y0;
};

/*
 * The interpretation of glyph metrics depend, whether glyph belongs to
 * GnomeFace, GnomeFont or GnomeTranformedFont
 */

const ArtBpath * gnome_font_glyph_get_bpath (GnomeFontGlyph * glyph);
#define gnome_font_glyph_get_stdoutline gnome_font_glyph_get_bpath

/*
 * Possible candidates for portable functions
 * Not, that font direction should be already encoded into font,
 * all bearings, outlines and bitmaps are also adjusted for direction
 */

ArtDRect * gnome_font_glyph_get_dimensions (GnomeFontGlyph * glyph, ArtDRect * box);
#define gnome_font_glyph_get_stdbbox gnome_font_glyph_get_dimensions
ArtPoint * gnome_font_glyph_get_stdadvance (GnomeFontGlyph * glyph, ArtPoint * p);

/*
 * Graymap
 */

const GFGGrayMap * gnome_font_glyph_get_graymap (GnomeFontGlyph * glyph);

/*
 * This is not sufficent - probably
 * What about multiple visuals/displays - but still then there should be
 * probably multiple rfonts anyway
 * Also - handle coloring
 */

const GFGGdkMap * gnome_font_glyph_get_gdkmap (GnomeFontGlyph * glyph);

/*
 * Kerning
 */

ArtPoint * gnome_font_glyph_get_kerning (GnomeFontGlyph * a, GnomeFontGlyph * b, ArtPoint * p);

END_GNOME_DECLS

#endif
