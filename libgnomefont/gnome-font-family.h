#ifndef GNOME_FONT_FAMILY_H
#define GNOME_FONT_FAMILY_H

/* FontFamily */

/*
 * THIS IS EXPERIMENTAL AND LIABLE TO CHANGE WITHOUT NOTICE
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#define GNOME_TYPE_FONT_FAMILY		(gnome_font_family_get_type ())
#define GNOME_FONT_FAMILY(obj)		(GTK_CHECK_CAST ((obj), GNOME_TYPE_FONT_FAMILY, GnomeFontFamily))
#define GNOME_FONT_FAMILY_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_FONT_FAMILY, GnomeFontFamilyClass))
#define GNOME_IS_FONT_FAMILY(obj)	(GTK_CHECK_TYPE ((obj), GNOME_TYPE_FONT_FAMILY))
#define GNOME_IS_FONT_FAMILY_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_FONT_FAMILY))

typedef struct _GnomeFontFamily		GnomeFontFamily;
typedef struct _GnomeFontFamilyClass	GnomeFontFamilyClass;
typedef struct _GnomeFontFamilyPrivate	GnomeFontFamilyPrivate;

#include <gtk/gtkobject.h>
#include "gnome-font-face.h"

GtkType gnome_font_family_get_type (void);

#define gnome_font_family_ref(f) gtk_object_ref (GTK_OBJECT (f))
#define gnome_font_family_unref(f) gtk_object_unref (GTK_OBJECT (f))

/*
 * Convenience and compatibility methods from gnome-print
 * THESE ARE DEPRECATED by default, although some will probably be preserved
 *
 */

GnomeFontFamily * gnome_font_family_new (const gchar * name);
GList * gnome_font_family_style_list (GnomeFontFamily * family);
void gnome_font_family_style_list_free (GList * list);
GnomeFontFace * gnome_font_family_get_face_by_stylename (GnomeFontFamily * family, const gchar * stylename);

END_GNOME_DECLS

#endif
