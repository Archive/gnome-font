#ifndef _GNOME_VFONT_H_
#define _GNOME_VFONT_H_

/*
 * GnomeVFont
 *
 * Abstract virtual base class for all glyph collection types
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <gtk/gtk.h>
#include <libart_lgpl/art_misc.h>
#include <libart_lgpl/art_rect.h>
#include <libgnome/gnome-defs.h>
#include "gnome-font-glyph.h"

BEGIN_GNOME_DECLS

#define GNOME_TYPE_VFONT (gnome_vfont_get_type ())
#define GNOME_VFONT(obj) (GTK_CHECK_CAST ((obj), GNOME_TYPE_VFONT, GnomeVFont))
#define GNOME_VFONT_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_VFONT, GnomeVFontClass))
#define GNOME_IS_VFONT(obj) (GTK_CHECK_TYPE ((obj), GNOME_TYPE_VFONT))
#define GNOME_IS_VFONT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_VFONT))

typedef struct _GnomeVFont GnomeVFont;
typedef struct _GnomeVFontClass	GnomeVFontClass;

GtkType gnome_vfont_get_type (void);

#define gnome_vfont_ref(f) gtk_object_ref (GTK_OBJECT (f))
#define gnome_vfont_unref(f) gtk_object_unref (GTK_OBJECT (f))

/*
 * Methods
 *
 * 0. VFont only methods
 * 1. Get glyph properties, glyph state will remain unknown
 * 2. Get glyph properties, glyph will remain locked
 */

const ArtDRect * gnome_vfont_get_stdbbox (const GnomeVFont * vfont);

ArtPoint * gnome_vfont_get_glyph_stdadvance (const GnomeVFont * vfont, gint glyphnum, ArtPoint * advance);
ArtDRect * gnome_vfont_get_glyph_stdbbox (const GnomeVFont * vfont, gint glyphnum, ArtDRect * bbox);

const GnomeFontGlyph * gnome_vfont_get_glyph (const GnomeVFont * vfont, gint glyphnum);
const ArtBpath * gnome_vfont_get_glyph_stdoutline (const GnomeVFont * vfont, gint glyphnum);
void gnome_vfont_glyph_lock (const GnomeVFont * vfont, gint glyphnum);
void gnome_vfont_glyph_unlock (const GnomeVFont * vfont, gint glyphnum);

END_GNOME_DECLS

#endif
