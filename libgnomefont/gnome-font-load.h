#ifndef GNOME_FONT_LOAD_H
#define GNOME_FONT_LOAD_H

/*
 * Loaders and similar things for gnome-font
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include "gnome-font-face.h"

/*
 * Basic constructor gnome_font_face_new (name) is here
 */

/*
 * Creates new typeface object from file
 */

GnomeFontFace * gnome_font_face_new_from_file (const gchar * filename);

#endif
