#define _GF_FONTMAP_C_

/*
 * Fontmap implementation
 *
 * Authors:
 *   Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 * TODO: Recycle font entries, if they are identical for different maps
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-util.h>
#include <gnome-xml/parser.h>
#include <gnome-xml/xmlmemory.h>
#include "gf-fontmap.h"

static GFFontMap * gf_fontmap_load (void);
static void gf_fontmap_ref (GFFontMap * map);
static void gf_fontmap_unref (GFFontMap * map);
static void gf_family_entry_ref (GFFamilyEntry * entry);
static void gf_family_entry_unref (GFFamilyEntry * entry);
static void gf_fm_load_fonts (GFFontMap * map, xmlDoc * doc);
static void gf_fm_load_fonts_1_0 (GFFontMap * map, xmlNodePtr root);
static void gf_fm_load_font_1_0 (GFFontMap * map, xmlNodePtr node);
static void gf_fm_load_fonts_2_0 (GFFontMap * map, xmlNodePtr root);
static void gf_fm_load_font_2_0_t1 (GFFontMap * map, xmlNodePtr node, gboolean alias);
static void gf_fm_load_font_2_0_tt (GFFontMap * map, xmlNodePtr node);
static void gf_fm_load_aliases (GFFontMap * map, xmlDoc * doc);
static gchar * gf_xmlGetPropString (xmlNodePtr node, const gchar * name);
static gint gf_fe_sortname (gconstpointer a, gconstpointer b);
static gint gf_fe_sortspecies (gconstpointer a, gconstpointer b);
static gint gf_familyentry_sortname (gconstpointer a, gconstpointer b);
static gchar * gf_fm_get_species_name (const gchar * fullname, const gchar * familyname);
static gboolean gf_fm_is_changed (GFFontMap * map);

/* Fontlist -> FontMap */
static GHashTable * fontlist2map = NULL;
/* Familylist -> FontMap */
static GHashTable * familylist2map = NULL;

GFFontMap *
gf_fontmap_get (void)
{
	static GFFontMap * map = NULL;

	if (map) {
		if (gf_fm_is_changed (map)) {
			g_print ("Fontmap is changed, reloading\n");
			gf_fontmap_release (map);
			map = NULL;
		}
	}

	if (!map) map = gf_fontmap_load ();

	map->refcount++;

	return map;
}

void
gf_fontmap_release (GFFontMap * map)
{
	gf_fontmap_unref (map);
}

static void
gf_fontmap_ref (GFFontMap * map)
{
	g_return_if_fail (map != NULL);

	map->refcount++;
}

static void
gf_fontmap_unref (GFFontMap * map)
{
	g_return_if_fail (map != NULL);

	if (--map->refcount < 1) {
		g_print ("Releasing fontmap\n");
		if (map->user.name) g_free (map->user.name);
		if (map->system.name) g_free (map->system.name);
		if (map->familydict) g_hash_table_destroy (map->familydict);
		if (map->fontdict) g_hash_table_destroy (map->fontdict);
		if (map->familylist) {
			g_hash_table_remove (familylist2map, map->familylist);
			g_list_free (map->familylist);
		}
		if (map->fontlist) {
			g_hash_table_remove (fontlist2map, map->fontlist);
			g_list_free (map->fontlist);
		}
		while (map->families) {
			gf_family_entry_unref ((GFFamilyEntry *) map->families->data);
			map->families = g_slist_remove (map->families, map->families->data);
		}
		while (map->fonts) {
			gf_font_entry_unref ((GFFontEntry *) map->fonts->data);
			map->fonts = g_slist_remove (map->fonts, map->fonts->data);
		}
		g_free (map);
	}
}

static void
gf_family_entry_ref (GFFamilyEntry * entry)
{
	entry->refcount++;
}

static void
gf_family_entry_unref (GFFamilyEntry * entry)
{
	if (--entry->refcount < 1) {
		if (entry->name) g_free (entry->name);
		if (entry->fonts) g_slist_free (entry->fonts);
		g_free (entry);
	}
}

static GFFontMap *
gf_fontmap_load (void)
{
	GFFontMap * map;
	gchar * home, * name;
	xmlDoc * usermap, * systemmap;
	struct stat s;
	GSList * l;

	map = g_new (GFFontMap, 1);
	map->refcount = 1; /* Permanent fontmap */
	map->num_fonts = 0;
	map->user.name = NULL;
	map->system.name = NULL;
	map->fontdict = g_hash_table_new (g_str_hash, g_str_equal);
	map->familydict = g_hash_table_new (g_str_hash, g_str_equal);
	map->fonts = NULL;
	map->families = NULL;
	map->fontlist = NULL;
	map->familylist = NULL;

	/* Usermap */

	usermap = NULL;
	home = getenv ("HOME");
	g_return_val_if_fail (home != NULL, NULL);
	name = g_concat_dir_and_file (home, "/.gnome/fonts/fontmap");

	if ((stat (name, &s) == 0) && S_ISREG (s.st_mode) && (usermap = xmlParseFile (name))) {
		map->user.name = name;
		map->user.size = s.st_size;
		map->user.mtime = s.st_mtime;
	} else {
		g_free (name);
	}

	/* System map */

	systemmap = NULL;
	name = gnome_datadir_file ("fonts/fontmap");
	if (!name) {
		name = g_concat_dir_and_file (DATADIR, "/fonts/fontmap");
	}
	if ((stat (name, &s) == 0) && S_ISREG (s.st_mode) && (systemmap = xmlParseFile (name))) {
		map->system.name = name;
		map->system.size = s.st_size;
		map->system.mtime = s.st_mtime;
	} else {
		g_free (name);
	}

	if (usermap) gf_fm_load_fonts (map, usermap);
	if (systemmap) gf_fm_load_fonts (map, systemmap);
	if (usermap) gf_fm_load_aliases (map, usermap);
	if (systemmap) gf_fm_load_aliases (map, systemmap);
	if (usermap) xmlFreeDoc (usermap);
	if (systemmap) xmlFreeDoc (systemmap);

	map->fonts = g_slist_sort (map->fonts, gf_fe_sortname);

	for (l = map->fonts; l != NULL; l = l->next) {
		GFFontEntry * e;
		GFFamilyEntry * f;
		e = (GFFontEntry *) l->data;
		f = g_hash_table_lookup (map->familydict, e->familyname);
		if (!f) {
			f = g_new0 (GFFamilyEntry, 1);
			gf_family_entry_ref (f);
			f->name = g_strdup (e->familyname);
			f->fonts = g_slist_prepend (f->fonts, e);
			g_hash_table_insert (map->familydict, f->name, f);
			map->families = g_slist_prepend (map->families, f);
		} else {
			f->fonts = g_slist_prepend (f->fonts, e);
		}
	}

	for (l = map->families; l != NULL; l = l->next) {
		GFFamilyEntry * f;
		f = (GFFamilyEntry *) l->data;
		f->fonts = g_slist_sort (f->fonts, gf_fe_sortspecies);
	}

	map->families = g_slist_sort (map->families, gf_familyentry_sortname);

	return map;
}

static void
gf_fm_load_fonts (GFFontMap * map, xmlDoc * doc)
{
	xmlNodePtr root;

	root = xmlDocGetRootElement (doc);

	if (!strcmp (root->name, "fontmap")) {
		xmlChar * version;

		version = xmlGetProp (root, "version");

		if (!version) {
			/* Version 1.0 */
			gf_fm_load_fonts_1_0 (map, root);
		} else if (!strcmp (version, "2.0")) {
			/* Version 2.0 */
			xmlFree (version);
			gf_fm_load_fonts_2_0 (map, root);
		} else {
			xmlFree (version);
		}
	}
}

static void
gf_fm_load_fonts_1_0 (GFFontMap * map, xmlNodePtr root)
{
	xmlNodePtr child;

	child = root->xmlChildrenNode;

	while (child) {
		xmlChar * format;
		format = xmlGetProp (child, "format");
		if (format) {
			if (!strcmp (format, "type1")) {
				/* We are type1 entry */
				gf_fm_load_font_1_0 (map, child);
			}
			xmlFree (format);
		}
		child = child->next;
	}
}

static void
gf_fm_load_font_1_0 (GFFontMap * map, xmlNodePtr node)
{
	GFFontEntryT1 * t1;
	GFFontEntry * e;
	gchar * alias, * p;

	alias = gf_xmlGetPropString (node, "alias");

	if (alias) {
		GFFontEntryT1Alias * t1a;
		t1a = g_new0 (GFFontEntryT1Alias, 1);
		t1a->t1.entry.type = GF_FONT_ENTRY_TYPE1_ALIAS;
		t1a->alias = alias;
		t1 = (GFFontEntryT1 *) t1a;
	} else {
		t1 = g_new0 (GFFontEntryT1, 1);
		t1->entry.type = GF_FONT_ENTRY_TYPE1;
	}

	e = (GFFontEntry *) t1;

	e->refcount = 1;
	e->face = NULL;

	t1->afm.name = gf_xmlGetPropString (node, "metrics");
	t1->pfb.name = gf_xmlGetPropString (node, "glyphs");
	e->name = gf_xmlGetPropString (node, "fullname");
	e->version = gf_xmlGetPropString (node, "version");
	e->familyname = gf_xmlGetPropString (node, "familyname");
	e->psname = gf_xmlGetPropString (node, "name");

	if (!(t1->afm.name && t1->pfb.name && e->name && e->familyname && e->psname)) {
		gf_font_entry_unref (e);
		return;
	}

	/* fixme: check integrity */
	/* fixme: load afm */

	/* Read fontmap 1.0 weight string */

	e->weight = gf_xmlGetPropString (node, "weight");
	if (e->weight) {
		t1->Weight = gf_fontmap_lookup_weight (e->weight);
	} else {
		e->weight = g_strdup ("Book");
		t1->Weight = GNOME_FONT_BOOK;
	}

	/* Discover species name */

	e->speciesname = gf_fm_get_species_name (e->name, e->familyname);

	/* Parse Italic from species name */

	p = strstr (e->speciesname, "Italic");
	if (!p) p = strstr (e->speciesname, "Oblique");

	if (p) {
		t1->ItalicAngle = -10.0;
	} else {
		t1->ItalicAngle = 0.0;
	}

	/* fixme: fixme: fixme: */

	if (g_hash_table_lookup (map->fontdict, e->name)) {
		gf_font_entry_unref (e);
		return;
	}

	g_hash_table_insert (map->fontdict, e->name, e);
	map->num_fonts++;
	map->fonts = g_slist_prepend (map->fonts, e);
}

static void
gf_fm_load_fonts_2_0 (GFFontMap * map, xmlNodePtr root)
{
	xmlNodePtr child;

	child = root->xmlChildrenNode;

	while (child) {
		if (!strcmp (child->name, "font")) {
			xmlChar * format;
			format = xmlGetProp (child, "format");
			if (format) {
				if ((!strcmp (format, "type1")) || (!strcmp (format, "type1alias"))) {
					/* We are type1/type1alias entry */
					gf_fm_load_font_2_0_t1 (map, child, strcmp (format, "type1"));
				} else if (!strcmp (format, "truetype")) {
					gf_fm_load_font_2_0_tt (map, child);
				}
				xmlFree (format);
			}
		}
		child = child->next;
	}
}

static void
gf_fm_load_font_2_0_t1 (GFFontMap * map, xmlNodePtr node, gboolean alias)
{
	GFFontEntryT1 * t1;
	GFFontEntry * e;
	gchar * p;
	xmlNodePtr child;
	xmlChar * t;

	if (alias) {
		GFFontEntryT1Alias * t1a;
		t1a = g_new0 (GFFontEntryT1Alias, 1);
		t1a->t1.entry.type = GF_FONT_ENTRY_TYPE1_ALIAS;
		t1a->alias = gf_xmlGetPropString (node, "alias");
		t1 = (GFFontEntryT1 *) t1a;
	} else {
		t1 = g_new0 (GFFontEntryT1, 1);
		t1->entry.type = GF_FONT_ENTRY_TYPE1;
	}

	child = node->xmlChildrenNode;
	while (child) {
		if (!strcmp (child->name, "file")) {
			xmlChar * type;
			type = xmlGetProp (child, "type");
			if (!strcmp (type, "afm")) {
				t1->afm.name = gf_xmlGetPropString (child, "path");
				t = xmlGetProp (child, "size");
				if (t) t1->afm.size = atoi (t);
				if (t) xmlFree (t);
				t = xmlGetProp (child, "mtime");
				if (t) t1->afm.mtime = atoi (t);
				if (t) xmlFree (t);
			} else if (!strcmp (type, "pfb")) {
				t1->pfb.name = gf_xmlGetPropString (child, "path");
				t = xmlGetProp (child, "size");
				if (t) t1->pfb.size = atoi (t);
				if (t) xmlFree (t);
				t = xmlGetProp (child, "mtime");
				if (t) t1->pfb.mtime = atoi (t);
				if (t) xmlFree (t);
			}
			xmlFree (type);
		}
		if (t1->afm.name && t1->pfb.name) break;
		child = child->next;
	}

	e = (GFFontEntry *) t1;

	e->refcount = 1;
	e->face = NULL;

	if (!(t1->afm.name && t1->pfb.name)) {
		gf_font_entry_unref (e);
		return;
	}

	e->name = gf_xmlGetPropString (node, "name");
	e->version = gf_xmlGetPropString (node, "version");
	e->familyname = gf_xmlGetPropString (node, "familyname");
	e->speciesname = gf_xmlGetPropString (node, "speciesname");
	e->psname = gf_xmlGetPropString (node, "psname");

	if (!(e->name && e->familyname && e->psname)) {
		gf_font_entry_unref (e);
		return;
	}

	/* fixme: check integrity */
	/* fixme: load afm */

	/* Read fontmap 1.0 weight string */

	e->weight = gf_xmlGetPropString (node, "weight");
	if (e->weight) {
		t1->Weight = gf_fontmap_lookup_weight (e->weight);
	} else {
		e->weight = g_strdup ("Book");
		t1->Weight = GNOME_FONT_BOOK;
	}

	/* Discover species name */

	if (!e->speciesname) e->speciesname = gf_fm_get_species_name (e->name, e->familyname);

	/* Parse Italic */

	t = xmlGetProp (node, "italicangle");
	if (!t) {
		p = strstr (e->speciesname, "Italic");
		if (!p) p = strstr (e->speciesname, "Oblique");
		if (p) {
			t1->ItalicAngle = -10.0;
		} else {
			t1->ItalicAngle = 0.0;
		}
	} else {
		t1->ItalicAngle = atof (t);
		xmlFree (t);
	}

	/* fixme: fixme: fixme: */

	if (g_hash_table_lookup (map->fontdict, e->name)) {
		gf_font_entry_unref (e);
		return;
	}

	g_hash_table_insert (map->fontdict, e->name, e);
	map->num_fonts++;
	map->fonts = g_slist_prepend (map->fonts, e);
}

static void
gf_fm_load_font_2_0_tt (GFFontMap * map, xmlNodePtr node)
{
	GFFontEntryTT * tt;
	GFFontEntry * e;
	gchar * p;
	xmlNodePtr child;
	xmlChar * t;

	tt = g_new0 (GFFontEntryTT, 1);

	tt->entry.type = GF_FONT_ENTRY_TRUETYPE;

	child = node->xmlChildrenNode;
	while (child) {
		if (!strcmp (child->name, "file")) {
			xmlChar * type;
			type = xmlGetProp (child, "type");
			if (!strcmp (type, "ttf")) {
				tt->ttf.name = gf_xmlGetPropString (child, "path");
				t = xmlGetProp (child, "size");
				if (t) tt->ttf.size = atoi (t);
				if (t) xmlFree (t);
				t = xmlGetProp (child, "mtime");
				if (t) tt->ttf.mtime = atoi (t);
				if (t) xmlFree (t);
			}
			xmlFree (type);
		}
		if (tt->ttf.name) break;
		child = child->next;
	}

	e = (GFFontEntry *) tt;

	e->refcount = 1;
	e->face = NULL;

	if (!tt->ttf.name) {
		gf_font_entry_unref (e);
		return;
	}

	e->name = gf_xmlGetPropString (node, "name");
	e->version = gf_xmlGetPropString (node, "version");
	e->familyname = gf_xmlGetPropString (node, "familyname");
	e->speciesname = gf_xmlGetPropString (node, "speciesname");
	e->psname = gf_xmlGetPropString (node, "psname");

	if (!(e->name && e->familyname && e->psname)) {
		gf_font_entry_unref (e);
		return;
	}

	/* fixme: check integrity */
	/* fixme: load afm */

	/* Read fontmap 1.0 weight string */

	e->weight = gf_xmlGetPropString (node, "weight");
	if (e->weight) {
		tt->Weight = gf_fontmap_lookup_weight (e->weight);
	} else {
		e->weight = g_strdup ("Book");
		tt->Weight = GNOME_FONT_BOOK;
	}

	/* Discover species name */

	if (!e->speciesname) e->speciesname = gf_fm_get_species_name (e->name, e->familyname);

	/* Parse Italic */

	t = xmlGetProp (node, "italicangle");
	if (!t) {
		p = strstr (e->speciesname, "Italic");
		if (!p) p = strstr (e->speciesname, "Oblique");
		if (p) {
			tt->ItalicAngle = -10.0;
		} else {
			tt->ItalicAngle = 0.0;
		}
	} else {
		tt->ItalicAngle = atof (t);
		xmlFree (t);
	}

	/* fixme: fixme: fixme: */

	if (g_hash_table_lookup (map->fontdict, e->name)) {
		gf_font_entry_unref (e);
		return;
	}

	g_hash_table_insert (map->fontdict, e->name, e);
	map->num_fonts++;
	map->fonts = g_slist_prepend (map->fonts, e);
}

static void
gf_fm_load_aliases (GFFontMap * map, xmlDoc * doc)
{
}

/*
 * Font Entry stuff
 *
 * If face is created, it has to reference entry
 */

void
gf_font_entry_ref (GFFontEntry * entry)
{
	g_return_if_fail (entry != NULL);
	/* refcount can be 1 or 2 at moment */
	g_return_if_fail (entry->refcount > 0);
	g_return_if_fail (entry->refcount < 2);

	entry->refcount++;
}

void
gf_font_entry_unref (GFFontEntry * entry)
{
	g_return_if_fail (entry != NULL);
	/* refcount can be 1 or 2 at moment */
	g_return_if_fail (entry->refcount > 0);
	g_return_if_fail (entry->refcount < 3);

	if (--entry->refcount < 1) {
		GFFontEntryTT * tt;
		GFFontEntryT1 * t1;
		GFFontEntryT1Alias * t1a;

		g_return_if_fail (entry->face == NULL);

		if (entry->name) g_free (entry->name);
		if (entry->version) g_free (entry->version);
		if (entry->familyname) g_free (entry->familyname);
		if (entry->speciesname) g_free (entry->speciesname);
		if (entry->psname) g_free (entry->psname);
		if (entry->weight) g_free (entry->weight);

		switch (entry->type) {
		case GF_FONT_ENTRY_TRUETYPE:
			tt = (GFFontEntryTT *) entry;
			if (tt->ttf.name) g_free (tt->ttf.name);
			break;
		case GF_FONT_ENTRY_TYPE1_ALIAS:
			t1a = (GFFontEntryT1Alias *) entry;
			if (t1a->alias) g_free (t1a->alias);
		case GF_FONT_ENTRY_TYPE1:
			t1 = (GFFontEntryT1 *) entry;
			if (t1->afm.name) g_free (t1->afm.name);
			if (t1->pfb.name) g_free (t1->pfb.name);
			break;
		case GF_FONT_ENTRY_ALIAS:
			break;
		default:
			g_assert_not_reached ();
			break;
		}
		g_free (entry);
	}
}

/*
 * Font list stuff
 *
 * We use Hack'O'Hacks here:
 * Getting list saves list->fontmap mapping and refs fontmap
 * Freeing list releases mapping and frees fontmap
 */

GList *
gnome_font_list ()
{
	GFFontMap * map;
	GSList * l;

	map = gf_fontmap_get ();

	if (!map->fontlist) {
		for (l = map->fonts; l != NULL; l = l->next) {
			GFFontEntry * e;
			e = (GFFontEntry *) l->data;
			map->fontlist = g_list_prepend (map->fontlist, e->name);
		}
		map->fontlist = g_list_reverse (map->fontlist);
		if (!fontlist2map) fontlist2map = g_hash_table_new (NULL, NULL);
		g_hash_table_insert (fontlist2map, map->fontlist, map);
		gf_fontmap_ref (map);
	}

	gf_fontmap_release (map);

	return map->fontlist;
}

void
gnome_font_list_free (GList * fontlist)
{
	GFFontMap * map;

	g_return_if_fail (fontlist != NULL);

	map = g_hash_table_lookup (fontlist2map, fontlist);
	g_return_if_fail (map != NULL);

	gf_fontmap_unref (map);
}

GList *
gnome_font_family_list ()
{
	GFFontMap * map;
	GSList * l;

	map = gf_fontmap_get ();

	if (!map->familylist) {
		for (l = map->families; l != NULL; l = l->next) {
			GFFamilyEntry * f;
			f = (GFFamilyEntry *) l->data;
			map->familylist = g_list_prepend (map->familylist, f->name);
		}
		map->familylist = g_list_reverse (map->familylist);
		if (!familylist2map) familylist2map = g_hash_table_new (NULL, NULL);
		g_hash_table_insert (familylist2map, map->familylist, map);
	}

	gf_fontmap_ref (map);

	gf_fontmap_release (map);

	return map->familylist;
}

void
gnome_font_family_list_free (GList * fontlist)
{
	GFFontMap * map;

	g_return_if_fail (fontlist != NULL);

	map = g_hash_table_lookup (familylist2map, fontlist);
	g_return_if_fail (map != NULL);

	gf_fontmap_unref (map);
}

/*
 * Returns newly allocated string or NULL
 */

static gchar *
gf_xmlGetPropString (xmlNodePtr node, const gchar * name)
{
	xmlChar * prop;
	gchar * str;

	prop = xmlGetProp (node, name);
	if (prop) {
		str = g_strdup (prop);
		xmlFree (prop);
		return str;
	}

	return NULL;
}

static gint
gf_fe_sortname (gconstpointer a, gconstpointer b)
{
	return strcasecmp (((GFFontEntry *) a)->name, ((GFFontEntry *) b)->name);
}

static gint
gf_fe_sortspecies (gconstpointer a, gconstpointer b)
{
	return strcasecmp (((GFFontEntry *) a)->speciesname, ((GFFontEntry *) b)->speciesname);
}

static gint
gf_familyentry_sortname (gconstpointer a, gconstpointer b)
{
	return strcasecmp (((GFFamilyEntry *) a)->name, ((GFFamilyEntry *) b)->name);
}

static gchar *
gf_fm_get_species_name (const gchar * fullname, const gchar * familyname)
{
	gchar * p;

	p = strstr (fullname, familyname);

	if (!p) return g_strdup ("Normal");

	p = p + strlen (familyname);

	while (*p && (*p < 'A')) p++;

	if (!*p) return g_strdup ("Normal");

	return g_strdup (p);
}

GnomeFontWeight
gf_fontmap_lookup_weight (const gchar * weight)
{
	static GHashTable * weights = NULL;
	GnomeFontWeight wcode;

	if (!weights) {
		weights = g_hash_table_new (g_str_hash, g_str_equal);

		g_hash_table_insert (weights, "Extra Light", GINT_TO_POINTER (GNOME_FONT_EXTRA_LIGHT));
		g_hash_table_insert (weights, "Extralight", GINT_TO_POINTER (GNOME_FONT_EXTRA_LIGHT));

		g_hash_table_insert (weights, "Thin", GINT_TO_POINTER (GNOME_FONT_THIN));

		g_hash_table_insert (weights, "Light", GINT_TO_POINTER (GNOME_FONT_LIGHT));

		g_hash_table_insert (weights, "Book", GINT_TO_POINTER (GNOME_FONT_BOOK));
		g_hash_table_insert (weights, "Roman", GINT_TO_POINTER (GNOME_FONT_BOOK));
		g_hash_table_insert (weights, "Regular", GINT_TO_POINTER (GNOME_FONT_BOOK));

		g_hash_table_insert (weights, "Medium", GINT_TO_POINTER (GNOME_FONT_MEDIUM));

		g_hash_table_insert (weights, "Semi", GINT_TO_POINTER (GNOME_FONT_SEMI));
		g_hash_table_insert (weights, "Semibold", GINT_TO_POINTER (GNOME_FONT_SEMI));
		g_hash_table_insert (weights, "Demi", GINT_TO_POINTER (GNOME_FONT_SEMI));
		g_hash_table_insert (weights, "Demibold", GINT_TO_POINTER (GNOME_FONT_SEMI));

		g_hash_table_insert (weights, "Bold", GINT_TO_POINTER (GNOME_FONT_BOLD));

		g_hash_table_insert (weights, "Heavy", GINT_TO_POINTER (GNOME_FONT_HEAVY));
 
		g_hash_table_insert (weights, "Extra", GINT_TO_POINTER (GNOME_FONT_EXTRABOLD));
		g_hash_table_insert (weights, "Extra Bold", GINT_TO_POINTER (GNOME_FONT_EXTRABOLD));

		g_hash_table_insert (weights, "Black", GINT_TO_POINTER (GNOME_FONT_BLACK));

		g_hash_table_insert (weights, "Extra Black", GINT_TO_POINTER (GNOME_FONT_EXTRABLACK));
		g_hash_table_insert (weights, "Extrablack", GINT_TO_POINTER (GNOME_FONT_EXTRABLACK));
		g_hash_table_insert (weights, "Ultra Bold", GINT_TO_POINTER (GNOME_FONT_EXTRABLACK));
	};

	wcode = GPOINTER_TO_INT (g_hash_table_lookup (weights, weight));

	return wcode;
}

static gboolean
gf_fm_is_changed (GFFontMap * map)
{
	struct stat s;

	if (map->user.name) {
		if (stat (map->user.name, &s) < 0) return TRUE;
		if (!S_ISREG (s.st_mode)) return TRUE;
		if (s.st_size != map->user.size) return TRUE;
		if (s.st_mtime != map->user.mtime) return TRUE;
	} else {
		gchar * home;
		home = getenv ("HOME");
		if (home) {
			gchar * name;
			name = g_concat_dir_and_file (home, "/.gnome/fonts/fontmap");
			if ((stat (name, &s) == 0) && S_ISREG (s.st_mode)) {
				g_free (name);
				return TRUE;
			}
		}
	}

	if (map->system.name) {
		if (stat (map->system.name, &s) < 0) return TRUE;
		if (!S_ISREG (s.st_mode)) return TRUE;
		if (s.st_size != map->system.size) return TRUE;
		if (s.st_mtime != map->system.mtime) return TRUE;
	} else {
		gchar * name;
		name = gnome_datadir_file ("fonts/fontmap");
		if (!name) name = g_concat_dir_and_file (DATADIR, "/fonts/fontmap");
		if ((stat (name, &s) == 0) && S_ISREG (s.st_mode)) {
			g_free (name);
			return TRUE;
		}
	}

	return FALSE;
}
