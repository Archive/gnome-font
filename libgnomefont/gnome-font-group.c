#define GNOME_FONT_GROUP_C

#include <config.h>
#include <glib.h>
#include <gnome-xml/parser.h>
#include <libgnomefont/gnome-font-group.h>

struct _GnomeFontGroupPrivate {
	gpointer dummy;
};

static void gnome_font_group_class_init (GnomeFontGroupClass * klass);
static void gnome_font_group_init (GnomeFontGroup * group);

static void gnome_font_group_destroy (GtkObject * object);

/* Private & temporary */

static GSList * gfglist = NULL;
static GHashTable * gdghash = NULL;

static xmlDocPtr gfg_read_groups (void);

static GtkObjectClass * parent_class = NULL;

GtkType
gnome_font_group_get_type (void)
{
	static GtkType group_type = 0;
	if (!group_type) {
		GtkTypeInfo group_info = {
			"GnomeFontGroup",
			sizeof (GnomeFontGroup),
			sizeof (GnomeFontGroupClass),
			(GtkClassInitFunc) gnome_font_group_class_init,
			(GtkObjectInitFunc) gnome_font_group_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};
		group_type = gtk_type_unique (gtk_object_get_type (), &group_info);
	}
	return group_type;
}

static void
gnome_font_group_class_init (GnomeFontGroupClass * klass)
{
	GtkObjectClass * object_class;

	object_class = (GtkObjectClass *) klass;

	parent_class = gtk_type_class (gtk_object_get_type ());

	object_class->destroy = gnome_font_group_destroy;
}

static void
gnome_font_group_init (GnomeFontGroup * group)
{
	group->private = g_new (GnomeFontGroupPrivate, 1);
}

static void
gnome_font_group_destroy (GtkObject * object)
{
	GnomeFontGroup * group;

	group = (GnomeFontGroup *) object;

	if (group->private) {
		g_free (group->private);
		group->private = NULL;
	}

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

/* Basic methods */

void
gnome_font_group_ref (GnomeFontGroup * group)
{
	g_return_if_fail (group != NULL);
	g_return_if_fail (GNOME_IS_FONT_GROUP (group));

	gtk_object_ref ((GtkObject *) group);
}

void
gnome_font_group_unref (GnomeFontGroup * group)
{
	g_return_if_fail (group != NULL);
	g_return_if_fail (GNOME_IS_FONT_GROUP (group));

	gtk_object_unref ((GtkObject *) group);
}

/* Access */

GnomeFontGroup *
gnome_font_get_group (const gchar * name)
{
	return NULL;
}

/* Private & temporary */
/* fixme: all things */

const GSList *
gnome_font_group_list (const gchar * language)
{
	static xmlDocPtr xml = NULL;
	static GSList * list = NULL;
	xmlNodePtr root, node;

	if (xml == NULL) {
		xml = gfg_read_groups ();
	}

	g_return_val_if_fail (xml != NULL, NULL);

	if (list != NULL) g_slist_free (list);
	list = NULL;

	root = xml->root;
	g_assert (root != NULL);

	for (node = root->childs; node != NULL; node = node->next) {
		xmlAttrPtr attr;
		for (attr = node->properties; attr != NULL; attr = attr->next) {
			if (strcmp (attr->name, "name") == 0) {
				list = g_slist_prepend (list, attr->val->content);
				break;
			}
		}
	}

	return list;
}

static xmlDocPtr
gfg_read_groups (void)
{
	static xmlDocPtr xml = NULL;

	if (xml == NULL) {
		static gchar * fontfile;
		fontfile = "fonts";
		xml = xmlParseFile (fontfile);
		g_free (fontfile);
	}

	return xml;
}
