#define _GNOME_RFONT_C_

/*
 * GnomeRFont
 *
 * Base class for rasterized fonts, i.e. the ones scaled to real output device
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#define unimpl() g_print ("%s:%d UNIMPLEMENTED METHOD %s\n", __FILE__, __LINE__, __FUNCTION__)

#include <config.h>
#include <math.h>
#include <libart_lgpl/art_affine.h>
#include "gnome-pgl-private.h"
#include "gnome-rfont-private.h"

static void gnome_rfont_class_init (GnomeRFontClass * klass);
static void gnome_rfont_init (GnomeRFont * rfont);

static void gnome_rfont_destroy (GtkObject * object);

static GnomeVFontClass * parent_class = NULL;

GtkType
gnome_rfont_get_type (void)
{
	static GtkType group_type = 0;
	if (!group_type) {
		GtkTypeInfo group_info = {
			"GnomeRFont",
			sizeof (GnomeRFont),
			sizeof (GnomeRFontClass),
			(GtkClassInitFunc) gnome_rfont_class_init,
			(GtkObjectInitFunc) gnome_rfont_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};
		group_type = gtk_type_unique (gnome_vfont_get_type (), &group_info);
	}
	return group_type;
}

static void
gnome_rfont_class_init (GnomeRFontClass * klass)
{
	GtkObjectClass * object_class;

	object_class = (GtkObjectClass *) klass;

	parent_class = gtk_type_class (gnome_vfont_get_type ());

	object_class->destroy = gnome_rfont_destroy;
}

static void
gnome_rfont_init (GnomeRFont * rfont)
{
	rfont->font = NULL;
	art_affine_identity (rfont->affine);
}

static void
gnome_rfont_destroy (GtkObject * object)
{
	GnomeRFont * font;

	font = (GnomeRFont *) object;

	/* fixme: unref font */

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

ArtDRect *
gnome_rfont_get_stdbbox (const GnomeRFont * rfont, ArtDRect * bbox)
{
	const ArtDRect * b;

	g_return_val_if_fail (rfont != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_RFONT (rfont), NULL);

	b = gnome_vfont_get_stdbbox (GNOME_VFONT (rfont));

	if (!b) return NULL;

	*bbox = *b;

	return bbox;
}

ArtPoint *
gnome_rfont_get_glyph_stdadvance (const GnomeRFont * rfont, gint glyphnum, ArtPoint * advance)
{
	g_return_val_if_fail (rfont != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_RFONT (rfont), NULL);

	return gnome_vfont_get_glyph_stdadvance (GNOME_VFONT (rfont), glyphnum, advance);
}

ArtDRect *
gnome_rfont_get_glyph_stdbbox (const GnomeRFont * rfont, gint glyphnum, ArtDRect * bbox)
{
	g_return_val_if_fail (rfont != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_RFONT (rfont), NULL);

	return gnome_vfont_get_glyph_stdbbox (GNOME_VFONT (rfont), glyphnum, bbox);
}

const ArtBpath *
gnome_rfont_get_glyph_stdoutline (const GnomeRFont * rfont, gint glyphnum)
{
	g_return_val_if_fail (rfont != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_RFONT (rfont), NULL);

	return gnome_vfont_get_glyph_stdoutline (GNOME_VFONT (rfont), glyphnum);
}

/*
 * convenience and compatibility methods from gnome-print
 * THESE ARE DEPRECATED by default, although some probably will be preserved
 */

/*
 * fixme: Implement what deserves implementation
 */

ArtPoint *
gnome_rfont_get_stdadvance (const GnomeRFont * rfont, ArtPoint * advance)
{
	unimpl();
	return NULL;
}

const GnomeFont *
gnome_rfont_get_font (const GnomeRFont * rfont)
{
	unimpl();
	return NULL;
}

const GnomeFontFace *
gnome_rfont_get_face (const GnomeRFont * rfont)
{
	unimpl();
	return NULL;
}

gdouble *
gnome_rfont_get_matrix (const GnomeRFont * rfont, gdouble * matrix)
{
	unimpl();
	return NULL;
}

const ArtBpath *
gnome_rfont_get_glyph_bpath (const GnomeRFont * rfont, gint glyph)
{
	unimpl();
	return NULL;
}

const ArtSVP *
gnome_rfont_get_glyph_svp (const GnomeRFont * rfont, gint glyph)
{
	unimpl();
	return NULL;
}

void
gnome_rfont_render_glyph_rgba8 (const GnomeRFont * rfont, gint glyph,
                                     guint32 rgba,
                                     gdouble x, gdouble y,
                                     guchar * buf,
                                     gint width, gint height, gint rowstride,
                                     guint flags)
{
	unimpl();
}

/* fixme: */
/* fixme: */
void
gnome_rfont_render_glyph_rgb8 (const GnomeRFont * rfont, gint glyphnum,
                                    guint32 rgba,
                                    gdouble x, gdouble y,
                                    guchar * buf,
                                    gint width, gint height, gint rowstride,
                                    guint flags)
{
	const GnomeFontGlyph * glyph;
	const GFGGrayMap * gmap;
	gint xp, yp, x0, y0, x1, y1;
	guint inkr, inkg, inkb, inka;
	guchar * s, * s0, * d, * d0;

	/* fixme: think about that whole refcounting hell */
	glyph = gnome_vfont_get_glyph (GNOME_VFONT (rfont), glyphnum);
	g_return_if_fail (glyph != NULL);

	gmap = gnome_font_glyph_get_graymap (glyph);

	xp = (gint) floor (x + 0.5);
	yp = (gint) floor (y + 0.5);

	x0 = MAX (0, xp + gmap->x0);
	y0 = MAX (0, yp + gmap->y0);
	x1 = MIN (width, xp + gmap->x0 + gmap->width);
	y1 = MIN (height, yp + gmap->y0 + gmap->height);

	inkr = rgba >> 24;
	inkg = (rgba >> 16) & 0xff;
	inkb = (rgba >> 8) & 0xff;
	inka = rgba & 0xff;

	d0 = d = buf + y0 * rowstride + x0 * 3;
	s0 = s = gmap->pixels + (y0 - yp - gmap->y0) * gmap->rowstride + (x0 - xp - gmap->x0);

	for (y = y0; y < y1; y++) {
		for (x = x0; x < x1; x++) {
			guint bgr, bgg, bgb;
			guint fgr, fgg, fgb;
			guint alpha;

			bgr = * (d + 0);
			bgg = * (d + 1);
			bgb = * (d + 2);
			alpha = (inka * (*s++) + 0x80) >> 8;

			fgr = (inkr * alpha + 0x80) >> 8;
			fgg = (inkg * alpha + 0x80) >> 8;
			fgb = (inkb * alpha + 0x80) >> 8;

			*d++ = ((bgr * (0xff - alpha) + 0x80) >> 8) + fgr;
			*d++ = ((bgg * (0xff - alpha) + 0x80) >> 8) + fgg;
			*d++ = ((bgb * (0xff - alpha) + 0x80) >> 8) + fgb;
		}

		s = s0 += gmap->rowstride;
		d = d0 += rowstride;
	}
}

/* fixme: */
/* fixme: */
void
gnome_rfont_render_pgl_rgb8 (const GnomePosGlyphList * pgl,
                                    gdouble x, gdouble y,
                                    guchar * buf,
                                    gint width, gint height, gint rowstride,
                                    guint flags)
{
	GSList * l;

	for (l = pgl->strings; l != NULL; l = l->next) {
		GnomePosString * string;
		gint i;
		string = (GnomePosString *) l->data;
		for (i = 0; i < string->length; i++) {
			/* fixme: */
			gnome_rfont_render_glyph_rgb8 (string->rfont, string->glyphs[i].glyph,
						       string->glyphs[i].color,
						       x + string->glyphs[i].x,
						       y + string->glyphs[i].y,
						       buf,width, height, rowstride,
						       flags);
		}
	}
}

void
gnome_rfont_render_glyph_gdk_drawable (const GnomeRFont * rfont, gint glyph,
                                guint32 rgba,
                                gdouble x, gdouble y,
                                GdkDrawable * drawable,
                                guint32 background,
                                guint flags)
{
	unimpl();
}

void
gnome_rfont_render_pgl_gdk_drawable (const GnomePosGlyphList * pgl,
                                gdouble x, gdouble y,
                                GdkDrawable * drawable,
                                guint32 background,
                                guint flags)
{
	unimpl();
}


