#ifndef GNOME_FONT_GROUP_H
#define GNOME_FONT_GROUP_H

#include <gtk/gtk.h>

/* FontGroup */

#define GNOME_TYPE_FONT_GROUP		(gnome_font_group_get_type ())
#define GNOME_FONT_GROUP(obj)		(GTK_CHECK_CAST ((obj), GNOME_TYPE_FONT_GROUP, GnomeFontGroup))
#define GNOME_FONT_GROUP_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_FONT_GROUP, GnomeFontGroupClass))
#define GNOME_IS_FONT_GROUP(obj)		(GTK_CHECK_TYPE ((obj), GNOME_TYPE_FONT_GROUP))
#define GNOME_IS_FONT_GROUP_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_FONT_GROUP))

typedef struct _GnomeFontGroup		GnomeFontGroup;
typedef struct _GnomeFontGroupClass	GnomeFontGroupClass;
typedef struct _GnomeFontGroupPrivate	GnomeFontGroupPrivate;

struct _GnomeFontGroup {
	GtkObject object;
	GnomeFontGroupPrivate * private;
};

struct _GnomeFontGroupClass {
	GtkObjectClass parent_class;
};

GtkType gnome_font_group_get_type (void);

void gnome_font_group_ref (GnomeFontGroup * group);
void gnome_font_group_unref (GnomeFontGroup * group);

GnomeFontGroup * gnome_font_get_group (const gchar * name);

const GSList * gnome_font_group_list (const gchar * language);

#endif
