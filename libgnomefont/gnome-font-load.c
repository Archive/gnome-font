#define GNOME_FONT_LOAD_C

/*
 * Loaders and similar things for gnome-font
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <math.h>
#include "gf-fontmap.h"
#include "gnome-font-face-private.h"
#include "gnome-font-load.h"

GnomeFontFace * gnome_font_face_ft2_new_from_file (const gchar * name, gint code);
#if 0
GnomeFontFace * gnome_font_face_ft_new_from_file (const gchar * name);
GnomeFontFace * gnome_font_face_gt1_new_from_file (const gchar * name);
#endif

GnomeFontFace *
gnome_font_face_new (const gchar * name)
{
	GFFontMap * map;
	GFFontEntry * entry;

	g_return_val_if_fail (name != NULL, NULL);

	map = gf_fontmap_get ();

	entry = g_hash_table_lookup (map->fontdict, name);

	if (entry) {
		if (!entry->face) {
			GnomeFontFace * face = NULL;
			switch (entry->type) {
			case GF_FONT_ENTRY_TRUETYPE:
				/* fixme: */
				face = gnome_font_face_ft2_new_from_file (((GFFontEntryTT *) entry)->ttf.name, 0);
				break;
			case GF_FONT_ENTRY_TYPE1:
			case GF_FONT_ENTRY_TYPE1_ALIAS:
				/* fixme: */
				face = gnome_font_face_ft2_new_from_file (((GFFontEntryT1 *) entry)->pfb.name, 0);
				break;
			default:
				break;
			}
			if (face) gnome_font_face_construct (face, entry);
		} else {
			gnome_font_face_ref (entry->face);
		}
	}

	gf_fontmap_release (map);

	if (!entry) return NULL;

	return entry->face;
}

/* fixme: */
/* fixme: Some or all that functionality should go to fontmap */

GnomeFontFace * gnome_font_unsized_closest (const char * familyname, GnomeFontWeight weight, gboolean italic)
{
	GnomeFontFace * face;
	GFFontMap * map;
	GFFamilyEntry * familyentry;
	GFFontEntry * entry;

	g_return_val_if_fail (familyname != NULL, NULL);

	map = gf_fontmap_get ();

	entry = NULL;

	familyentry = g_hash_table_lookup (map->familydict, familyname);

	if (familyentry) {
		gdouble best, dist;
		GSList * l;
		best = 1e18;
		for (l = familyentry->fonts; l != NULL; l = l->next) {
			GFFontEntry * e;
			e = (GFFontEntry *) l->data;
			/* fixme: */
			if ((e->type == GF_FONT_ENTRY_TYPE1) || (e->type == GF_FONT_ENTRY_TYPE1_ALIAS)) {
				GFFontEntryT1 * t1;
				t1 = (GFFontEntryT1 *) e;
				/* fixme: */
				dist = fabs (weight - t1->Weight) + 100.0 * (italic != (t1->ItalicAngle != 0));
				if (dist < best) {
					best = dist;
					entry = e;
				}
			} else if (e->type == GF_FONT_ENTRY_TRUETYPE) {
				GFFontEntryTT * tt;
				tt = (GFFontEntryTT *) e;
				/* fixme: */
				dist = fabs (weight - tt->Weight) + 100.0 * (italic != (tt->ItalicAngle != 0));
				if (dist < best) {
					best = dist;
					entry = e;
				}
			}
		}
	}

	if (!entry) entry = g_hash_table_lookup (map->fontdict, "Helvetica");
	if (!entry) entry = (GFFontEntry *) map->fonts->data;
	g_return_val_if_fail (entry != NULL, NULL);

	/* fixme: */
	face = gnome_font_face_new (entry->name);

	gf_fontmap_release (map);

	return face;
}

GnomeFontFace *
gnome_font_face_new_from_file (const gchar * name)
{
	GnomeFontFace * face;

	g_return_val_if_fail (name != NULL, NULL);

	face = gnome_font_face_ft2_new_from_file (name, 0);

	if (face) return face;

#if 0
#ifdef USE_FREETYPE
	face = gnome_font_face_ft_new_from_file (name);

	if (face) return face;
#endif
#ifdef USE_GT1
	face = gnome_font_face_gt1_new_from_file (name);

	if (face) return face;
#endif
#endif

	g_warning ("Unsupported file type");

	return NULL;
}


