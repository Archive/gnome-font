#ifndef GNOME_FONT_FACE_PRIVATE_H
#define GNOME_FONT_FACE_PRIVATE_H

/*
 * GnomeFontFace
 *
 * Abstract, unsized typeface handle. Note, that this is also abstract base
 * class for inheriting implementations of different font file formats
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include "gf-fontmap.h"
#include "gnome-vfont-private.h"
#include "gnome-font-face.h"

BEGIN_GNOME_DECLS

typedef struct _GnomeFontFaceMetrics    GnomeFontFaceMetrics;
typedef struct _GnomeFontFaceDesc	GnomeFontFaceDesc;

struct _GnomeFontFaceMetrics {
	ArtDRect bbox;
	ArtPoint advance;
};

/* fixme: this should go to implementations */

struct _GnomeFontFaceDesc {
	/* ASCII */
	gchar * language;

	/* Following are UTF-8 or NULL */
	gchar * family;
	gchar * species;
	gchar * name;
};

struct _GnomeFontFace {
	GnomeVFont vfont;

	GHashTable * descriptions;

	GFFontEntry * entry;
};

struct _GnomeFontFaceClass {
	GnomeVFontClass parent_class;

#if 0
	/* fill face metrics table */

	GnomeFontFaceMetrics * (* get_face_metrics) (const GnomeFontFace * face, GnomeFontFaceMetrics * metrics);
#endif

	/* get fresh allocated face description in given language */

	GnomeFontFaceDesc * (* description) (GnomeFontFace * face, const gchar * language);

	/* Get member font given master resolution */

	GnomeFont * (* get_font) (GnomeFontFace * face, gdouble size, gdouble transform[]);

	/* Set encoding ID to given encoding name, TRUE, if successful */

	gboolean (* set_encoding) (GnomeFontFace * face, gint id, const gchar * encoding);

	/* Return matching glyph in given encoding table */

	gint (* lookup) (GnomeFontFace * face, gint encoding, gint code);

	/* Returns encapsulated ps object that defines font/encoding if executed */
	guchar * (* get_ps_object) (const GnomeFontFace * face, const guchar * psname);
};

/* Our basic constructor */

GnomeFontFace * gnome_font_face_construct (GnomeFontFace * face, GFFontEntry * entry);

END_GNOME_DECLS

#endif
