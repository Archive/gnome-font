#ifndef _GNOME_RFONT_PRIVATE_H_
#define _GNOME_RFONT_PRIVATE_H_

/*
 * GnomeRFont
 *
 * Base class for rasterized fonts, i.e. the ones scaled to real output device
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include "gnome-vfont-private.h"
#include "gnome-rfont.h"

BEGIN_GNOME_DECLS

struct _GnomeRFont {
	GnomeVFont vfont;

	/* Out master font */

	GnomeFont * font;

	/* Base -> device affine transformation. NB! We use 3x2 matrix,
	 * to keep it usable from libart */

	gdouble affine[6];
};

struct _GnomeRFontClass {
	GnomeVFontClass parent_class;
};

END_GNOME_DECLS

#endif
