#define _GNOME_FONT_C_

/*
 * GnomeFont
 *
 * Base class for user visible font
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <libart_lgpl/art_affine.h>
#include "gnome-font-private.h"

#define unimpl() g_print ("%s:%d UNIMPLEMENTED METHOD %s\n", __FILE__, __LINE__, __FUNCTION__)

static void gnome_font_class_init (GnomeFontClass * klass);
static void gnome_font_init (GnomeFont * group);

static void gnome_font_destroy (GtkObject * object);

static GnomeVFontClass * parent_class = NULL;

GtkType
gnome_font_get_type (void)
{
	static GtkType group_type = 0;
	if (!group_type) {
		GtkTypeInfo group_info = {
			"GnomeFont",
			sizeof (GnomeFont),
			sizeof (GnomeFontClass),
			(GtkClassInitFunc) gnome_font_class_init,
			(GtkObjectInitFunc) gnome_font_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};
		group_type = gtk_type_unique (gnome_vfont_get_type (), &group_info);
	}
	return group_type;
}

static void
gnome_font_class_init (GnomeFontClass * klass)
{
	GtkObjectClass * object_class;

	object_class = (GtkObjectClass *) klass;

	parent_class = gtk_type_class (gnome_vfont_get_type ());

	object_class->destroy = gnome_font_destroy;
}

static void
gnome_font_init (GnomeFont * font)
{
	font->face = NULL;
	art_affine_identity (font->affine);
}

static void
gnome_font_destroy (GtkObject * object)
{
	GnomeFont * font;

	font = (GnomeFont *) object;

	if (font->face) {
		gnome_font_face_unref (font->face);
		font->face = NULL;
	}

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

/* Methods */

const GnomeFontFamily *
gnome_font_get_family (const GnomeFont * font)
{
	g_return_val_if_fail (font != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT (font), NULL);

	return gnome_font_face_get_family (font->face);
}

/* NB! Unlike face internal functions, these return name in current locale */

const gchar *
gnome_font_get_name (const GnomeFont * font)
{
	g_return_val_if_fail (font != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT (font), NULL);

	return gnome_font_face_get_name (font->face);
}

const gchar *
gnome_font_get_family_name (const GnomeFont * font)
{
	g_return_val_if_fail (font != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT (font), NULL);

	return gnome_font_face_get_family_name (font->face);
}

const gchar *
gnome_font_get_species_name (const GnomeFont * font)
{
	g_return_val_if_fail (font != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT (font), NULL);

	return gnome_font_face_get_species_name (font->face);
}

const gchar *
gnome_font_get_ps_name (const GnomeFont * font)
{
	g_return_val_if_fail (font != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT (font), NULL);

	return gnome_font_face_get_ps_name (font->face);
}

gint
gnome_font_lookup_default (const GnomeFont * font, gint unicode)
{
	g_return_val_if_fail (font != NULL, -1);
	g_return_val_if_fail (GNOME_IS_FONT (font), -1);

	return gnome_font_face_lookup_default (font->face, unicode);
}

ArtDRect *
gnome_font_get_stdbbox (const GnomeFont * font, ArtDRect * bbox)
{
	const ArtDRect * b;

	g_return_val_if_fail (font != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT (font), NULL);

	b = gnome_vfont_get_stdbbox (GNOME_VFONT (font));

	if (!b) return NULL;

	*bbox = *b;

	return bbox;
}

ArtPoint *
gnome_font_get_glyph_stdadvance (const GnomeFont * font, gint glyphnum, ArtPoint * advance)
{
	g_return_val_if_fail (font != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT (font), NULL);

	return gnome_vfont_get_glyph_stdadvance (GNOME_VFONT (font), glyphnum, advance);
}

ArtDRect *
gnome_font_get_glyph_stdbbox (const GnomeFont * font, gint glyphnum, ArtDRect * bbox)
{
	g_return_val_if_fail (font != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT (font), NULL);

	return gnome_vfont_get_glyph_stdbbox (GNOME_VFONT (font), glyphnum, bbox);
}

const ArtBpath *
gnome_font_get_glyph_stdoutline (const GnomeFont * font, gint glyphnum)
{
	g_return_val_if_fail (font != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT (font), NULL);

	return gnome_vfont_get_glyph_stdoutline (GNOME_VFONT (font), glyphnum);
}

GnomeRFont *
gnome_font_get_rfont (GnomeFont * font, gdouble affine[])
{
	g_return_val_if_fail (font != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT (font), NULL);
	g_return_val_if_fail (affine != NULL, NULL);

	if (((GnomeFontClass *) (((GtkObject *) font)->klass))->get_rfont) {
		return (* ((GnomeFontClass *) (((GtkObject *) font)->klass))->get_rfont) (font, affine);
	} else {
		g_warning ("Font does not implement get_rfont method!");
	}

	return NULL;
}

/*
 * Convenience and compatibility methods from gnome-print
 * THESE ARE DEPRECATED BY DEFAULT, although some will probably be preserved
 *
 */

/*
 * fixme: Implement those
 */

gdouble
gnome_font_get_ascender (const GnomeFont * font)
{
	const ArtDRect * bbox;

	g_return_val_if_fail (font != NULL, 0.0);
	g_return_val_if_fail (GNOME_IS_FONT (font), 0.0);

	/* fixme: this is probably not correct */
	bbox = gnome_vfont_get_stdbbox (GNOME_VFONT (font));
	g_return_val_if_fail (bbox != NULL, 0.0);

	return bbox->y1;
}

gdouble
gnome_font_get_descender (const GnomeFont * font)
{
	const ArtDRect * bbox;

	g_return_val_if_fail (font != NULL, 0.0);
	g_return_val_if_fail (GNOME_IS_FONT (font), 0.0);

	/* fixme: this is probably not correct */
	bbox = gnome_vfont_get_stdbbox (GNOME_VFONT (font));
	g_return_val_if_fail (bbox != NULL, 0.0);

	/* NB! descender is positive (usually) */
	return -bbox->y1;
}

gdouble
gnome_font_get_underline_position (const GnomeFont * font)
{
	unimpl ();
	return 0.0;
}

gdouble
gnome_font_get_underline_thickness (const GnomeFont * font)
{
	unimpl ();
	return 0.0;
}

gdouble
gnome_font_get_glyph_width (const GnomeFont * font, gint glyph)
{
	unimpl ();
	return 0.0;
}

gdouble
gnome_font_get_glyph_kerning (const GnomeFont * font, gint glyph1, gint glyph2)
{
	unimpl ();
	return 0.0;
}

GnomeFontWeight
gnome_font_get_weight_code (const GnomeFont * font)
{
	unimpl ();
	return GNOME_FONT_BOOK;
}

gboolean
gnome_font_is_italic (const GnomeFont * font)
{
	unimpl ();
	return FALSE;
}

gchar *
gnome_font_get_pfa (const GnomeFont * font)
{
	unimpl ();
	return NULL;
}

const GnomeFontFace *
gnome_font_get_face (const GnomeFont * font)
{
	g_return_val_if_fail (font != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT (font), NULL);

	return font->face;
}

gdouble
gnome_font_get_size (const GnomeFont * font)
{
	g_return_val_if_fail (font != NULL, 0.0);
	g_return_val_if_fail (GNOME_IS_FONT (font), 0.0);

	return font->size;
}

GnomeFont *
gnome_font_new (const char * name, double size)
{
	GnomeFontFace * face;
	GnomeFont * font;

	g_return_val_if_fail (name != NULL, NULL);
	g_return_val_if_fail (size > 1e-18, NULL);

	face = gnome_font_face_new (name);
	if (!face) return NULL;

	font = gnome_font_face_get_font_default (face, size);
	gnome_font_face_unref (face);

	return font;
}

GnomeFont * gnome_font_new_closest (const char *family_name,
                                   GnomeFontWeight weight,
                                   gboolean italic,
                                   double size)
{
	GnomeFontFace * face;
	GnomeFont * font;

	g_return_val_if_fail (family_name != NULL, NULL);
	g_return_val_if_fail (size > 1e-18, NULL);

	/* fixme: implement real matching */
	face = gnome_font_unsized_closest (family_name, weight, italic);
	if (!face) return NULL;

	font = gnome_font_face_get_font_default (face, size);
	gnome_font_face_unref (face);

	return font;
}

GnomeFont *
gnome_font_new_from_full_name (const char *string)
{
	unimpl ();
	return NULL;
}

char *
gnome_font_get_full_name (const GnomeFont *font)
{
	unimpl ();
	return NULL;
}

double
gnome_font_get_width_string (const GnomeFont *font, const char *s)
{
	unimpl ();
	return 0.0;
}

double
gnome_font_get_width_string_n (const GnomeFont *font, const char *s, int n)
{
	unimpl ();
	return 0.0;
}
  
GnomeDisplayFont *
gnome_font_get_display_font (GnomeFont *font)
{
	unimpl ();
	return NULL;
}

const char *
gnome_font_weight_to_string (GnomeFontWeight gfw)
{
	unimpl ();
	return NULL;
}

GnomeFontWeight
string_to_gnome_font_weight (const char *weight)
{
	unimpl ();
	return GNOME_FONT_BOOK;
}

GnomeDisplayFont *
gnome_get_display_font (const char * family,  
			GnomeFontWeight weight,
			gboolean italic,
			double points,  
			double scale)
{
	unimpl ();
	return NULL;
}
  
int
gnome_display_font_height (GnomeDisplayFont * gdf)
{
	unimpl ();
	return 0;
}

const GnomeFontFace *
gnome_display_font_get_face (const GnomeDisplayFont * gdf)
{
	unimpl ();
	return NULL;
}

const GnomeFont *
gnome_display_font_get_font (const GnomeDisplayFont * gdf)
{
	unimpl ();
	return NULL;
}

gdouble
gnome_display_font_get_scale (const GnomeDisplayFont * gdf)
{
	unimpl ();
	return 0.0;
}

const gchar *
gnome_display_font_get_x_font_name (const GnomeDisplayFont * gdf)
{
	unimpl ();
	return NULL;
}

GdkFont *
gnome_display_font_get_gdk_font (const GnomeDisplayFont * gdf)
{
	unimpl ();
	return NULL;
}

void
gnome_display_font_ref (GnomeDisplayFont * gdf)
{
	unimpl ();
	gnome_rfont_ref (gdf);
}

void
gnome_display_font_unref (GnomeDisplayFont * gdf)
{
	unimpl ();
	gnome_rfont_unref (gdf);
}


