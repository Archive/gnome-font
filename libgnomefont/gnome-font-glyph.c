#define GNOME_FONT_GLYPH_C

/*
 * GnomeFontGlyph
 *
 * Abstract base class for separate glyphs
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include "gnome-vfont-private.h"
#include "gnome-font-glyph-private.h"

static void gnome_font_glyph_class_init (GnomeFontGlyphClass * klass);
static void gnome_font_glyph_init (GnomeFontGlyph * glyph);

static void gnome_font_glyph_destroy (GtkObject * object);

static GtkObjectClass * parent_class = NULL;

GtkType
gnome_font_glyph_get_type (void)
{
	static GtkType glyph_type = 0;
	if (!glyph_type) {
		GtkTypeInfo glyph_info = {
			"GnomeFontGlyph",
			sizeof (GnomeFontGlyph),
			sizeof (GnomeFontGlyphClass),
			(GtkClassInitFunc) gnome_font_glyph_class_init,
			(GtkObjectInitFunc) gnome_font_glyph_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};
		glyph_type = gtk_type_unique (gtk_object_get_type (), &glyph_info);
	}
	return glyph_type;
}

static void
gnome_font_glyph_class_init (GnomeFontGlyphClass * klass)
{
	GtkObjectClass * object_class;

	object_class = (GtkObjectClass *) klass;

	parent_class = gtk_type_class (gtk_object_get_type ());

	object_class->destroy = gnome_font_glyph_destroy;
}

static void
gnome_font_glyph_init (GnomeFontGlyph * glyph)
{
	/* Nothing */
}

static void
gnome_font_glyph_destroy (GtkObject * object)
{
	GnomeFontGlyph * glyph;

	glyph = (GnomeFontGlyph *) object;

	/* If we have CACHED flag, user is mangled ref-ing :( */

	g_assert (!(GTK_OBJECT_FLAGS (glyph) & GFG_CACHED));

	gnome_vfont_unref (glyph->vfont);

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

void
gnome_font_glyph_ref (GnomeFontGlyph * glyph)
{
	gtk_object_ref (GTK_OBJECT (glyph));
}

void
gnome_font_glyph_unref (GnomeFontGlyph * glyph)
{
	gtk_object_ref (GTK_OBJECT (glyph));
}

const ArtBpath *
gnome_font_glyph_get_bpath (GnomeFontGlyph * glyph)
{
	g_return_val_if_fail (glyph != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_GLYPH (glyph), NULL);

	if (((GnomeFontGlyphClass *) (((GtkObject *) glyph)->klass))->bpath) {
		return (* ((GnomeFontGlyphClass *) (((GtkObject *) glyph)->klass))->bpath) (glyph);
	} else {
		g_warning ("Glyph does not implement ::bpath() method");
	}

	return NULL;
}

ArtDRect *
gnome_font_glyph_get_dimensions (GnomeFontGlyph * glyph, ArtDRect * box)
{
	ArtPoint a;

	g_return_val_if_fail (glyph != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_GLYPH (glyph), NULL);
	g_return_val_if_fail (box != NULL, NULL);

	if (((GnomeFontGlyphClass *) (((GtkObject *) glyph)->klass))->metrics) {
		(* ((GnomeFontGlyphClass *) (((GtkObject *) glyph)->klass))->metrics) (glyph, box, &a);
	} else {
		g_warning ("Glyph does not implement ::metrics() method");
	}

	return box;
}

ArtPoint *
gnome_font_glyph_get_stdadvance (GnomeFontGlyph * glyph, ArtPoint * p)
{
	ArtDRect box;

	g_return_val_if_fail (glyph != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_GLYPH (glyph), NULL);
	g_return_val_if_fail (p != NULL, NULL);

	if (((GnomeFontGlyphClass *) (((GtkObject *) glyph)->klass))->metrics) {
		(* ((GnomeFontGlyphClass *) (((GtkObject *) glyph)->klass))->metrics) (glyph, &box, p);
	} else {
		g_warning ("Glyph does not implement ::metrics() method");
	}

	return p;
}

const GFGGrayMap *
gnome_font_glyph_get_graymap (GnomeFontGlyph * glyph)
{
	g_return_val_if_fail (glyph != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_GLYPH (glyph), NULL);

	if (((GnomeFontGlyphClass *) (((GtkObject *) glyph)->klass))->graymap) {
		return (* ((GnomeFontGlyphClass *) (((GtkObject *) glyph)->klass))->graymap) (glyph);
	} else {
		g_warning ("Glyph does not implement ::graymap() method");
	}

	return NULL;
}

const GFGGdkMap *
gnome_font_glyph_get_gdkmap (GnomeFontGlyph * glyph)
{
	g_return_val_if_fail (glyph != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_GLYPH (glyph), NULL);

	if (((GnomeFontGlyphClass *) (((GtkObject *) glyph)->klass))->gdkmap) {
		(* ((GnomeFontGlyphClass *) (((GtkObject *) glyph)->klass))->gdkmap) (glyph);
	} else {
		g_warning ("Glyph does not implement ::gdkmap() method");
	}

	return NULL;
}

ArtPoint *
gnome_font_glyph_get_kerning (GnomeFontGlyph * a, GnomeFontGlyph * b, ArtPoint * p)
{
	g_return_val_if_fail (a != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_GLYPH (a), NULL);
	g_return_val_if_fail (b != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_GLYPH (b), NULL);
	g_return_val_if_fail (a->vfont == b->vfont, NULL);
	g_return_val_if_fail (p != NULL, NULL);

#if 0
	if (((GnomeVFontClass *) (((GtkObject *) a->vfont)->klass))->kerning) {
		return (* ((GnomeVFontClass *) (((GtkObject *) a->vfont)->klass))->kerning) (a->vfont, a->code, b->code, p);
	} else {
		g_warning ("VFont does not implement ::kerning() method");
	}
#endif

	p->x = 0.0;
	p->y = 0.0;

	return p;
}

