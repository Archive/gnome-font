#ifndef _GNOME_VFONT_PRIVATE_H_
#define _GNOME_VFONT_PRIVATE_H_

/*
 * GnomeVFont
 *
 * Abstract virtual base class for all glyph collection types
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include "e-cache.h"
#include "gnome-vfont.h"

struct _GnomeVFont {
	GtkObject object;
	
	guint has_info : 1;

	gint num_glyphs;
	ArtDRect bbox;
	gdouble ascender, descender, height;

	ECache * glyphs; /* Cached glyphs */
	GHashTable * locked; /* Locked glyphs */
};

struct _GnomeVFontClass {
	GtkObjectClass parent_class;

	/* Fill vfont metrics data */
	gboolean (* info) (GnomeVFont * vfont);

	/* Get glyph implementation with refcount == 1 */
	GnomeFontGlyph * (* glyph) (GnomeVFont * vfont, gint glyphnum);

#if 0
	ArtPoint * (* kerning) (GnomeVFont * vfont, gint c1, gint c2, ArtPoint * p);
#endif
};

END_GNOME_DECLS

#endif
