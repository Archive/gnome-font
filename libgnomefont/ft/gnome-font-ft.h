#ifndef GNOME_FONT_FT_H
#define GNOME_FONT_FT_H

/*
 * GnomeFontFT
 *
 * Freetype font
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <libgnomefont/gnome-font.h>

#define GNOME_TYPE_FONT_FT		(gnome_font_ft_get_type ())
#define GNOME_FONT_FT(obj)		(GTK_CHECK_CAST ((obj), GNOME_TYPE_FONT_FT, GnomeFontFT))
#define GNOME_FONT_FT_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_FONT_FT, GnomeFontFTClass))
#define GNOME_IS_FONT_FT(obj)	(GTK_CHECK_TYPE ((obj), GNOME_TYPE_FONT_FT))
#define GNOME_IS_FONT_FT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_FONT_FT))

typedef struct _GnomeFontFT		GnomeFontFT;
typedef struct _GnomeFontFTClass	GnomeFontFTClass;
typedef struct _GnomeFontFTPrivate	GnomeFontFTPrivate;

struct _GnomeFontFT {
	GnomeFont font;
	GnomeFontFTPrivate * private;
};

struct _GnomeFontFTClass {
	GnomeFontClass parent_class;
};

GtkType gnome_font_ft_get_type (void);

#endif
