#ifndef GNOME_FONT_FT_PRIVATE_H
#define GNOME_FONT_FT_PRIVATE_H

#include <freetype/freetype.h>
#include <libgnomefont/gnome-font-face.h>

struct _GnomeFontFTPrivate {
	TT_Instance tt_instance;

	GSList * rfonts;

	gint num_glyphs;
	GnomeFontGlyph ** glyphs;

	/*
	 * Font size for FT library
	 */

	gdouble dsize;
	TT_F26Dot6 size;

	/*
	 * Resolution values for FreeType library
	 * pixels_size = font_size * resolution / 72.0
	 */

	gdouble dresx, dresy;
	TT_UShort resx, resy;

	/*
	 * Remainder transformation matrix
	 * pixel_size * reminder = actual transform
	 */

	gdouble drm[6];
	TT_Matrix tt_m;
};

GnomeFont * gnome_font_ft_new (GnomeFontFace * face, gdouble size, gdouble transform[]);

#endif
