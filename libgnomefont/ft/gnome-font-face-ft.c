#define GNOME_FONT_FACE_FT_C

/*
 * GnomeFontFaceFT
 *
 * Standard TTF font file handler
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <string.h>
#include <libgnomefont/gnome-font-face.h>
#include <libgnomefont/gnome-font-face-private.h>
#include "gf-freetype.h"
#include "gnome-font-face-ft.h"
#include "gnome-font-face-ft-private.h"
#include "gnome-font-ft.h"
#include "gnome-font-ft-private.h"
#include "gnome-font-glyph-ft.h"
#include "gnome-font-glyph-ft-private.h"

static void gft_class_init (GnomeFontFaceFTClass * klass);
static void gft_init (GnomeFontFaceFT * face);

static void gft_destroy (GtkObject * object);

/* GnomeVFont methods */

static GnomeFontGlyph * gft_get_glyph (GnomeVFont * vfont, gint code);

/* GnomeFontFace methods */

static GnomeFontFaceDesc * gft_description (GnomeFontFace * face, const gchar * language);
static GnomeFont * gft_get_font (GnomeFontFace * face, gdouble size, gdouble transform[]);
static gboolean gft_set_encoding (GnomeFontFace * face, gint id, const gchar * encoding);
static gint gft_lookup (GnomeFontFace * face, gint encoding, gint code);

/* Private methods */

static gchar * gft_search_name (GnomeFontFaceFT * face, TT_UShort id, GFFT_Locale * loc);
static gchar * gft_get_name (GnomeFontFaceFT * face, TT_UShort id, GFFT_Locale * loc);

static GHashTable * pathdict = NULL;
static GnomeFontFaceClass * parent_class = NULL;

GtkType
gnome_font_face_ft_get_type (void)
{
	static GtkType face_type = 0;
	if (!face_type) {
		GtkTypeInfo face_info = {
			"GnomeFontFaceFT",
			sizeof (GnomeFontFaceFT),
			sizeof (GnomeFontFaceFTClass),
			(GtkClassInitFunc) gft_class_init,
			(GtkObjectInitFunc) gft_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};
		face_type = gtk_type_unique (gnome_font_face_get_type (), &face_info);
	}
	return face_type;
}

static void
gft_class_init (GnomeFontFaceFTClass * klass)
{
	GtkObjectClass * object_class;
	GnomeVFontClass * vfont_class;
	GnomeFontFaceClass * face_class;

	object_class = (GtkObjectClass *) klass;
	vfont_class = (GnomeVFontClass *) klass;
	face_class = (GnomeFontFaceClass *) klass;

	parent_class = gtk_type_class (gtk_object_get_type ());

	object_class->destroy = gft_destroy;

	vfont_class->get_glyph = gft_get_glyph;

	face_class->description = gft_description;
	face_class->get_font = gft_get_font;
	face_class->set_encoding = gft_set_encoding;
	face_class->lookup = gft_lookup;
}

static void
gft_init (GnomeFontFaceFT * face)
{
	face->private = g_new0 (GnomeFontFaceFTPrivate, 1);
}

static void
gft_destroy (GtkObject * object)
{
	GnomeFontFaceFT * face;

	face = (GnomeFontFaceFT *) object;

	if (face->private) {
		GnomeFontFaceFTPrivate * priv;
		gint i;

		priv = face->private;

		if (face->private->path) {
			g_hash_table_remove (pathdict, face->private->path);
			g_free (face->private->path);
		}

		/* Destroy fonts */

		while (priv->fonts) {
			gtk_object_destroy ((GtkObject *) priv->fonts->data);
			priv->fonts = g_slist_remove_link (priv->fonts, priv->fonts);
		}

		/* Destroy glyphs */

		for (i = 0; i < priv->num_glyphs; i++) {
			if (priv->glyphs[i]) gtk_object_destroy ((GtkObject *) priv->glyphs[i]);
		}

		if (priv->glyphs) g_free (priv->glyphs);

		/* Closing face closes instance too */

		TT_Close_Face (face->private->tt_face);
		g_free (face->private);
		face->private = NULL;
	}

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

/* GnomeVFont methods */

GnomeFontGlyph *
gft_get_glyph (GnomeVFont * vfont, gint code)
{
	GnomeFontFaceFT * ftface;
	GnomeFontFaceFTPrivate * priv;
	GnomeFontGlyph * glyph;

	ftface = (GnomeFontFaceFT *) vfont;
	priv = ftface->private;

	if (!priv->has_tt_inst) {
		TT_New_Instance (priv->tt_face, &priv->tt_inst);
		TT_Set_Instance_Resolutions (priv->tt_inst, 720, 720);
		TT_Set_Instance_CharSize (priv->tt_inst, 6400);
		priv->has_tt_inst = TRUE;
		priv->num_glyphs = priv->tt_props.num_Glyphs;
		priv->glyphs = g_new0 (GnomeFontGlyph *, priv->num_glyphs);
	}

	g_return_val_if_fail (code < priv->num_glyphs, NULL);

	if (priv->glyphs[code] == NULL) {
		glyph = gnome_font_glyph_ft_new (vfont,
			priv->tt_face,
			priv->tt_inst,
			code,
			GFGFT_TYPE_FACE);
		g_return_val_if_fail (glyph != NULL, NULL);
		priv->glyphs[code] = glyph;
	}

	return priv->glyphs[code];
}

static GnomeFontFaceDesc *
gft_description (GnomeFontFace * face, const gchar * language)
{
	GnomeFontFaceDesc * desc;
	const GSList * preflist;
	GFFT_Locale * gfl;
	const GSList * l;
	gchar * name;

	desc = g_new0 (GnomeFontFaceDesc, 1);

	preflist = gf_freetype_locale_preference_list (language);
	g_assert (preflist != NULL);

	desc->language = g_strdup (language);

	/* Find family name */

	name = NULL;

	for (l = preflist; l != NULL; l = l->next) {
		gfl = (GFFT_Locale *) l->data;
		/* Find preferred family, if present */
		name = gft_search_name ((GnomeFontFaceFT *) face,
			TT_NAME_ID_PREFERRED_FAMILY, gfl);
		if (name == NULL) {
			/* If no preferred family, find family */
			name = gft_search_name ((GnomeFontFaceFT *) face,
				TT_NAME_ID_FONT_FAMILY, gfl);
		}
		if (name != NULL) break;
	}

	if (name == NULL) {
		desc->family = g_strdup ("Font Error");
	} else {
		desc->family = name;
	}

	/* Find genus name */

	name = NULL;

	for (l = preflist; l != NULL; l = l->next) {
		gfl = (GFFT_Locale *) l->data;
		/* Find preferred subfamily, if present */
		name = gft_search_name ((GnomeFontFaceFT *) face,
			TT_NAME_ID_PREFERRED_SUBFAMILY, gfl);
		if (name == NULL) {
			/* If no preferred family, find family */
			name = gft_search_name ((GnomeFontFaceFT *) face,
				TT_NAME_ID_FONT_SUBFAMILY, gfl);
		}
		if (name != NULL) break;
	}

	if (name == NULL) {
		desc->species = g_strdup ("Font Error");
	} else {
		desc->species = name;
	}

	/* Find canonical name */

	for (l = preflist; l != NULL; l = l->next) {
		gfl = (GFFT_Locale *) l->data;
		desc->name = gft_search_name ((GnomeFontFaceFT *) face,
			TT_NAME_ID_FULL_NAME, gfl);
		if (desc->name != NULL) break;
	}

	if (desc->name == NULL) desc->name = g_strdup ("Font Error");

	return desc;
}

static GnomeFont *
gft_get_font (GnomeFontFace * face, gdouble size, gdouble transform[])
{
	GnomeFontFaceFT * ftf;
	GnomeFontFaceFTPrivate * priv;
	GnomeFont * font;

	ftf = (GnomeFontFaceFT *) face;
	priv = ftf->private;

	/* fixme: implement caching */

	font = gnome_font_ft_new (face, size, transform);
	g_return_val_if_fail (font != NULL, NULL);

	priv->fonts = g_slist_prepend (priv->fonts, font);

	return font;
}

/* NB! HIGHLY EXPERIMENTAL */

static gboolean
gft_set_encoding (GnomeFontFace * face, gint id, const gchar * encoding)
{
	GnomeFontFaceFT * ftface;
	GnomeFontFaceFTPrivate * priv;
	TT_UShort pid, eid;
	gint i;

	ftface = (GnomeFontFaceFT *) face;
	priv = ftface->private;

	if (strcmp (encoding, "iso-8859-1") != 0) return FALSE;

	for (i = 0; i < priv->tt_props.num_CharMaps; i++) {
		TT_Get_CharMap_ID (priv->tt_face, i, &pid, &eid);
		g_print ("charmap %d %d %d\n", i, pid, eid);
		if ((pid == 3) && (eid == 1)) {
			/* MS Unicode? */
			g_print ("found\n");
			TT_Get_CharMap (priv->tt_face, i, &priv->isomap);
			return TRUE;
		}
	}

	return FALSE;
}

/* fixme: API test */

static gint
gft_lookup (GnomeFontFace * face, gint encoding, gint code)
{
	GnomeFontFaceFT * ftface;
	GnomeFontFaceFTPrivate * priv;

	ftface = (GnomeFontFaceFT *) face;
	priv = ftface->private;

	return TT_Char_Index (priv->isomap, code);
}

/* Private & temporary methods */

GnomeFontFace *
gnome_font_face_ft_new_from_file (const gchar * path)
{
	GnomeFontFaceFT * face;
	GnomeFontFaceFTPrivate * priv;
	TT_Face tt_face;
	TT_Face_Properties tt_props;
	TT_Error result;

	g_return_val_if_fail (path != NULL, NULL);

	/* Test, if given path is already loaded */

	if (pathdict == NULL) {
		pathdict = g_hash_table_new (g_str_hash, g_str_equal);
	}

	face = g_hash_table_lookup (pathdict, path);

	if (face != NULL) {
		g_assert (GNOME_IS_FONT_FACE_FT (face));
		gnome_font_face_ref ((GnomeFontFace *) face);
		return (GnomeFontFace *) face;
	}

	/* Ensure FreeType engine is loaded */

	g_assert (GFFT);

	/* Open FreeType face handle */

	result = TT_Open_Face (GFFT_ENGINE, path, &tt_face);

	if (result != TT_Err_Ok) {
		g_warning ("Cannot open typeface file %s", path);
		return NULL;
	}

	/* Get face properties */

	result = TT_Get_Face_Properties (tt_face, &tt_props);

	if (result != TT_Err_Ok) {
		g_warning ("Cannot face properties for file %s", path);
		TT_Close_Face (tt_face);
		return NULL;
	}

	/* This function does not handle collections */

	if (tt_props.num_Faces != 1) {
		g_warning ("Font file %s is collection", path);
		TT_Close_Face (tt_face);
		return NULL;
	}

	/* Create Gnome face object */

	face = gtk_type_new (GNOME_TYPE_FONT_FACE_FT);

	priv = face->private;

	priv->tt_face = tt_face;
	priv->tt_props = tt_props;
	priv->path = g_strdup (path);

	g_hash_table_insert (pathdict, priv->path, face);

	/* fixme: register loaded face */
	/* fixme: handle collections */
	/* fixme: close face on destroy */
	/* fixme: kill glyphs on destroy */

	return (GnomeFontFace *) face;
}

/* Helpers */

static gchar *
gft_search_name (GnomeFontFaceFT * face, TT_UShort id, GFFT_Locale * loc)
{
	GnomeFontFaceFTPrivate * priv;
	gint i;
	TT_UShort p, s, l, n;
	TT_Error result;

	priv = face->private;

	for (i = 0; i < priv->tt_props.num_Names; i++) {
		result = TT_Get_Name_ID (priv->tt_face, i, &p, &s, &l, &n);
		if (result != TT_Err_Ok) return NULL;
#if 0
g_print ("found %d %d %d %d\n", p, s, l, n);
#endif
		if ((p == loc->PlatformID) &&
			(s == loc->PlatformSpecificID) &&
			(l == loc->LanguageID) &&
			(n == id)) {
			/* Found it! */
g_print ("found it: %d %d %d %d\n", p, s, l, n);
			return gft_get_name (face, i, loc);
		}
	}

	return NULL;
}

static gchar *
gft_get_name (GnomeFontFaceFT * face, TT_UShort id, GFFT_Locale * loc)
{
	GnomeFontFaceFTPrivate * priv;
	TT_String * tt_sptr;
	TT_UShort tt_length;
	TT_Error result;
	gchar * name;

	priv = face->private;

	result = TT_Get_Name_String (priv->tt_face, id, &tt_sptr, &tt_length);
	if (result != TT_Err_Ok) return NULL;

	/* fixme: fixme fixme fixme */

	name = gf_freetype_strdup_utf8 (tt_sptr, tt_length, loc);

	return name;
}

