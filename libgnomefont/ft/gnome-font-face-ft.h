#ifndef GNOME_FONT_FACE_FT_H
#define GNOME_FONT_FACE_FT_H

/*
 * GnomeFontFaceFT
 *
 * Standard TTF file handler
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <libgnomefont/gnome-font-face.h>

/* FontFaceFT */

#define GNOME_TYPE_FONT_FACE_FT		(gnome_font_face_ft_get_type ())
#define GNOME_FONT_FACE_FT(obj)		(GTK_CHECK_CAST ((obj), GNOME_TYPE_FONT_FACE_FT, GnomeFontFaceFT))
#define GNOME_FONT_FACE_FT_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_FONT_FACE_FT, GnomeFontFaceFTClass))
#define GNOME_IS_FONT_FACE_FT(obj)		(GTK_CHECK_TYPE ((obj), GNOME_TYPE_FONT_FACE_FT))
#define GNOME_IS_FONT_FACE_FT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_FONT_FACE_FT))

typedef struct _GnomeFontFaceFT		GnomeFontFaceFT;
typedef struct _GnomeFontFaceFTClass	GnomeFontFaceFTClass;
typedef struct _GnomeFontFaceFTPrivate	GnomeFontFaceFTPrivate;

struct _GnomeFontFaceFT {
	GnomeFontFace face;
	GnomeFontFaceFTPrivate * private;
};

struct _GnomeFontFaceFTClass {
	GnomeFontFaceClass parent_class;
};

GtkType gnome_font_face_ft_get_type (void);

#endif
