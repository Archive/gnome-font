#ifndef GNOME_RFONT_FT_H
#define GNOME_RFONT_FT_H

/*
 * GnomeRFontFT
 *
 * Freetype rasterized font implementation
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <libgnomefont/gnome-rfont.h>

#define GNOME_TYPE_RFONT_FT		(gnome_rfont_ft_get_type ())
#define GNOME_RFONT_FT(obj)		(GTK_CHECK_CAST ((obj), GNOME_TYPE_RFONT_FT, GnomeRFontFT))
#define GNOME_RFONT_FT_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_RFONT_FT, GnomeRFontFTClass))
#define GNOME_IS_RFONT_FT(obj)		(GTK_CHECK_TYPE ((obj), GNOME_TYPE_RFONT_FT))
#define GNOME_IS_RFONT_FT_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_RFONT_FT))

typedef struct _GnomeRFontFT		GnomeRFontFT;
typedef struct _GnomeRFontFTClass	GnomeRFontFTClass;
typedef struct _GnomeRFontFTPrivate	GnomeRFontFTPrivate;

struct _GnomeRFontFT {
	GnomeRFont rfont;
	GnomeRFontFTPrivate * private;
};

struct _GnomeRFontFTClass {
	GnomeRFontClass parent_class;
};

GtkType gnome_rfont_ft_get_type (void);

#endif
