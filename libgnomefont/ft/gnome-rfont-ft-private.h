#ifndef GNOME_RFONT_FT_PRIVATE_H
#define GNOME_RFONT_FT_PRIVATE_H

#include <freetype/freetype.h>
#include <libgnomefont/gnome-font.h>

struct _GnomeRFontFTPrivate {
	TT_Instance tt_instance;

	gint num_glyphs;
	GnomeFontGlyph ** glyphs;

	/*
	 * Font size for FT library
	 */

	gdouble dsize;
	TT_F26Dot6 size;

	/*
	 * Resolution values for for FreeType library
	 * pixel_size = font->size * resolution / 72.0
	 */

	gdouble dresx, dresy;
	TT_UShort resx, resy;

	/*
	 * Remainder transformation matrix
	 * pixel_size * reminder = actual transform
	 */

	gdouble drm[6];
	TT_Matrix tt_m;
};

GnomeRFont * gnome_rfont_ft_new (GnomeFont * font, gdouble affine[]);

#endif
