#define GNOME_FONT_FT_C

/*
 * GnomeFontFT
 *
 * FreeType font
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <math.h>
#include "gnome-font-face-ft.h"
#include "gnome-font-face-ft-private.h"
#include "gnome-font-ft.h"
#include "gnome-font-ft-private.h"
#include "gnome-rfont-ft.h"
#include "gnome-rfont-ft-private.h"
#include "gnome-font-glyph-ft.h"
#include "gnome-font-glyph-ft-private.h"

static void gnome_font_ft_class_init (GnomeFontFTClass * klass);
static void gnome_font_ft_init (GnomeFontFT * group);

static void gnome_font_ft_destroy (GtkObject * object);

static GnomeFontGlyph * gnome_font_ft_get_glyph (GnomeVFont * vfont, gint code);

static GnomeRFont * gnome_font_ft_get_rfont (GnomeFont * font, gdouble affine[]);

static GnomeFontClass * parent_class = NULL;

GtkType
gnome_font_ft_get_type (void)
{
	static GtkType group_type = 0;
	if (!group_type) {
		GtkTypeInfo group_info = {
			"GnomeFontFT",
			sizeof (GnomeFontFT),
			sizeof (GnomeFontFTClass),
			(GtkClassInitFunc) gnome_font_ft_class_init,
			(GtkObjectInitFunc) gnome_font_ft_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};
		group_type = gtk_type_unique (gnome_font_get_type (), &group_info);
	}
	return group_type;
}

static void
gnome_font_ft_class_init (GnomeFontFTClass * klass)
{
	GtkObjectClass * object_class;
	GnomeVFontClass * vfont_class;
	GnomeFontClass * font_class;

	object_class = (GtkObjectClass *) klass;
	vfont_class = (GnomeVFontClass *) klass;
	font_class = (GnomeFontClass *) klass;

	parent_class = gtk_type_class (gnome_font_get_type ());

	object_class->destroy = gnome_font_ft_destroy;

	vfont_class->get_glyph = gnome_font_ft_get_glyph;

	font_class->get_rfont = gnome_font_ft_get_rfont;
}

static void
gnome_font_ft_init (GnomeFontFT * font)
{
	font->private = g_new0 (GnomeFontFTPrivate, 1);
}

static void
gnome_font_ft_destroy (GtkObject * object)
{
	GnomeFontFT * font;

	font = (GnomeFontFT *) object;

	if (font->private) {
		GnomeFontFTPrivate * priv;
		gint i;

		priv = font->private;

		while (priv->rfonts) {
			gtk_object_destroy ((GtkObject *) priv->rfonts->data);
			priv->rfonts = g_slist_remove_link (priv->rfonts, priv->rfonts);
		}

		for (i = 0; i < priv->num_glyphs; i++) {
			if (priv->glyphs[i]) gtk_object_destroy ((GtkObject *) priv->glyphs[i]);
		}

		if (priv->glyphs) g_free (priv->glyphs);

		g_free (font->private);
		font->private = NULL;
	}

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

/* fixme: */

static GnomeFontGlyph *
gnome_font_ft_get_glyph (GnomeVFont * vfont, gint code)
{
	GnomeFontFT * ftfont;
	GnomeFontFTPrivate * priv;
	GnomeFontFaceFT * ftface;
	GnomeFontGlyph * glyph;

	ftface = (GnomeFontFaceFT *) ((GnomeFont *) vfont)->face;
	ftfont = (GnomeFontFT *) vfont;
	priv = ftfont->private;

	if (!priv->glyphs) {
		priv->num_glyphs = ftface->private->tt_props.num_Glyphs;
		priv->glyphs = g_new0 (GnomeFontGlyph *, priv->num_glyphs);
	}

	g_return_val_if_fail (code < priv->num_glyphs, NULL);

	if (priv->glyphs[code] == NULL) {
		glyph = gnome_font_glyph_ft_new (vfont,
			ftface->private->tt_face,
			priv->tt_instance,
			code,
			GFGFT_TYPE_FONT);
		g_return_val_if_fail (glyph != NULL, NULL);
		priv->glyphs[code] = glyph;
	}

	return priv->glyphs[code];
}

/* fixme: */

static GnomeRFont *
gnome_font_ft_get_rfont (GnomeFont * font, gdouble affine[])
{
	GnomeFontFT * ftfont;
	GnomeFontFTPrivate * priv;
	GnomeRFont * rfont;

	ftfont = (GnomeFontFT *) font;
	priv = ftfont->private;

	rfont = gnome_rfont_ft_new (font, affine);
	g_return_val_if_fail (rfont != NULL, NULL);

	priv->rfonts = g_slist_prepend (priv->rfonts, rfont);

	return rfont;
}

/* Methods */

GnomeFont *
gnome_font_ft_new (GnomeFontFace * face, gdouble size, gdouble transform[])
{
	GnomeFontFaceFT * ftface;
	GnomeFontFaceFTPrivate * ftfacepriv;
	GnomeFont * font;
	GnomeFontFT * ftfont;
	GnomeFontFTPrivate * priv;
	TT_Error result;

	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE_FT (face), NULL);

	ftface = (GnomeFontFaceFT *) face;
	ftfacepriv = ftface->private;

	font = gtk_type_new (GNOME_TYPE_FONT_FT);
	ftfont = (GnomeFontFT *) font;
	priv = ftfont->private;

	font->face = face;
	font->size = size;
	memcpy (font->affine, transform, 4 * sizeof (double));
	font->affine[4] = font->affine[5] = 0.0;

	/* fixme: add rounding remainder to transformation matrix */

	priv->size = (gint) (font->size * 64.0 + 0.5);
	priv->dsize = (gdouble) priv->size / 64.0;

	/* Find best approximation of resolution */

	priv->dresx = rint (hypot (transform[0], transform[1]) * 72.0);
	priv->dresy = rint (hypot (transform[2], transform[3]) * 72.0);
	priv->resx = (gint) priv->dresx;
	priv->resy = (gint) priv->dresy;

	/* Find remainder matrix */

	priv->drm[0] = transform[0] * 72.0 / priv->dresx;
	priv->drm[1] = transform[1] * 72.0 / priv->dresx;
	priv->drm[2] = transform[2] * 72.0 / priv->dresy;
	priv->drm[3] = transform[3] * 72.0 / priv->dresy;
	priv->drm[4] = priv->drm[5] = 0.0;

	priv->tt_m.xx = rint (priv->drm[0] * 65536.0);
	priv->tt_m.xy = rint (priv->drm[1] * 65536.0);
	priv->tt_m.yx = rint (priv->drm[2] * 65536.0);
	priv->tt_m.yy = rint (priv->drm[3] * 65536.0);

	/* Now we have font transformation matrixes set up :) */

	/* fixme: error checking */

	result = TT_New_Instance (ftfacepriv->tt_face,
		&priv->tt_instance);
	g_return_val_if_fail (result == TT_Err_Ok, NULL);

	TT_Set_Instance_Resolutions (priv->tt_instance,
		priv->resx, priv->resy);
	TT_Set_Instance_CharSize (priv->tt_instance,
		priv->size);

	return font;
}


