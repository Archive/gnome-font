#ifndef GNOME_FONT_FACE_FT_PRIVATE_H
#define GNOME_FONT_FACE_FT_PRIVATE_H

#include <freetype/freetype.h>
#include <libgnomefont/gnome-font-face.h>

struct _GnomeFontFaceFTPrivate {

	/* Flags (we can use GtkObject flags at one point) */

	guint has_tt_inst : 1;	/* Whether TT instance has been created */

	/* Font filename */
	gchar * path;

	/* TT handles */
	TT_Face tt_face;
	TT_Face_Properties tt_props;

	/* Unscaled instance */
	TT_Instance tt_inst;

	GSList * fonts;

	gint num_glyphs;
	GnomeFontGlyph ** glyphs;

	/* dung */
	TT_CharMap isomap;
};

#define GF_F_FT_Units_Per_EM(f) (GNOME_FONT_FACE_FT (f)->private->tt_props.header->Units_Per_EM)

GnomeFontFace * gnome_font_face_ft_new_from_file (const gchar * path);

#endif
