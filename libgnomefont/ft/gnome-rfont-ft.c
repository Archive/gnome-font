#define GNOME_RFONT_FT_C

/*
 * GnomeRFontFT
 *
 * Freetype rasterized font implementation
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <math.h>
#include "gnome-font-face-ft.h"
#include "gnome-font-face-ft-private.h"
#include "gnome-font-ft.h"
#include "gnome-font-ft-private.h"
#include "gnome-rfont-ft.h"
#include "gnome-rfont-ft-private.h"
#include "gnome-font-glyph-ft.h"
#include "gnome-font-glyph-ft-private.h"

static void gnome_rfont_ft_class_init (GnomeRFontFTClass * klass);
static void gnome_rfont_ft_init (GnomeRFontFT * group);

static void gnome_rfont_ft_destroy (GtkObject * object);

static GnomeFontGlyph * gnome_rfont_ft_get_glyph (GnomeVFont * vfont, gint code);

static GnomeRFontClass * parent_class = NULL;

GtkType
gnome_rfont_ft_get_type (void)
{
	static GtkType group_type = 0;
	if (!group_type) {
		GtkTypeInfo group_info = {
			"GnomeRFontFT",
			sizeof (GnomeRFontFT),
			sizeof (GnomeRFontFTClass),
			(GtkClassInitFunc) gnome_rfont_ft_class_init,
			(GtkObjectInitFunc) gnome_rfont_ft_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};
		group_type = gtk_type_unique (gnome_rfont_get_type (), &group_info);
	}
	return group_type;
}

static void
gnome_rfont_ft_class_init (GnomeRFontFTClass * klass)
{
	GtkObjectClass * object_class;
	GnomeVFontClass * vfont_class;

	object_class = (GtkObjectClass *) klass;
	vfont_class = (GnomeVFontClass *) klass;

	parent_class = gtk_type_class (gnome_rfont_get_type ());

	object_class->destroy = gnome_rfont_ft_destroy;

	vfont_class->get_glyph = gnome_rfont_ft_get_glyph;
}

static void
gnome_rfont_ft_init (GnomeRFontFT * font)
{
	font->private = g_new0 (GnomeRFontFTPrivate, 1);
}

static void
gnome_rfont_ft_destroy (GtkObject * object)
{
	GnomeRFontFT * font;

	font = (GnomeRFontFT *) object;

	if (font->private) {
		GnomeRFontFTPrivate * priv;
		gint i;

		priv = font->private;

		for (i = 0; i < priv->num_glyphs; i++) {
			gtk_object_destroy ((GtkObject *) priv->glyphs[i]);
		}

		if (priv->glyphs) g_free (priv->glyphs);

		g_free (priv);
		font->private = NULL;
	}

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

/* fixme: */

static GnomeFontGlyph *
gnome_rfont_ft_get_glyph (GnomeVFont * vfont, gint code)
{
	GnomeRFontFT * rfontft;
	GnomeRFontFTPrivate * priv;
	GnomeFontFaceFT * ftface;
	GnomeFontGlyph * glyph;

	/* We can rely on having instance, hopefully :) */
	/* But we NEED dictionary still :( */
	/* And here we need grid-fitting too */

	ftface = (GnomeFontFaceFT *) ((GnomeRFont *) vfont)->font->face;
	rfontft = (GnomeRFontFT *) vfont;
	priv = rfontft->private;

	if (!priv->glyphs) {
		priv->num_glyphs = ftface->private->tt_props.num_Glyphs;
		priv->glyphs = g_new0 (GnomeFontGlyph *, priv->num_glyphs);
	}

	g_return_val_if_fail (code < priv->num_glyphs, NULL);

	if (priv->glyphs[code] == NULL) {
		glyph = gnome_font_glyph_ft_new (vfont,
			ftface->private->tt_face,
			priv->tt_instance,
			code,
			GFGFT_TYPE_RFONT);
		g_return_val_if_fail (glyph != NULL, NULL);
		priv->glyphs[code] = glyph;
	}

	return priv->glyphs[code];
}

/* Methods */

GnomeRFont *
gnome_rfont_ft_new (GnomeFont * font, gdouble affine[])
{
	GnomeRFont * rfont;
	GnomeRFontFT * rfontft;
	GnomeRFontFTPrivate * priv;

	g_return_val_if_fail (font != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FT (font), NULL);

	rfontft = gtk_type_new (GNOME_TYPE_RFONT_FT);
	rfont = (GnomeRFont *) rfontft;
	priv = rfontft->private;

	/* fixme: ref font */

	/* Fill device values in GnomeRFont struct */

	rfont->font = font;
	memcpy (rfont->affine, affine, 4 * sizeof (gdouble));
	rfont->affine[4] = rfont->affine[5] = 0.0;

	/* Fill FT device values in GnomeRFontFT struct */

	/* Find best approximation of font size */

	/* fixme: add rounding remainder to transformation matrix */

	priv->size = (gint) (font->size * 64.0 + 0.5);
	priv->dsize = (gdouble) priv->size / 64.0;

	/* Find best approximation of resolution */

	priv->dresx = rint (hypot (affine[0], affine[1]) * 72.0);
	priv->dresy = rint (hypot (affine[2], affine[3]) * 72.0);
	priv->resx = (gint) priv->dresx;
	priv->resy = (gint) priv->dresy;

	/* Find remainder matrix */

	priv->drm[0] = affine[0] * 72.0 / priv->dresx;
	priv->drm[1] = affine[1] * 72.0 / priv->dresx;
	priv->drm[2] = affine[2] * 72.0 / priv->dresy;
	priv->drm[3] = affine[3] * 72.0 / priv->dresy;
	priv->drm[4] = priv->drm[5] = 0.0;

	priv->tt_m.xx = rint (priv->drm[0] * 65536.0);
	priv->tt_m.xy = rint (priv->drm[1] * 65536.0);
	priv->tt_m.yx = rint (priv->drm[2] * 65536.0);
	priv->tt_m.yy = rint (priv->drm[3] * 65536.0);

	/* Now we have font transformation matrixes set up :) */

	/* fixme: error checking */

	TT_New_Instance (((GnomeFontFaceFT *) font->face)->private->tt_face,
		&priv->tt_instance);

	TT_Set_Instance_Resolutions (priv->tt_instance,
		priv->resx,
		priv->resy);

	TT_Set_Instance_CharSize (priv->tt_instance,
		priv->size);

	return rfont;
}


