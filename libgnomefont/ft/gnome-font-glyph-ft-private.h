#ifndef GNOME_FONT_GLYPH_FT_PRIVATE_H
#define GNOME_FONT_GLYPH_FT_PRIVATE_H

#include <freetype/freetype.h>
#include <libgnomefont/gnome-vfont.h>
#include <libgnomefont/gnome-font-glyph.h>

typedef enum {
	GFGFT_TYPE_NONE,
	GFGFT_TYPE_FACE,
	GFGFT_TYPE_FONT,
	GFGFT_TYPE_RFONT
} GFGFTType;

struct _GnomeFontGlyphFTPrivate {
	GnomeVFont * vfont;
	GFGFTType type;
	guint can_outline : 1;
	guint err_outline : 1;
	guint can_graymap : 1;
	guint err_graymap : 1;
	TT_Glyph tt_glyph;
	TT_Big_Glyph_Metrics tt_bigmetrics;
	ArtBpath * bpath;

	/*
	 * This is remainder matrix, transforming already scaled glyph
	 * to real raster (rotation etc.)
	 */

	GFGGrayMap graymap;
	GFGGdkMap gdkmap;
};

GnomeFontGlyph * gnome_font_glyph_ft_new (GnomeVFont * vfont, TT_Face face, TT_Instance inst, gint code, GFGFTType type);

#endif
