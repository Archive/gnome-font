#ifndef GNOME_FONT_GLYPH_FT_H
#define GNOME_FONT_GLYPH_FT_H

/*
 * GnomeFontGlyphFT
 *
 * FreeType glyph
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <libgnomefont/gnome-font-glyph.h>

/* FontGlyphFT */

#define GNOME_TYPE_FONT_GLYPH_FT		(gnome_font_glyph_ft_get_type ())
#define GNOME_FONT_GLYPH_FT(obj)		(GTK_CHECK_CAST ((obj), GNOME_TYPE_FONT_GLYPH_FT, GnomeFontGlyphFT))
#define GNOME_FONT_GLYPH_FT_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_FONT_GLYPH_FT, GnomeFontGlyphFTClass))
#define GNOME_IS_FONT_GLYPH_FT(obj)		(GTK_CHECK_TYPE ((obj), GNOME_TYPE_FONT_GLYPH_FT))
#define GNOME_IS_FONT_GLYPH_FT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_FONT_GLYPH_FT))

typedef struct _GnomeFontGlyphFT	GnomeFontGlyphFT;
typedef struct _GnomeFontGlyphFTClass	GnomeFontGlyphFTClass;
typedef struct _GnomeFontGlyphFTPrivate	GnomeFontGlyphFTPrivate;

struct _GnomeFontGlyphFT {
	GnomeFontGlyph glyph;
	GnomeFontGlyphFTPrivate * private;
};

struct _GnomeFontGlyphFTClass {
	GnomeFontGlyphClass parent_class;
};

GtkType gnome_font_glyph_ft_get_type (void);

#endif
