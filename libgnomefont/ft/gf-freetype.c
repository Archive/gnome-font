#define GF_FREETYPE_C

/*
 * FreeType library plug
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <string.h>
#include <unicode.h>
#include "gf-freetype.h"

static gchar * gfft_strdup_mac_utf8 (TT_String * src, TT_UShort length, GFFT_Locale * loc);
static gchar * gfft_strdup_utf16_utf8 (TT_String * src, TT_UShort length, GFFT_Locale * loc);

gboolean
gf_freetype_init (void)
{
	TT_Byte palette[] = {0x00, 0x3f, 0x7f, 0xbf, 0xff};
	TT_Error result;

	if (gfft_initialized) return TRUE;

	result = TT_Init_FreeType (&gnome_font_TT_Engine);
	if (result != TT_Err_Ok) return FALSE;

	TT_Set_Raster_Gray_Palette (gnome_font_TT_Engine, palette);

	gfft_initialized = TRUE;

	return TRUE;
}

/* fixme: either implement or rethink this */

static GFFT_Locale gfl[] = {
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_DEFAULT, TT_MAC_LANGID_ENGLISH},
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_UNICODE_2_0, TT_MAC_LANGID_ENGLISH},
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_UNICODE_1_1, TT_MAC_LANGID_ENGLISH},
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_ISO_10646, TT_MAC_LANGID_ENGLISH},

	{TT_PLATFORM_MACINTOSH, TT_MAC_ID_ROMAN, TT_MAC_LANGID_ENGLISH},

	{TT_PLATFORM_MICROSOFT, TT_MS_ID_UNICODE_CS, TT_MS_LANGID_ENGLISH_UNITED_STATES}
};
gint gfl_length = (sizeof (gfl) / sizeof (gfl[0]));

static GFFT_Locale gfl_fr_FR[] = {
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_DEFAULT, TT_MAC_LANGID_FRENCH},
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_UNICODE_2_0, TT_MAC_LANGID_FRENCH},
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_UNICODE_1_1, TT_MAC_LANGID_FRENCH},
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_ISO_10646, TT_MAC_LANGID_FRENCH},

	{TT_PLATFORM_MACINTOSH, TT_MAC_ID_ROMAN, TT_MAC_LANGID_FRENCH},

	{TT_PLATFORM_MICROSOFT, TT_MS_ID_UNICODE_CS, TT_MS_LANGID_FRENCH_FRANCE}
};
gint gfl_fr_FR_length = (sizeof (gfl_fr_FR) / sizeof (gfl_fr_FR[0]));

const GSList *
gf_freetype_locale_preference_list (const gchar * language)
{
	static GSList * l = NULL;
	gint i;

	/* Fake locale preferences */

	if (strcmp (language, "fr_FR") == 0) {
		for (i = gfl_fr_FR_length - 1; i >= 0; i--) {
			l = g_slist_prepend (l, &gfl_fr_FR[i]);
		}
		return l;
	}

	if (l == NULL) {
		for (i = gfl_length - 1; i >= 0; i--) {
			l = g_slist_prepend (l, &gfl[i]);
		}
	}

	return l;
}

/* fixme: */

gchar *
gf_freetype_strdup_utf8 (TT_String * src, TT_UShort length, GFFT_Locale * loc)
{
	g_assert (loc != NULL);

	switch (loc->PlatformID) {
	case TT_PLATFORM_APPLE_UNICODE:
	case TT_PLATFORM_MICROSOFT:
	case TT_PLATFORM_ISO:
		return gfft_strdup_utf16_utf8 (src, length, loc);
		break;
	case TT_PLATFORM_MACINTOSH:
		return gfft_strdup_mac_utf8 (src, length, loc);
		break;
	default:
		g_assert_not_reached ();
		break;
	}

	return NULL;
}

/* fixme: do real thing */

static gchar *
gfft_strdup_mac_utf8 (TT_String * src, TT_UShort length, GFFT_Locale * loc)
{
	gchar * dst;

	dst = g_new (gchar, (length + 1) * sizeof (gchar));
	memcpy (dst, src, length * sizeof (gchar));
	* (dst + length) = '\0';

	return dst;
}

/* fixme: */

static gchar *
gfft_strdup_utf16_utf8 (TT_String * src, TT_UShort length, GFFT_Locale * loc)
{
	static gboolean uni_initialized = FALSE;
	static unicode_iconv_t uiconv = NULL;
	gchar * tmp, * dst;
	const char * ip;
	char * op;
	size_t il, ol;

	if (!uni_initialized) {
		unicode_init ();
		uiconv = unicode_iconv_open ("UTF-8", "UTF-16");
		if (uiconv == (unicode_iconv_t) -1) {
			g_warning ("Error creating UTF-16 -> UTF-8 converter");
			return NULL;
		}
		uni_initialized = TRUE;
	}

	ip = src;
	il = length;

	tmp = g_new0 (gchar, length * 8 + 1);
	op = tmp;
	ol = length * 8;

	unicode_iconv (uiconv, &ip, &il, &op, &ol);

	dst = g_strdup (tmp);

	g_free (tmp);

	return dst;
}

/* Bpath methods */

static ArtBpath *
gfgft_moveto (ArtBpath * p, gdouble x, gdouble y)
{
	p->code = ART_MOVETO;
	p->x3 = x;
	p->y3 = y;

	return ++p;
}

static ArtBpath *
gfgft_lineto (ArtBpath * p, gdouble x, gdouble y)
{
	p->code = ART_LINETO;
	p->x3 = x;
	p->y3 = y;

	return ++p;
}

static ArtBpath *
gfgft_curveto (ArtBpath * p,
	gdouble sx, gdouble sy,
	gdouble midx, gdouble midy,
	gdouble x, gdouble y)
{
	gdouble x0, y0, x1, y1, x2, y2, x3, y3;
	gdouble mx, my;

	x0 = sx;
	y0 = sy;

	mx = midx;
	my = midy;

	x3 = x;
	y3 = y;

	x1 = x0 + 2 * (mx - x0) / 3;
	y1 = y0 + 2 * (my - y0) / 3;

	x2 = mx + (x3 - mx) / 3;
	y2 = my + (y3 - my) / 3;

	p->code = ART_CURVETO;
	p->x1 = x1;
	p->y1 = y1;
	p->x2 = x2;
	p->y2 = y2;
	p->x3 = x3;
	p->y3 = y3;

	return ++p;
}

static ArtBpath *
gfgft_end (ArtBpath * p)
{
	p->code = ART_END;

	return ++p;
}

/* 1 - on, 0 - off */

ArtBpath *
gf_freetype_bpath_from_outline (TT_Outline * ol, gdouble xscale, gdouble yscale)
{
	gint ctr, pt;
	gint idx;
	TT_Vector * v;
	ArtBpath * bp, * p;
	gint code, lastcode;
	gdouble x0, y0, startx, starty, lastx, lasty, midx, midy, x, y;

	g_assert (ol != NULL);

	bp = g_new (ArtBpath, ol->n_points * 2 + ol->n_contours + 1);

	idx = 0;
	p = bp;
	for (ctr = 0; ctr < ol->n_contours; ctr++) {
		v = ol->points + idx;
		x0 = startx = v->x * xscale;
		y0 = starty = v->y * yscale;
		p = gfgft_moveto (p, startx, starty);
		g_assert ((ol->flags[idx] & 0x1) != 0);
		lastcode = 1;
		lastx = startx;
		lasty = starty;
		idx++;
		for (pt = idx; pt <= ol->contours[ctr]; pt++) {
			v = ol->points + pt;
			code = ol->flags[pt];
			x = v->x * xscale;
			y = v->y * yscale;
			if (code != 0) {
				if (lastcode == 1) {
					/* Simple lineto */
					p = gfgft_lineto (p, x, y);
					startx = x;
					starty = y;
				} else {
					/* Curveto close */
					p = gfgft_curveto (p, startx, starty, lastx, lasty, x, y);
					startx = x;
					starty = y;
				}
			} else {
				if (lastcode == 1) {
					/* Curveto open - nothing */
				} else {
					/* Midpoint, close, open */
					midx = (lastx + x) / 2;
					midy = (lasty + y) / 2;
					p = gfgft_curveto (p, startx, starty, lastx, lasty, midx, midy);
					startx = midx;
					starty = midy;
				}
			}
			lastcode = code;
			lastx = x;
			lasty = y;
		}
		if (lastcode == 1) {
			/* Simple lineto */
			p = gfgft_lineto (p, x0, y0);
		} else {
			/* Curveto close */
			p = gfgft_curveto (p, startx, starty, lastx, lasty, x0, y0);
		}
		idx = pt;
	}
	p = gfgft_end (p);
#if 0
	bp = g_renew (ArtBpath, bp, p - bp);
#endif
	return bp;
}

