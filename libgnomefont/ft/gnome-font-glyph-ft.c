#define GNOME_FONTGLYPH_FT_C

/*
 * GnomeFontGlyphFT
 *
 * FreeType glyph
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <libart_lgpl/art_affine.h>
#include "gf-freetype.h"
#include "gnome-font-glyph-ft.h"
#include "gnome-font-glyph-ft-private.h"
#include "gnome-font-face-ft.h"
#include "gnome-font-face-ft-private.h"
#include "gnome-font-ft.h"
#include "gnome-font-ft-private.h"
#include "gnome-rfont-ft.h"
#include "gnome-rfont-ft-private.h"

static void gnome_font_glyph_ft_class_init (GnomeFontGlyphFTClass * klass);
static void gnome_font_glyph_ft_init (GnomeFontGlyphFT * glyph);

static void gnome_font_glyph_ft_destroy (GtkObject * object);

static const ArtBpath * gfgft_bpath (GnomeFontGlyph * glyph);
static void gfgft_metrics (GnomeFontGlyph * glyph, ArtDRect * bbox, ArtPoint * a);
static const GFGGrayMap * gfgft_graymap (GnomeFontGlyph * glyph);
static const GFGGdkMap * gfgft_gdkmap (GnomeFontGlyph * glyph);

static GnomeFontGlyphClass * parent_class = NULL;

GtkType
gnome_font_glyph_ft_get_type (void)
{
	static GtkType glyph_type = 0;
	if (!glyph_type) {
		GtkTypeInfo glyph_info = {
			"GnomeFontGlyphFT",
			sizeof (GnomeFontGlyphFT),
			sizeof (GnomeFontGlyphFTClass),
			(GtkClassInitFunc) gnome_font_glyph_ft_class_init,
			(GtkObjectInitFunc) gnome_font_glyph_ft_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};
		glyph_type = gtk_type_unique (gnome_font_glyph_get_type (), &glyph_info);
	}
	return glyph_type;
}

static void
gnome_font_glyph_ft_class_init (GnomeFontGlyphFTClass * klass)
{
	GtkObjectClass * object_class;
	GnomeFontGlyphClass * glyph_class;

	object_class = (GtkObjectClass *) klass;
	glyph_class = (GnomeFontGlyphClass *) klass;

	parent_class = gtk_type_class (gnome_font_glyph_get_type ());

	object_class->destroy = gnome_font_glyph_ft_destroy;

	glyph_class->bpath = gfgft_bpath;
	glyph_class->metrics = gfgft_metrics;
	glyph_class->graymap = gfgft_graymap;
	glyph_class->gdkmap = gfgft_gdkmap;
}

static void
gnome_font_glyph_ft_init (GnomeFontGlyphFT * glyph)
{
	glyph->private = g_new0 (GnomeFontGlyphFTPrivate, 1);
}

static void
gnome_font_glyph_ft_destroy (GtkObject * object)
{
	GnomeFontGlyphFT * glyph;

	glyph = (GnomeFontGlyphFT *) object;

	/* fixme: */

	if (glyph->private) {
		g_free (glyph->private);
		glyph->private = NULL;
	}

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

static const ArtBpath *
gfgft_bpath (GnomeFontGlyph * glyph)
{
	GnomeFontGlyphFT * gft;
	GnomeFontGlyphFTPrivate * priv;
	TT_Outline ol;
	TT_Error result;
	gdouble scale;

	gft = (GnomeFontGlyphFT *) glyph;
	priv = gft->private;

	if (priv->err_outline) return NULL;

	if (priv->bpath == NULL) {
		result = TT_Get_Glyph_Outline (priv->tt_glyph, &ol);
		g_return_val_if_fail (result == TT_Err_Ok, NULL);

		if (priv->type == GFGFT_TYPE_FACE) {
			scale = 1000.0 / GF_F_FT_Units_Per_EM (glyph->vfont);
		} else {
			scale = 1.0 / 64.0;
		}

		priv->bpath = gf_freetype_bpath_from_outline (&ol, scale, scale);
		TT_Done_Outline (&ol);
		if (!priv->bpath) priv->err_outline = TRUE;
	}

	return priv->bpath;
}

static void
gfgft_metrics (GnomeFontGlyph * glyph, ArtDRect * bbox, ArtPoint * a)
{
	GnomeFontGlyphFT * ftglyph;
	GnomeFontGlyphFTPrivate * ftpriv;
	GnomeFontFT * ftfont;
	GnomeRFontFT * ftrfont;
	gdouble scale, scalex, scaley;
	ArtPoint p;

	ftglyph = (GnomeFontGlyphFT *) glyph;
	ftpriv = ftglyph->private;

	switch (ftpriv->type) {
	case GFGFT_TYPE_FACE:
		/* We are face, so we want to get metrics in 1000pt em */
		/* fixme: use ints, if unscaled */
		scale = 1000.0 / GF_F_FT_Units_Per_EM (glyph->vfont);
		bbox->x0 = ftpriv->tt_bigmetrics.bbox.xMin * scale;
		bbox->y0 = ftpriv->tt_bigmetrics.bbox.yMin * scale;
		bbox->x1 = ftpriv->tt_bigmetrics.bbox.xMax * scale;
		bbox->y1 = ftpriv->tt_bigmetrics.bbox.yMax * scale;
		/* We are currently hardcoded for horizontal LR layout */
		a->x = ftpriv->tt_bigmetrics.horiAdvance * scale;
		a->y = 0.0;
#if 0
		/* fixme: this is probably useless in given context */
		a->y = ftpriv->tt_bigmetrics.vertAdvance * scale;
#endif
		break;
	case GFGFT_TYPE_FONT:
		/* We are font, so we want to use base coordinates */
		/* Fixme: font CAN have nontrivial affine */
		ftfont = (GnomeFontFT *) ftpriv->vfont;
		scalex = 72.0 / 64.0 / ftfont->private->dresx;
		scaley = 72.0 / 64.0 / ftfont->private->dresy;
		bbox->x0 = ftpriv->tt_bigmetrics.bbox.xMin * scalex;
		bbox->y0 = ftpriv->tt_bigmetrics.bbox.yMin * scaley;
		bbox->x1 = ftpriv->tt_bigmetrics.bbox.xMax * scalex;
		bbox->y1 = ftpriv->tt_bigmetrics.bbox.yMax * scaley;
		a->x = ftpriv->tt_bigmetrics.horiAdvance * scalex;
		a->y = 0.0;
#if 0
		/* fixme: this is probably useless in given context */
		a->y = ftpriv->tt_bigmetrics.vertAdvance / 64.0 * 72.0 / ftfont->private->dresy;
#endif
		break;
	case GFGFT_TYPE_RFONT:
		/* fixme */
		ftrfont = (GnomeRFontFT *) ftpriv->vfont;
		/* This is currently CRAP */
		bbox->x0 = ftpriv->tt_bigmetrics.bbox.xMin / 64.0;
		bbox->y0 = ftpriv->tt_bigmetrics.bbox.yMin / 64.0;
		bbox->x1 = ftpriv->tt_bigmetrics.bbox.xMax / 64.0;
		bbox->y1 = ftpriv->tt_bigmetrics.bbox.yMax / 64.0;
#if 0
		a->x = ftpriv->tt_bigmetrics.horiAdvance / 64.0 / ftfont->private->xscale;
		/* fixme: this is probably useless in given context */
		a->y = ftpriv->tt_bigmetrics.vertAdvance / 64.0 / ftfont->private->yscale;
#else
		p.x = ftpriv->tt_bigmetrics.horiAdvance / 64.0;
		p.y = 0.0;
		/* fixme: rounding */
		art_affine_point (a, &p, ftrfont->private->drm);
#endif
		break;
	default:
		g_assert_not_reached ();
		break;
	}
}

static const GFGGrayMap *
gfgft_graymap (GnomeFontGlyph * glyph)
{
	GnomeFontGlyphFT * gft;
	GnomeFontGlyphFTPrivate * priv;
	GnomeRFontFT * rfont;
	GnomeRFontFTPrivate * rpriv;
	TT_Outline gol, ol;
	TT_BBox bb;
	TT_Error result;
	gint x0, y0, x1, y1;
	gint w, h;
	TT_Raster_Map rmap;

	gft = (GnomeFontGlyphFT *) glyph;
	priv = gft->private;

	if (priv->err_graymap) return NULL;
	if (priv->type != GFGFT_TYPE_RFONT) return NULL;

	rfont = (GnomeRFontFT *) glyph->vfont;
	rpriv = rfont->private;

	if (priv->graymap.pixels == NULL) {

		/* Get outline */

		result = TT_Get_Glyph_Outline (priv->tt_glyph, &gol);
		g_return_val_if_fail (result == TT_Err_Ok, NULL);
		TT_New_Outline (gol.n_points, gol.n_contours, &ol);
		TT_Copy_Outline (&gol, &ol);

		/* Transform outline with remainder matrix */

		TT_Transform_Outline (&ol, &rpriv->tt_m);

		/* Find new bbox */

		TT_Get_Outline_BBox (&ol, &bb);

		/* Get outline bbox in pixels */

		x0 = (bb.xMin & -64) >> 6;
		y0 = (bb.yMin & -64) >> 6;
		x1 = (bb.xMax & -64) >> 6;
		y1 = (bb.yMax & -64) >> 6;

		/* Blah blah - setup raster map */

		w = ((x1 - x0) + 4) & 0xfffffffc;
		h = (y1 - y0) + 1;

		priv->graymap.pixels = g_new0 (gchar, w * h);
		priv->graymap.width = w;
		priv->graymap.height = h;
		priv->graymap.rowstride = w;
		priv->graymap.x0 = x0;
		priv->graymap.y0 = y0;

		/* Blah blah - setup FreeType raster map */

		rmap.rows = h;
		rmap.cols = w;
		rmap.width = w;
		rmap.flow = TT_Flow_Up;
		rmap.bitmap = priv->graymap.pixels;
		rmap.size = w * h;

		/* Blah, blah - translate outline to bbox */

		TT_Translate_Outline (&ol, (-x0) * 64, (-y0) * 64);

		TT_Get_Outline_Pixmap (GFFT_ENGINE, &ol, &rmap);

		TT_Done_Outline (&ol);
	}

	return &priv->graymap;
}

static const GFGGdkMap *
gfgft_gdkmap (GnomeFontGlyph * glyph)
{
#if 0
	GnomeRFontFT * rfont;
	GnomeRFontFTPrivate * priv;
	const GFGGrayMap * gmap;

#if 0
	if (priv->err_graymap) return NULL;
#endif
	if (priv->type != GFGFT_TYPE_RFONT) return NULL;

	rfont = (GnomeRFontFT *) glyph->vfont;
	priv = rfont->private;

	gmap = gfgft_graymap (glyph);
	g_return_if_fail (gmap != NULL);

	priv->gdkmap.pixmap = gdk_pixmap_new (NULL, gmap->width, gmap->height,
		gdk_rgb_get_visual ()->depth);
	gc = gdk_gc_new (priv->gdkmap.pixmap);
	gdk_draw_gray_image (priv->gdkmap.pixmap, gc,
		0, 0, gmap->width, gmap->height,
		GDK_RGB_DITHER_NORMAL,
		gmap->pixels, gmap->rowstride);
	gdk_gc_unref (priv->gdkmap.pixmap);

	priv->gdkmap.mask = gdk_pixmap_new (NULL, gmap->width, gmap->height, 1);
	gc = gdk_gc_new (priv->gdkmap.pixmap);
	gdk_draw_gray_image (priv->gdkmap.pixmap, gc,
		0, 0, gmap->width, gmap->height,
		GDK_RGB_DITHER_NORMAL,
		gmap->pixels, gmap->rowstride);
	gdk_gc_unref (priv->gdkmap.pixmap);

	priv->gdkmap.x0 = gmap->x0;
	priv->gdkmap.y0 = gmap->y0;

	return &priv->gdkmap;
#else
	return NULL;
#endif
}

/* Constructor */

GnomeFontGlyph *
gnome_font_glyph_ft_new (GnomeVFont * vfont, TT_Face face, TT_Instance inst, gint code, GFGFTType type)
{
	GnomeFontGlyphFT * glyph;
	TT_UShort flags;
	TT_Error result;

	glyph = gtk_type_new (GNOME_TYPE_FONT_GLYPH_FT);

	/* fixme: */

	((GnomeFontGlyph *) glyph)->vfont = vfont;

	glyph->private->vfont = vfont;

	glyph->private->type = type;

	glyph->private->can_outline = TRUE;
	glyph->private->can_graymap = TRUE;

	result = TT_New_Glyph (face, &glyph->private->tt_glyph);
	g_return_val_if_fail (result == TT_Err_Ok, NULL);

	/* fixme: use font units for face glyphs */

	flags = 0;

	switch (type) {
	case GFGFT_TYPE_FACE:
		flags = 0;
		break;
	case GFGFT_TYPE_FONT:
	case GFGFT_TYPE_RFONT:
		flags = TTLOAD_SCALE_GLYPH | TTLOAD_HINT_GLYPH;
		break;
	default:
		g_assert_not_reached ();
		break;
	}

	result = TT_Load_Glyph (inst, glyph->private->tt_glyph, code, flags);
	g_return_val_if_fail (result == TT_Err_Ok, NULL);

	TT_Get_Glyph_Big_Metrics (glyph->private->tt_glyph, &glyph->private->tt_bigmetrics);

	return (GnomeFontGlyph *) glyph;
}

static GnomeFontFaceFT *
gfgft_get_face (GnomeFontGlyphFT * glyph)
{
	switch (glyph->private->type) {
	case GFGFT_TYPE_FACE:
		return (GnomeFontFaceFT *) glyph->glyph.vfont;
	case GFGFT_TYPE_FONT:
		return (GnomeFontFaceFT *) ((GnomeFont *) glyph->glyph.vfont)->face;
	case GFGFT_TYPE_RFONT:
		return (GnomeFontFaceFT *) ((GnomeRFont *) glyph->glyph.vfont)->font->face;
	default:
		g_assert_not_reached ();
	}

	return NULL;
}

