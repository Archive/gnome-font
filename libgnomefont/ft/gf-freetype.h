#ifndef GF_FREETYPE_H
#define GF_FREETYPE_H

/*
 * FreeType library plug
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <glib.h>
#include <freetype/freetype.h>
#include <libart_lgpl/art_bpath.h>

#ifndef GF_FREETYPE_C
	extern TT_Engine gnome_font_TT_Engine;
	extern gboolean gfft_initialized;
#else
	TT_Engine gnome_font_TT_Engine;
	gboolean gfft_initialized = FALSE;
#endif

#define GFFT (gfft_initialized | gf_freetype_init ())
#define GFFT_ENGINE gnome_font_TT_Engine

gboolean gf_freetype_init (void);

/*
 * Here we have to do unusual locale chemistry, to extract available relevant
 * information from multiplatform TrueType fonts. The idea is to request
 * GSList of PlatforID/SpecificID/LanguageID preferences for given locale,
 * and process it, until we find all required strings
 */
 
typedef struct _GFFT_Locale GFFT_Locale;

struct _GFFT_Locale {
	TT_UShort PlatformID;
	TT_UShort PlatformSpecificID;
	TT_UShort LanguageID;
};

const GSList * gf_freetype_locale_preference_list (const gchar * language);

/* Duplicates string in one of TrueType encodings to UTF-8 encoded string */

gchar * gf_freetype_strdup_utf8 (TT_String * src, TT_UShort length, GFFT_Locale * loc);

/* Converts FreeType glyph outline to ArtBpath, given scaling */

ArtBpath * gf_freetype_bpath_from_outline (TT_Outline * ol, gdouble xscale, gdouble yscale);

#endif
