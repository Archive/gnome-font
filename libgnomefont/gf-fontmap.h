#ifndef _GF_FONTMAP_H_
#define _GF_FONTMAP_H_

/*
 * Fontmap implementation
 *
 * Authors:
 *   Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

typedef struct _GFFontMap GFFontMap;
typedef struct _GFFileEntry GFFileEntry;
typedef struct _GFFontEntry GFFontEntry;
typedef struct _GFFamilyEntry GFFamilyEntry;
typedef struct _GFFontEntryTT GFFontEntryTT;
typedef struct _GFFontEntryT1 GFFontEntryT1;
typedef struct _GFFontEntryT1Alias GFFontEntryT1Alias;
typedef struct _GFFontEntryAlias GFFontEntryAlias;

#include <sys/types.h>
#include <glib.h>
#include <gnome-xml/tree.h>
#include "gnome-font-face.h"

typedef enum {
	GF_FONT_ENTRY_UNKNOWN,
	GF_FONT_ENTRY_TRUETYPE,
	GF_FONT_ENTRY_TYPE1, /* Just ordinary Type1 font */
	GF_FONT_ENTRY_TYPE1_ALIAS, /* Type1 font with foreign afm */
	GF_FONT_ENTRY_ALIAS /* Full alias */
} GFFontEntryType;

struct _GFFileEntry {
	gchar * name;
	size_t size;
	time_t mtime;
};

struct _GFFontMap {
	gint refcount;
	gint num_fonts;
	GFFileEntry user;
	GFFileEntry system;
	/* Name -> FontEntry */
	GHashTable * fontdict;
	/* Family name -> FamilyEntry */
	GHashTable * familydict;
	/* List of FontEntries, sorted A-Z */
	GSList * fonts;
	/* List of FamilyEntries, sorted A-Z */
	GSList * families;
	/* List of font names (pointing to entry->name) */
	GList * fontlist;
	/* List of family names (pointing to entry->name */
	GList * familylist;
};

struct _GFFontEntry {
	GFFontEntryType type;
	gint refcount;
	/* Our face */
	GnomeFontFace * face;
	/* Common fields */
	gchar * name;
	gchar * version;
	gchar * familyname;
	gchar * speciesname;
	gchar * psname;
	/* fixme: fixme: fixme: */
	gchar * weight;
};

struct _GFFamilyEntry {
	gint refcount;
	gchar * name;
	/* List of FontEntries */
	GSList * fonts;
};

struct _GFFontEntryT1 {
	GFFontEntry entry;
	GFFileEntry afm;
	GFFileEntry pfb;
	/* Some parsed afm latin metrics */
	GnomeFontWeight Weight;
	gdouble ItalicAngle; /* italic < 0 */
#if 0
	gboolean IsFixedPitch;
	ArtDRect FontBBox;
	gdouble UnderlinePosition;
	gdouble UnderlineThickness;
	gdouble CapHeight;
	gdouble XHeight;
	gdouble Ascender;
	gdouble Descender;
#endif
};

struct _GFFontEntryT1Alias {
	GFFontEntryT1 t1;
	gchar * alias;
};

struct _GFFontEntryAlias {
	GFFontEntry entry;
	GFFontEntry * ref;
};

struct _GFFontEntryTT {
	GFFontEntry entry;
	GFFileEntry ttf;
	/* fixme: */
	/* Some parsed afm latin metrics */
	GnomeFontWeight Weight;
	gdouble ItalicAngle; /* italic < 0 */
};

GFFontMap * gf_fontmap_get (void);
void gf_fontmap_release (GFFontMap *);

void gf_font_entry_ref (GFFontEntry * entry);
void gf_font_entry_unref (GFFontEntry * entry);

/*
 * Font lists
 *
 * You have to free all lists using provided functions, or your
 * fontmap will never be unrefed
 *
 */

GList * gnome_font_list (void);
void gnome_font_list_free (GList * fontlist);
GList * gnome_font_family_list (void);
void gnome_font_family_list_free (GList * familylist);

GnomeFontWeight gf_fontmap_lookup_weight (const gchar * weight);

END_GNOME_DECLS

#endif
