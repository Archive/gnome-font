#define GNOME_FONTGLYPH_GT1_C

/*
 * GnomeFontGlyphGT1
 *
 * Type1 glyph
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <libart_lgpl/art_misc.h>
#include <libart_lgpl/art_affine.h>
#include <libart_lgpl/art_vpath.h>
#include <libart_lgpl/art_bpath.h>
#include <libart_lgpl/art_vpath_bpath.h>
#include <libart_lgpl/art_svp.h>
#include <libart_lgpl/art_svp_vpath.h>
#include <libart_lgpl/art_svp_wind.h>
#include <libart_lgpl/art_gray_svp.h>
#include "gnome-font-glyph-gt1.h"
#include "gnome-font-glyph-gt1-private.h"
#include "gnome-font-face-gt1.h"
#include "gnome-font-face-gt1-private.h"
#include "gnome-font-gt1.h"
#include "gnome-font-gt1-private.h"
#include "gnome-rfont-gt1.h"
#include "gnome-rfont-gt1-private.h"

static void gnome_font_glyph_gt1_class_init (GnomeFontGlyphGT1Class * klass);
static void gnome_font_glyph_gt1_init (GnomeFontGlyphGT1 * glyph);

static void gnome_font_glyph_gt1_destroy (GtkObject * object);

static const ArtBpath * gfggt1_bpath (GnomeFontGlyph * glyph);
static void gfggt1_metrics (GnomeFontGlyph * glyph, ArtDRect * bbox, ArtPoint * a);
static const GFGGrayMap * gfggt1_graymap (GnomeFontGlyph * glyph);
static const GFGGdkMap * gfggt1_gdkmap (GnomeFontGlyph * glyph);

static Gt1LoadedFont * get_loadedfont (GnomeFontGlyph * glyph);
static gdouble * get_transform (GnomeFontGlyph * glyph);

static GnomeFontGlyphClass * parent_class = NULL;

GtkType
gnome_font_glyph_gt1_get_type (void)
{
	static GtkType glyph_type = 0;
	if (!glyph_type) {
		GtkTypeInfo glyph_info = {
			"GnomeFontGlyphGT1",
			sizeof (GnomeFontGlyphGT1),
			sizeof (GnomeFontGlyphGT1Class),
			(GtkClassInitFunc) gnome_font_glyph_gt1_class_init,
			(GtkObjectInitFunc) gnome_font_glyph_gt1_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};
		glyph_type = gtk_type_unique (gnome_font_glyph_get_type (), &glyph_info);
	}
	return glyph_type;
}

static void
gnome_font_glyph_gt1_class_init (GnomeFontGlyphGT1Class * klass)
{
	GtkObjectClass * object_class;
	GnomeFontGlyphClass * glyph_class;

	object_class = (GtkObjectClass *) klass;
	glyph_class = (GnomeFontGlyphClass *) klass;

	parent_class = gtk_type_class (gnome_font_glyph_get_type ());

	object_class->destroy = gnome_font_glyph_gt1_destroy;

	glyph_class->bpath = gfggt1_bpath;
	glyph_class->metrics = gfggt1_metrics;
	glyph_class->graymap = gfggt1_graymap;
	glyph_class->gdkmap = gfggt1_gdkmap;
}

static void
gnome_font_glyph_gt1_init (GnomeFontGlyphGT1 * glyph)
{
	glyph->private = g_new0 (GnomeFontGlyphGT1Private, 1);
}

static void
gnome_font_glyph_gt1_destroy (GtkObject * object)
{
	GnomeFontGlyphGT1 * glyph;

	glyph = (GnomeFontGlyphGT1 *) object;

	/* fixme: */

	if (glyph->private) {
		GnomeFontGlyphGT1Private * priv;

		priv = glyph->private;

		if (priv->bpath) {
			art_free (priv->bpath);
		}

		g_free (glyph->private);
		glyph->private = NULL;
	}

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

static const ArtBpath *
gfggt1_bpath (GnomeFontGlyph * glyph)
{
	GnomeFontGlyphGT1 * ggt1;
	GnomeFontGlyphGT1Private * priv;
	Gt1LoadedFont * lf;
	Gt1GlyphOutline * ol;
	gdouble * affine;

	ggt1 = (GnomeFontGlyphGT1 *) glyph;
	priv = ggt1->private;

	if (priv->bpath) return priv->bpath;

	if (priv->err_outline) return NULL;

	lf = get_loadedfont (glyph);
	g_assert (lf != NULL);

	ol = gt1_glyph_outline_lookup (lf, priv->code);

	if (ol == NULL) {
		priv->err_outline = TRUE;
		return NULL;
	}

	affine = get_transform (glyph);
	g_assert (affine != NULL);

	priv->bpath = art_bpath_affine_transform (ol->bpath, affine);

	/* fixme: how to free outline? */

	return priv->bpath;
}

static void
gfggt1_metrics (GnomeFontGlyph * glyph, ArtDRect * bbox, ArtPoint * a)
{
	GnomeFontGlyphGT1 * ggt1;
	GnomeFontGlyphGT1Private * priv;
	Gt1LoadedFont * lf;
	Gt1GlyphOutline * ol;
	gdouble * affine;
	ArtBpath * bpath;
	ArtVpath * vpath;
	ArtPoint p;

	ggt1 = (GnomeFontGlyphGT1 *) glyph;
	priv = ggt1->private;

	if (priv->err_metrics) return;

	if (!priv->has_metrics) {
		lf = get_loadedfont (glyph);
		g_assert (lf != NULL);

		ol = gt1_glyph_outline_lookup (lf, priv->code);

		if (ol == NULL) {
			priv->err_metrics = TRUE;
			return;
		}

		switch (priv->type) {
		case GFGGT1_TYPE_FACE:
			priv->bbox = ol->bbox;
			priv->advance.x = ol->wx;
			priv->advance.y = 0.0;
			break;
		case GFGGT1_TYPE_FONT:
		case GFGGT1_TYPE_RFONT:
			affine = get_transform (glyph);
			g_assert (affine != NULL);
			bpath = art_bpath_affine_transform (ol->bpath, affine);
			vpath = art_bez_path_to_vec (bpath, 0.25);
			art_free (bpath);
			art_vpath_bbox_drect (vpath, &priv->bbox);
			art_free (vpath);
			p.x = ol->wx;
			p.y = 0.0;
			art_affine_point (&priv->advance, &p, affine);
			break;
		default:
			g_assert_not_reached ();
			break;
		}

		priv->has_metrics = TRUE;
	}

	*bbox = priv->bbox;
	*a = priv->advance;
}

static const GFGGrayMap *
gfggt1_graymap (GnomeFontGlyph * glyph)
{
	GnomeFontGlyphGT1 * ggt1;
	GnomeFontGlyphGT1Private * priv;
	ArtIRect ibox;
	gint w, h;

	ggt1 = (GnomeFontGlyphGT1 *) glyph;
	priv = ggt1->private;

	if (priv->err_graymap) return NULL;
	if (priv->type != GFGGT1_TYPE_RFONT) return NULL;

	if (priv->graymap.pixels == NULL) {
		ArtDRect dr;
		ArtPoint ad;
		ArtVpath * vpath, * vpath1;
		ArtSVP * svp, * svp1;

		/* Get outline */

		if (!priv->bpath) gfggt1_bpath (glyph);
		g_return_val_if_fail (priv->bpath != NULL, NULL);

		if (!priv->has_metrics) gfggt1_metrics (glyph, &dr, &ad);
		g_return_val_if_fail (priv->bpath != NULL, NULL);

		art_drect_to_irect (&ibox, &priv->bbox);
		w = ibox.x1 - ibox.x0;
		h = ibox.y1 - ibox.y0;

		priv->graymap.pixels = g_new0 (gchar, w * h);
		priv->graymap.width = w;
		priv->graymap.height = h;
		priv->graymap.rowstride = w;
		priv->graymap.x0 = ibox.x0;
		priv->graymap.y0 = ibox.y0;

		vpath = art_bez_path_to_vec (priv->bpath, 0.25);
		vpath1 = art_vpath_perturb (vpath);
		art_free (vpath);
		svp = art_svp_from_vpath (vpath1);
		art_free (vpath1);
		svp1 = art_svp_uncross (svp);
		art_svp_free (svp);
		svp = art_svp_rewind_uncrossed (svp1, ART_WIND_RULE_ODDEVEN);
		art_svp_free (svp1);
		art_gray_svp_aa (svp, ibox.x0, ibox.y0, ibox.x1, ibox.y1,
			priv->graymap.pixels, w);
		art_svp_free (svp);
	}

	return &priv->graymap;
}

static const GFGGdkMap *
gfggt1_gdkmap (GnomeFontGlyph * glyph)
{
#if 0
	GnomeRFontGT1 * rfont;
	GnomeRFontGT1Private * priv;
	const GFGGrayMap * gmap;

#if 0
	if (priv->err_graymap) return NULL;
#endif
	if (priv->type != GFGGT1_TYPE_RFONT) return NULL;

	rfont = (GnomeRFontGT1 *) glyph->vfont;
	priv = rfont->private;

	gmap = gfggt1_graymap (glyph);
	g_return_if_fail (gmap != NULL);

	priv->gdkmap.pixmap = gdk_pixmap_new (NULL, gmap->width, gmap->height,
		gdk_rgb_get_visual ()->depth);
	gc = gdk_gc_new (priv->gdkmap.pixmap);
	gdk_draw_gray_image (priv->gdkmap.pixmap, gc,
		0, 0, gmap->width, gmap->height,
		GDK_RGB_DITHER_NORMAL,
		gmap->pixels, gmap->rowstride);
	gdk_gc_unref (priv->gdkmap.pixmap);

	priv->gdkmap.mask = gdk_pixmap_new (NULL, gmap->width, gmap->height, 1);
	gc = gdk_gc_new (priv->gdkmap.pixmap);
	gdk_draw_gray_image (priv->gdkmap.pixmap, gc,
		0, 0, gmap->width, gmap->height,
		GDK_RGB_DITHER_NORMAL,
		gmap->pixels, gmap->rowstride);
	gdk_gc_unref (priv->gdkmap.pixmap);

	priv->gdkmap.x0 = gmap->x0;
	priv->gdkmap.y0 = gmap->y0;

	return &priv->gdkmap;
#else
	return NULL;
#endif
}

/* Constructor */

GnomeFontGlyph *
gnome_font_glyph_gt1_new (GnomeVFont * vfont, Gt1LoadedFont * gt1_font, gint code, GFGGT1Type type)
{
	GnomeFontGlyphGT1 * glyph;

	glyph = gtk_type_new (GNOME_TYPE_FONT_GLYPH_GT1);

	/* fixme: */

	((GnomeFontGlyph *) glyph)->vfont = vfont;

	glyph->private->vfont = vfont;
	glyph->private->type = type;
	glyph->private->code = code;

	glyph->private->can_outline = TRUE;
	glyph->private->can_graymap = TRUE;
#if 0
	result = TT_New_Glyph (face, &glyph->private->tt_glyph);
	g_return_val_if_fail (result == TT_Err_Ok, NULL);

	/* fixme: use font units for face glyphs */

	flags = 0;

	switch (type) {
	case GFGGT1_TYPE_FACE:
		flags = 0;
		break;
	case GFGGT1_TYPE_FONT:
	case GFGGT1_TYPE_RFONT:
		flags = TTLOAD_SCALE_GLYPH | TTLOAD_HINT_GLYPH;
		break;
	default:
		g_assert_not_reached ();
		break;
	}

	result = TT_Load_Glyph (inst, glyph->private->tt_glyph, code, flags);
	g_return_val_if_fail (result == TT_Err_Ok, NULL);

	TT_Get_Glyph_Big_Metrics (glyph->private->tt_glyph, &glyph->private->tt_bigmetrics);
#endif
	return (GnomeFontGlyph *) glyph;
}

static Gt1LoadedFont *
get_loadedfont (GnomeFontGlyph * glyph)
{
	switch (((GnomeFontGlyphGT1 *)glyph)->private->type) {
		case GFGGT1_TYPE_FACE:
			return ((GnomeFontFaceGT1 *) glyph->vfont)->private->gt1_font;
			break;
		case GFGGT1_TYPE_FONT:
			return ((GnomeFontGT1 *) glyph->vfont)->private->gt1_font;
			break;
		case GFGGT1_TYPE_RFONT:
			return ((GnomeRFontGT1 *) glyph->vfont)->private->gt1_font;
			break;
		default:
			g_assert_not_reached ();
			break;
	}

	return NULL;
}

static gdouble *
get_transform (GnomeFontGlyph * glyph)
{
	static gdouble id[6] = {1.0, 0.0, 0.0, 1.0, 0.0, 0.0};

	switch (((GnomeFontGlyphGT1 *) glyph)->private->type) {
		case GFGGT1_TYPE_FACE:
			return id;
			break;
		case GFGGT1_TYPE_FONT:
			return ((GnomeFontGT1 *) glyph->vfont)->private->transform;
			break;
		case GFGGT1_TYPE_RFONT:
			return ((GnomeRFontGT1 *) glyph->vfont)->private->transform;
			break;
		default:
			g_assert_not_reached ();
			break;
	}

	return NULL;
}


