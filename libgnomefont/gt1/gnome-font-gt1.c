#define GNOME_FONT_GT1_C

/*
 * GnomeFontGT1
 *
 * Type1 font
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <math.h>
#include <libart_lgpl/art_affine.h>
#include "gnome-font-face-gt1.h"
#include "gnome-font-face-gt1-private.h"
#include "gnome-font-gt1.h"
#include "gnome-font-gt1-private.h"
#include "gnome-rfont-gt1.h"
#include "gnome-rfont-gt1-private.h"
#include "gnome-font-glyph-gt1.h"
#include "gnome-font-glyph-gt1-private.h"

static void gnome_font_gt1_class_init (GnomeFontGT1Class * klass);
static void gnome_font_gt1_init (GnomeFontGT1 * group);

static void gnome_font_gt1_destroy (GtkObject * object);

static GnomeFontGlyph * gnome_font_gt1_get_glyph (GnomeVFont * vfont, gint code);

static GnomeRFont * gnome_font_gt1_get_rfont (GnomeFont * font, gdouble affine[]);

static GnomeFontClass * parent_class = NULL;

GtkType
gnome_font_gt1_get_type (void)
{
	static GtkType group_type = 0;
	if (!group_type) {
		GtkTypeInfo group_info = {
			"GnomeFontGT1",
			sizeof (GnomeFontGT1),
			sizeof (GnomeFontGT1Class),
			(GtkClassInitFunc) gnome_font_gt1_class_init,
			(GtkObjectInitFunc) gnome_font_gt1_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};
		group_type = gtk_type_unique (gnome_font_get_type (), &group_info);
	}
	return group_type;
}

static void
gnome_font_gt1_class_init (GnomeFontGT1Class * klass)
{
	GtkObjectClass * object_class;
	GnomeVFontClass * vfont_class;
	GnomeFontClass * font_class;

	object_class = (GtkObjectClass *) klass;
	vfont_class = (GnomeVFontClass *) klass;
	font_class = (GnomeFontClass *) klass;

	parent_class = gtk_type_class (gnome_font_get_type ());

	object_class->destroy = gnome_font_gt1_destroy;

	vfont_class->get_glyph = gnome_font_gt1_get_glyph;

	font_class->get_rfont = gnome_font_gt1_get_rfont;
}

static void
gnome_font_gt1_init (GnomeFontGT1 * font)
{
	font->private = g_new0 (GnomeFontGT1Private, 1);
}

static void
gnome_font_gt1_destroy (GtkObject * object)
{
	GnomeFontGT1 * font;

	font = (GnomeFontGT1 *) object;

	if (font->private) {
		GnomeFontGT1Private * priv;

		priv = font->private;

		while (priv->rfonts) {
			gtk_object_destroy ((GtkObject *) priv->rfonts->data);
			priv->rfonts = g_slist_remove_link (priv->rfonts, priv->rfonts);
		}

		g_free (font->private);
		font->private = NULL;
	}

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

/* fixme: */

static GnomeFontGlyph *
gnome_font_gt1_get_glyph (GnomeVFont * vfont, gint code)
{
	GnomeFontGT1 * gt1font;
	GnomeFontFaceGT1 * gt1face;
	GnomeFontGlyph * glyph;

	gt1face = (GnomeFontFaceGT1 *) ((GnomeFont *) vfont)->face;
	gt1font = (GnomeFontGT1 *) vfont;

	glyph = gnome_font_glyph_gt1_new (vfont,
		gt1face->private->gt1_font,
		code,
		GFGGT1_TYPE_FONT);

	return glyph;
}

/* fixme: */

static GnomeRFont *
gnome_font_gt1_get_rfont (GnomeFont * font, gdouble affine[])
{
	GnomeFontGT1 * gt1font;
	GnomeFontGT1Private * priv;
	GnomeRFont * rfont;

	gt1font = (GnomeFontGT1 *) font;
	priv = gt1font->private;

	rfont = gnome_rfont_gt1_new (font, affine);
	g_return_val_if_fail (rfont != NULL, NULL);

	priv->rfonts = g_slist_prepend (priv->rfonts, rfont);

	return rfont;
}

/* Methods */

GnomeFont *
gnome_font_gt1_new (GnomeFontFace * face, gdouble size, gdouble transform[])
{
	GnomeFontFaceGT1 * gt1face;
	GnomeFontFaceGT1Private * priv;
	GnomeFont * font;
	GnomeFontGT1 * gt1font;

	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE_GT1 (face), NULL);

	gt1face = (GnomeFontFaceGT1 *) face;
	priv = gt1face->private;

	font = gtk_type_new (GNOME_TYPE_FONT_GT1);
	gt1font = (GnomeFontGT1 *) font;

	font->face = face;
	font->size = size;
	memcpy (font->affine, transform, 4 * sizeof (double));
	font->affine[4] = font->affine[5] = 0.0;

	/* fixme: add rounding remainder to transformation matrix */

	gt1font->private->gt1_font = gt1face->private->gt1_font;

	art_affine_scale (gt1font->private->transform, size / 1000.0, size / 1000.0);

	/* Now we have font transformation matrixes set up :) */

	/* fixme: error checking */

	return font;
}

