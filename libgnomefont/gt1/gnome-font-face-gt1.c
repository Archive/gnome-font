#define GNOME_FONT_FACE_GT1_C

/*
 * GnomeFontFaceGT1
 *
 * Standard GT1 font file handler
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <string.h>
#include <libgnomefont/gnome-font-face.h>
#include <libgnomefont/gnome-font-face-private.h>
#include "gf-gt1.h"
#include "gnome-font-face-gt1.h"
#include "gnome-font-face-gt1-private.h"
#include "gnome-font-gt1.h"
#include "gnome-font-gt1-private.h"
#include "gnome-font-glyph-gt1.h"
#include "gnome-font-glyph-gt1-private.h"

static void gft_class_init (GnomeFontFaceGT1Class * klass);
static void gft_init (GnomeFontFaceGT1 * face);

static void gft_destroy (GtkObject * object);

/* GnomeVFont methods */

static GnomeFontGlyph * gft_get_glyph (GnomeVFont * vfont, gint code);

/* GnomeFontFace methods */

static GnomeFontFaceDesc * gft_description (GnomeFontFace * face, const gchar * language);
static GnomeFont * gft_get_font (GnomeFontFace * face, gdouble size, gdouble transform[]);
static gboolean gft_set_encoding (GnomeFontFace * face, gint id, const gchar * encoding);
static gint gft_lookup (GnomeFontFace * face, gint encoding, gint code);

static GHashTable * pathdict = NULL;
static GnomeFontFaceClass * parent_class = NULL;

GtkType
gnome_font_face_gt1_get_type (void)
{
	static GtkType face_type = 0;
	if (!face_type) {
		GtkTypeInfo face_info = {
			"GnomeFontFaceGT1",
			sizeof (GnomeFontFaceGT1),
			sizeof (GnomeFontFaceGT1Class),
			(GtkClassInitFunc) gft_class_init,
			(GtkObjectInitFunc) gft_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};
		face_type = gtk_type_unique (gnome_font_face_get_type (), &face_info);
	}
	return face_type;
}

static void
gft_class_init (GnomeFontFaceGT1Class * klass)
{
	GtkObjectClass * object_class;
	GnomeVFontClass * vfont_class;
	GnomeFontFaceClass * face_class;

	object_class = (GtkObjectClass *) klass;
	vfont_class = (GnomeVFontClass *) klass;
	face_class = (GnomeFontFaceClass *) klass;

	parent_class = gtk_type_class (gtk_object_get_type ());

	object_class->destroy = gft_destroy;

	vfont_class->get_glyph = gft_get_glyph;

	face_class->description = gft_description;
	face_class->get_font = gft_get_font;
	face_class->set_encoding = gft_set_encoding;
	face_class->lookup = gft_lookup;
}

static void
gft_init (GnomeFontFaceGT1 * face)
{
	face->private = g_new0 (GnomeFontFaceGT1Private, 1);
}

static void
gft_destroy (GtkObject * object)
{
	GnomeFontFaceGT1 * face;

	face = (GnomeFontFaceGT1 *) object;

	if (face->private) {
		GnomeFontFaceGT1Private * priv;

		priv = face->private;

		if (face->private->pfb_path) {
			g_free (priv->pfb_path);
		}

		if (face->private->afm_path) {
			g_free (priv->afm_path);
		}

		/* Destroy fonts */

		while (priv->fonts) {
			gtk_object_destroy ((GtkObject *) priv->fonts->data);
			priv->fonts = g_slist_remove_link (priv->fonts, priv->fonts);
		}

		if (priv->gt1_font) gt1_unload_font (priv->gt1_font);

		g_free (face->private);
		face->private = NULL;
	}

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

/* GnomeVFont methods */

GnomeFontGlyph *
gft_get_glyph (GnomeVFont * vfont, gint code)
{
	GnomeFontFaceGT1 * ftface;
	GnomeFontFaceGT1Private * priv;
	GnomeFontGlyph * glyph;

	ftface = (GnomeFontFaceGT1 *) vfont;
	priv = ftface->private;

	glyph = gnome_font_glyph_gt1_new (vfont,
		priv->gt1_font,
		code,
		GFGGT1_TYPE_FACE);

	return glyph;
}

static GnomeFontFaceDesc *
gft_description (GnomeFontFace * face, const gchar * language)
{
	g_warning ("GnomeFontFaceGT1 does not implement ::description() method");

	return NULL;
}

static GnomeFont *
gft_get_font (GnomeFontFace * face, gdouble size, gdouble transform[])
{
	GnomeFontFaceGT1 * ftf;
	GnomeFontFaceGT1Private * priv;
	GnomeFont * font;

	ftf = (GnomeFontFaceGT1 *) face;
	priv = ftf->private;

	/* fixme: implement caching */

	font = gnome_font_gt1_new (face, size, transform);
	g_return_val_if_fail (font != NULL, NULL);

	priv->fonts = g_slist_prepend (priv->fonts, font);

	return font;
}

/* NB! HIGHLY EXPERIMENTAL */

static gboolean
gft_set_encoding (GnomeFontFace * face, gint id, const gchar * encoding)
{
	g_warning ("GnomeFontFaceGT1 does not implement ::set_encoding() method");

	return TRUE;
}

/* fixme: API test */

static gint
gft_lookup (GnomeFontFace * face, gint encoding, gint code)
{
	GnomeFontFaceGT1 * ftface;
	GnomeFontFaceGT1Private * priv;

	ftface = (GnomeFontFaceGT1 *) face;
	priv = ftface->private;

	return code;
}

/* Private & temporary methods */

GnomeFontFace *
gnome_font_face_gt1_new_from_file (const gchar * pfb_path, const gchar * afm_path)
{
	GnomeFontFaceGT1 * face;
	GnomeFontFaceGT1Private * priv;
	Gt1LoadedFont * gt1_font;

	g_return_val_if_fail (pfb_path != NULL, NULL);

	/* Test, if given path is already loaded */

	if (pathdict == NULL) {
		pathdict = g_hash_table_new (g_str_hash, g_str_equal);
	}

	face = g_hash_table_lookup (pathdict, pfb_path);

	if (face != NULL) {
		g_assert (GNOME_IS_FONT_FACE_GT1 (face));
		gnome_font_face_ref ((GnomeFontFace *) face);
		return (GnomeFontFace *) face;
	}

	gt1_font = gt1_load_font (pfb_path);

	if (!gt1_font) {
		g_warning ("Cannot open typeface file %s", pfb_path);
		return NULL;
	}

	/* Create Gnome face object */

	face = gtk_type_new (GNOME_TYPE_FONT_FACE_GT1);

	priv = face->private;

	priv->gt1_font = gt1_font;
	priv->pfb_path = g_strdup (pfb_path);

	if (afm_path) {
		priv->afm_path = g_strdup (afm_path);
	}

	g_hash_table_insert (pathdict, priv->pfb_path, face);

	/* fixme: register loaded face */
	/* fixme: handle collections */
	/* fixme: close face on destroy */
	/* fixme: kill glyphs on destroy */

	return (GnomeFontFace *) face;
}

