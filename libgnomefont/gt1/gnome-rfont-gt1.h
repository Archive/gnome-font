#ifndef GNOME_RFONT_GT1_H
#define GNOME_RFONT_GT1_H

/*
 * GnomeRFontGT1
 *
 * Type1 rasterized font implementation
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <libgnomefont/gnome-rfont.h>

#define GNOME_TYPE_RFONT_GT1		(gnome_rfont_gt1_get_type ())
#define GNOME_RFONT_GT1(obj)		(GTK_CHECK_CAST ((obj), GNOME_TYPE_RFONT_GT1, GnomeRFontGT1))
#define GNOME_RFONT_GT1_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_RFONT_GT1, GnomeRFontGT1Class))
#define GNOME_IS_RFONT_GT1(obj)		(GTK_CHECK_TYPE ((obj), GNOME_TYPE_RFONT_GT1))
#define GNOME_IS_RFONT_GT1_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_RFONT_GT1))

typedef struct _GnomeRFontGT1		GnomeRFontGT1;
typedef struct _GnomeRFontGT1Class	GnomeRFontGT1Class;
typedef struct _GnomeRFontGT1Private	GnomeRFontGT1Private;

struct _GnomeRFontGT1 {
	GnomeRFont rfont;
	GnomeRFontGT1Private * private;
};

struct _GnomeRFontGT1Class {
	GnomeRFontClass parent_class;
};

GtkType gnome_rfont_gt1_get_type (void);

#endif
