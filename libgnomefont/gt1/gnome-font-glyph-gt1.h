#ifndef GNOME_FONT_GLYPH_GT1_H
#define GNOME_FONT_GLYPH_GT1_H

/*
 * GnomeFontGlyphGT1
 *
 * Type1 glyph
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <libgnomefont/gnome-font-glyph.h>

/* FontGlyphGT1 */

#define GNOME_TYPE_FONT_GLYPH_GT1		(gnome_font_glyph_gt1_get_type ())
#define GNOME_FONT_GLYPH_GT1(obj)		(GTK_CHECK_CAST ((obj), GNOME_TYPE_FONT_GLYPH_GT1, GnomeFontGlyphGT1))
#define GNOME_FONT_GLYPH_GT1_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_FONT_GLYPH_GT1, GnomeFontGlyphGT1Class))
#define GNOME_IS_FONT_GLYPH_GT1(obj)		(GTK_CHECK_TYPE ((obj), GNOME_TYPE_FONT_GLYPH_GT1))
#define GNOME_IS_FONT_GLYPH_GT1_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_FONT_GLYPH_GT1))

typedef struct _GnomeFontGlyphGT1	GnomeFontGlyphGT1;
typedef struct _GnomeFontGlyphGT1Class	GnomeFontGlyphGT1Class;
typedef struct _GnomeFontGlyphGT1Private	GnomeFontGlyphGT1Private;

struct _GnomeFontGlyphGT1 {
	GnomeFontGlyph glyph;
	GnomeFontGlyphGT1Private * private;
};

struct _GnomeFontGlyphGT1Class {
	GnomeFontGlyphClass parent_class;
};

GtkType gnome_font_glyph_gt1_get_type (void);

#endif
