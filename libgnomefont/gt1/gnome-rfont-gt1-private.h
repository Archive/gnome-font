#ifndef GNOME_RFONT_GT1_PRIVATE_H
#define GNOME_RFONT_GT1_PRIVATE_H

#include <freetype/freetype.h>
#include <libgnomefont/gnome-font.h>

struct _GnomeRFontGT1Private {

	Gt1LoadedFont * gt1_font;

	/*
	 * PostScript to device transform
	 */

	gdouble transform[6];
};

GnomeRFont * gnome_rfont_gt1_new (GnomeFont * font, gdouble affine[]);

#endif
