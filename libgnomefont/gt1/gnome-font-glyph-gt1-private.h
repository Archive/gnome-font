#ifndef GNOME_FONT_GLYPH_GT1_PRIVATE_H
#define GNOME_FONT_GLYPH_GT1_PRIVATE_H

#include <libgnomefont/gnome-vfont.h>
#include <libgnomefont/gnome-font-glyph.h>

#include "gt1-parset1.h"

typedef enum {
	GFGGT1_TYPE_NONE,
	GFGGT1_TYPE_FACE,
	GFGGT1_TYPE_FONT,
	GFGGT1_TYPE_RFONT
} GFGGT1Type;

struct _GnomeFontGlyphGT1Private {
	GnomeVFont * vfont;
	GFGGT1Type type;
	gint code;
	guint has_metrics : 1;
	guint err_metrics : 1;
	guint can_outline : 1;
	guint err_outline : 1;
	guint can_graymap : 1;
	guint err_graymap : 1;
	ArtDRect bbox;
	ArtPoint advance;
	ArtBpath * bpath;

	GFGGrayMap graymap;
	GFGGdkMap gdkmap;
};

GnomeFontGlyph * gnome_font_glyph_gt1_new (GnomeVFont * vfont, Gt1LoadedFont * gt1_font, gint code, GFGGT1Type type);

#endif
