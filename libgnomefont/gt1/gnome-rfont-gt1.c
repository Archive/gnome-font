#define GNOME_RFONT_GT1_C

/*
 * GnomeRFontGT1
 *
 * Type1 rasterized font implementation
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <math.h>
#include <libart_lgpl/art_affine.h>
#include "gnome-font-face-gt1.h"
#include "gnome-font-face-gt1-private.h"
#include "gnome-font-gt1.h"
#include "gnome-font-gt1-private.h"
#include "gnome-rfont-gt1.h"
#include "gnome-rfont-gt1-private.h"
#include "gnome-font-glyph-gt1.h"
#include "gnome-font-glyph-gt1-private.h"

static void gnome_rfont_gt1_class_init (GnomeRFontGT1Class * klass);
static void gnome_rfont_gt1_init (GnomeRFontGT1 * group);

static void gnome_rfont_gt1_destroy (GtkObject * object);

static GnomeFontGlyph * gnome_rfont_gt1_get_glyph (GnomeVFont * vfont, gint code);

static GnomeRFontClass * parent_class = NULL;

GtkType
gnome_rfont_gt1_get_type (void)
{
	static GtkType group_type = 0;
	if (!group_type) {
		GtkTypeInfo group_info = {
			"GnomeRFontGT1",
			sizeof (GnomeRFontGT1),
			sizeof (GnomeRFontGT1Class),
			(GtkClassInitFunc) gnome_rfont_gt1_class_init,
			(GtkObjectInitFunc) gnome_rfont_gt1_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};
		group_type = gtk_type_unique (gnome_rfont_get_type (), &group_info);
	}
	return group_type;
}

static void
gnome_rfont_gt1_class_init (GnomeRFontGT1Class * klass)
{
	GtkObjectClass * object_class;
	GnomeVFontClass * vfont_class;

	object_class = (GtkObjectClass *) klass;
	vfont_class = (GnomeVFontClass *) klass;

	parent_class = gtk_type_class (gnome_rfont_get_type ());

	object_class->destroy = gnome_rfont_gt1_destroy;

	vfont_class->get_glyph = gnome_rfont_gt1_get_glyph;
}

static void
gnome_rfont_gt1_init (GnomeRFontGT1 * font)
{
	font->private = g_new0 (GnomeRFontGT1Private, 1);
}

static void
gnome_rfont_gt1_destroy (GtkObject * object)
{
	GnomeRFontGT1 * font;

	font = (GnomeRFontGT1 *) object;

	if (font->private) {
		GnomeRFontGT1Private * priv;

		priv = font->private;

		g_free (priv);
		font->private = NULL;
	}

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

/* fixme: */

static GnomeFontGlyph *
gnome_rfont_gt1_get_glyph (GnomeVFont * vfont, gint code)
{
	GnomeRFontGT1 * rfontgt1;
	GnomeRFontGT1Private * priv;
	GnomeFontFaceGT1 * gt1face;
	GnomeFontGlyph * glyph;

	/* But we NEED dictionary still :( */
	/* And here we need grid-fitting too */

	gt1face = (GnomeFontFaceGT1 *) ((GnomeRFont *) vfont)->font->face;
	rfontgt1 = (GnomeRFontGT1 *) vfont;
	priv = rfontgt1->private;

	glyph = gnome_font_glyph_gt1_new (vfont,
		gt1face->private->gt1_font,
		code,
		GFGGT1_TYPE_RFONT);

	return glyph;
}

/* Methods */

GnomeRFont *
gnome_rfont_gt1_new (GnomeFont * font, gdouble affine[])
{
	GnomeRFont * rfont;
	GnomeRFontGT1 * rfontgt1;
	GnomeRFontGT1Private * priv;
	gdouble a[6];

	g_return_val_if_fail (font != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_GT1 (font), NULL);

	rfontgt1 = gtk_type_new (GNOME_TYPE_RFONT_GT1);
	rfont = (GnomeRFont *) rfontgt1;
	priv = rfontgt1->private;

	/* fixme: ref font */

	/* Fill device values in GnomeRFont struct */

	rfont->font = font;
	memcpy (rfont->affine, affine, 4 * sizeof (gdouble));
	rfont->affine[4] = rfont->affine[5] = 0.0;

	rfontgt1->private->gt1_font = ((GnomeFontGT1 *) font)->private->gt1_font;

	a[0] = affine[0];
	a[1] = affine[1];
	a[2] = affine[2];
	a[3] = affine[3];
	a[4] = a[5] = 0.0;

	art_affine_multiply (priv->transform,
		((GnomeFontGT1 *) font)->private->transform, a);

	return rfont;
}


