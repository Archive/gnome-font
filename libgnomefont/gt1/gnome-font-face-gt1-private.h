#ifndef GNOME_FONT_FACE_GT1_PRIVATE_H
#define GNOME_FONT_FACE_GT1_PRIVATE_H

#include <libgnomefont/gnome-font-face.h>

#include "gt1-parset1.h"

struct _GnomeFontFaceGT1Private {
	/* Font filename */

	gchar * pfb_path;

	/* We should probably use dictionary size->afm here */

	gchar * afm_path;

	Gt1LoadedFont * gt1_font;

	GSList * fonts;
};

GnomeFontFace * gnome_font_face_gt1_new_from_file (const gchar * pfb_path, const gchar * afm_path);

#endif
