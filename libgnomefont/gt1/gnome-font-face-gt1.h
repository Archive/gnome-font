#ifndef GNOME_FONT_FACE_GT1_H
#define GNOME_FONT_FACE_GT1_H

/*
 * GnomeFontFaceGT1
 *
 * Standard Type1 file handler
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <libgnomefont/gnome-font-face.h>

/* FontFaceGT1 */

#define GNOME_TYPE_FONT_FACE_GT1		(gnome_font_face_gt1_get_type ())
#define GNOME_FONT_FACE_GT1(obj)		(GTK_CHECK_CAST ((obj), GNOME_TYPE_FONT_FACE_GT1, GnomeFontFaceGT1))
#define GNOME_FONT_FACE_GT1_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_FONT_FACE_GT1, GnomeFontFaceGT1Class))
#define GNOME_IS_FONT_FACE_GT1(obj)		(GTK_CHECK_TYPE ((obj), GNOME_TYPE_FONT_FACE_GT1))
#define GNOME_IS_FONT_FACE_GT1_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_FONT_FACE_GT1))

typedef struct _GnomeFontFaceGT1	GnomeFontFaceGT1;
typedef struct _GnomeFontFaceGT1Class	GnomeFontFaceGT1Class;
typedef struct _GnomeFontFaceGT1Private	GnomeFontFaceGT1Private;

struct _GnomeFontFaceGT1 {
	GnomeFontFace face;
	GnomeFontFaceGT1Private * private;
};

struct _GnomeFontFaceGT1Class {
	GnomeFontFaceClass parent_class;
};

GtkType gnome_font_face_gt1_get_type (void);

#endif
