#ifndef GNOME_FONT_GT1_H
#define GNOME_FONT_GT1_H

/*
 * GnomeFontGT1
 *
 * Type1 font
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <libgnomefont/gnome-font.h>

#define GNOME_TYPE_FONT_GT1		(gnome_font_gt1_get_type ())
#define GNOME_FONT_GT1(obj)		(GTK_CHECK_CAST ((obj), GNOME_TYPE_FONT_GT1, GnomeFontGT1))
#define GNOME_FONT_GT1_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_FONT_GT1, GnomeFontGT1Class))
#define GNOME_IS_FONT_GT1(obj)	(GTK_CHECK_TYPE ((obj), GNOME_TYPE_FONT_GT1))
#define GNOME_IS_FONT_GT1_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_FONT_GT1))

typedef struct _GnomeFontGT1		GnomeFontGT1;
typedef struct _GnomeFontGT1Class	GnomeFontGT1Class;
typedef struct _GnomeFontGT1Private	GnomeFontGT1Private;

struct _GnomeFontGT1 {
	GnomeFont font;
	GnomeFontGT1Private * private;
};

struct _GnomeFontGT1Class {
	GnomeFontClass parent_class;
};

GtkType gnome_font_gt1_get_type (void);

#endif
