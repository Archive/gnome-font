#ifndef GNOME_FONT_GT1_PRIVATE_H
#define GNOME_FONT_GT1_PRIVATE_H

#include <libgnomefont/gnome-font-face.h>

struct _GnomeFontGT1Private {

	GSList * rfonts;

	Gt1LoadedFont * gt1_font;

	/*
	 * PS to base matrix
	 */

	gdouble transform[6];
};

GnomeFont * gnome_font_gt1_new (GnomeFontFace * face, gdouble size, gdouble transform[]);

#endif
