#define GNOME_FONT_CONFIG_C

#include <config.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <libgnomefont/ft2/gf-freetype2.h>
#include <libgnomefont/gnome-font-config.h>

/* This is strictly private */

typedef enum {
	GF_FILE_TYPE_NONE,
	GF_FILE_TYPE_AFM_1_0,
	GF_FILE_TYPE_AFM_2_0,
	GF_FILE_TYPE_1,
	GF_FILE_TYPE_TTF
} GFFileType;

static GFFileType gf_get_file_type (const gchar * path);

void
gnome_font_config_try_file (const gchar * path)
{
	GFFileType type;

	g_return_if_fail (path != NULL);

	type = gf_get_file_type (path);

	switch (type) {
	case GF_FILE_TYPE_AFM_1_0:
	case GF_FILE_TYPE_AFM_2_0:
		g_print ("%s is afm file\n", path);
		break;
	case GF_FILE_TYPE_1:
		g_print ("%s is type1 file\n", path);
		break;
	case GF_FILE_TYPE_TTF:
		g_print ("%s is TrueType file\n", path);
		break;
	case GF_FILE_TYPE_NONE:
		g_print ("%s is not any of supported font file types\n", path);
		break;
	default:
		g_assert_not_reached ();
		break;
	}
}

/* Helpers */

static GFFileType
gf_get_file_type (const gchar * path)
{
	int fh, l;
	char b[32];

	fh = open (path, O_RDONLY);
	if (fh < 0) {
		return GF_FILE_TYPE_NONE;
	}

	l = read (fh, b, 32);
	if (l != 32) {
		close (fh);
		return GF_FILE_TYPE_NONE;
	}

	if (strncmp (b, "StartFontMetrics", 16) == 0) {
		/* We are afm file */
		close (fh);
		if (strncmp (b + 17, "1.0", 3) == 0)
			return GF_FILE_TYPE_AFM_1_0;
		if (strncmp (b + 17, "2.0", 3) == 0)
			return GF_FILE_TYPE_AFM_2_0;
		return GF_FILE_TYPE_NONE;
	}

	close (fh);

	return GF_FILE_TYPE_NONE;
}

