#ifndef _GNOME_FONT_GLYPH_PRIVATE_H_
#define _GNOME_FONT_GLYPH_PRIVATE_H_

/*
 * GnomeFontGlyph
 *
 * Abstract base class for separate glyphs
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include "gnome-font-glyph.h"

#define GNOME_TYPE_FONT_GLYPH		(gnome_font_glyph_get_type ())
#define GNOME_FONT_GLYPH(obj)		(GTK_CHECK_CAST ((obj), GNOME_TYPE_FONT_GLYPH, GnomeFontGlyph))
#define GNOME_FONT_GLYPH_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_FONT_GLYPH, GnomeFontGlyphClass))
#define GNOME_IS_FONT_GLYPH(obj)		(GTK_CHECK_TYPE ((obj), GNOME_TYPE_FONT_GLYPH))
#define GNOME_IS_FONT_GLYPH_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_FONT_GLYPH))

typedef struct _GnomeFontGlyphClass	GnomeFontGlyphClass;

struct _GnomeFontGlyph {
	GtkObject object;

	/* Our owner */

	GnomeVFont * vfont;
	gint code;
};

struct _GnomeFontGlyphClass {
	GtkObjectClass parent_class;

	const ArtBpath * (* bpath) (GnomeFontGlyph * glyph);

	/* fixme: This will be changed */

	void (* metrics) (GnomeFontGlyph * glyph, ArtDRect * bbox, ArtPoint * advance);

	const GFGGrayMap * (* graymap) (GnomeFontGlyph * glyph);

	const GFGGdkMap * (* gdkmap) (GnomeFontGlyph * glyph);
};

GtkType gnome_font_glyph_get_type (void);

/*
 * NB! These are meant to be used solely by VFont implementations!
 */

void gnome_font_glyph_ref (GnomeFontGlyph * glyph);
void gnome_font_glyph_unref (GnomeFontGlyph * glyph);

#endif
