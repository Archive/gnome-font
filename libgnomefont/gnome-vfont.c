#define _GNOME_VFONT_C_

/*
 * GnomeVFont
 *
 * Abstract virtual base class for all glyph collection types
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <locale.h>
#include "gnome-font-glyph-private.h"
#include "gnome-vfont-private.h"

#define VFONT_STD_CACHE_SIZE 1024

static void gnome_vfont_class_init (GnomeVFontClass * klass);
static void gnome_vfont_init (GnomeVFont * group);

static void gnome_vfont_destroy (GtkObject * object);

static gboolean gvf_locked_remove (gpointer key, gpointer value, gpointer data);
static gboolean gvf_info (GnomeVFont * vfont);
static GnomeFontGlyph * gvf_glyph (const GnomeVFont * vfont, gint glyphnum);

#define GVF_GET_INFO(v) (((GnomeVFont *)v)->has_info || gvf_info ((GnomeVFont *)v))

static GtkObjectClass * parent_class = NULL;

GtkType
gnome_vfont_get_type (void)
{
	static GtkType group_type = 0;
	if (!group_type) {
		GtkTypeInfo group_info = {
			"GnomeVFont",
			sizeof (GnomeVFont),
			sizeof (GnomeVFontClass),
			(GtkClassInitFunc) gnome_vfont_class_init,
			(GtkObjectInitFunc) gnome_vfont_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};
		group_type = gtk_type_unique (gtk_object_get_type (), &group_info);
	}
	return group_type;
}

static void
gnome_vfont_class_init (GnomeVFontClass * klass)
{
	GtkObjectClass * object_class;

	object_class = (GtkObjectClass *) klass;

	parent_class = gtk_type_class (gtk_object_get_type ());

	object_class->destroy = gnome_vfont_destroy;
}

/*
 * fixme:
 *
 * We currently cache glyphs blindly - i.e. without taking their dimensions into account (size = 1)
 * Usually very big glyps are rarely used, so we should really use bbox area
 *
 */

static void
gnome_vfont_init (GnomeVFont * vfont)
{
	vfont->has_info = FALSE;

	vfont->glyphs = e_cache_new (NULL, /* key_hash */
				     NULL, /* key_compare */
				     NULL, /* key_dup */
				     NULL, /* key_free */
				     (ECacheFreeFunc) gnome_font_glyph_unref, /* object_free */
				     VFONT_STD_CACHE_SIZE, /* soft_limit */
				     VFONT_STD_CACHE_SIZE); /* hard_limit */
	vfont->locked = g_hash_table_new (NULL, NULL);
}

static void
gnome_vfont_destroy (GtkObject * object)
{
	GnomeVFont * vfont;

	vfont = (GnomeVFont *) object;

	if (vfont->glyphs) {
		e_cache_unref (vfont->glyphs);
		vfont->glyphs = NULL;
	}

	if (vfont->locked) {
		g_hash_table_foreach_remove (vfont->locked, gvf_locked_remove, NULL);
	}

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

/*
 * Methods
 *
 * 0. VFont only methods
 * 1. Get glyph properties, glyph state will remain unknown
 * 2. Get glyph properties, glyph will remain locked
 * 3. Reference glyph
 */

const ArtDRect *
gnome_vfont_get_stdbbox (const GnomeVFont * vfont)
{
	g_return_val_if_fail (vfont != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_VFONT (vfont), NULL);
	g_return_val_if_fail (GVF_GET_INFO (vfont), NULL);

	return &vfont->bbox;
}

ArtPoint *
gnome_vfont_get_glyph_stdadvance (const GnomeVFont * vfont, gint glyphnum, ArtPoint * advance)
{
	GnomeFontGlyph * glyph;

	g_return_val_if_fail (vfont != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_VFONT (vfont), NULL);
	g_return_val_if_fail (glyphnum >= 0, NULL);
	g_return_val_if_fail (advance != NULL, NULL);

	g_return_val_if_fail (GVF_GET_INFO (vfont), NULL);
	g_return_val_if_fail (glyphnum < vfont->num_glyphs, NULL);

	/* Look into locked glyphs */

	glyph = g_hash_table_lookup (vfont->locked, GINT_TO_POINTER (glyphnum));

	if (glyph) {
		return gnome_font_glyph_get_stdadvance (glyph, advance);
	}

	/* Look into cache */

	glyph = e_cache_lookup (vfont->glyphs, GINT_TO_POINTER (glyphnum));

	if (glyph) {
		return gnome_font_glyph_get_stdadvance (glyph, advance);
	}

	/* Try to fetch glyph */

	glyph = gvf_glyph (vfont, glyphnum);

	g_return_val_if_fail (glyph != NULL, NULL);

	advance = gnome_font_glyph_get_stdadvance (glyph, advance);

	if (!e_cache_insert (vfont->glyphs, GINT_TO_POINTER (glyphnum), glyph, 1)) {
		gnome_font_glyph_unref (glyph);
	}

	return advance;
}

ArtDRect *
gnome_vfont_get_glyph_stdbbox (const GnomeVFont * vfont, gint glyphnum, ArtDRect * bbox)
{
	GnomeFontGlyph * glyph;

	g_return_val_if_fail (vfont != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_VFONT (vfont), NULL);
	g_return_val_if_fail (glyphnum >= 0, NULL);
	g_return_val_if_fail (bbox != NULL, NULL);

	g_return_val_if_fail (GVF_GET_INFO (vfont), NULL);
	g_return_val_if_fail (glyphnum < vfont->num_glyphs, NULL);

	/* Look into locked glyphs */

	glyph = g_hash_table_lookup (vfont->locked, GINT_TO_POINTER (glyphnum));

	if (glyph) {
		return gnome_font_glyph_get_stdbbox (glyph, bbox);
	}

	/* Look into cache */

	glyph = e_cache_lookup (vfont->glyphs, GINT_TO_POINTER (glyphnum));

	if (glyph) {
		return gnome_font_glyph_get_stdbbox (glyph, bbox);
	}

	/* Try to fetch glyph */

	glyph = gvf_glyph (vfont, glyphnum);

	g_return_val_if_fail (glyph != NULL, NULL);

	bbox = gnome_font_glyph_get_stdbbox (glyph, bbox);

	if (!e_cache_insert (vfont->glyphs, GINT_TO_POINTER (glyphnum), glyph, 1)) {
		gnome_font_glyph_unref (glyph);
	}

	return bbox;
}

const GnomeFontGlyph *
gnome_vfont_get_glyph (const GnomeVFont * vfont, gint glyphnum)
{
	GnomeFontGlyph * glyph;

	g_return_val_if_fail (vfont != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_VFONT (vfont), NULL);
	g_return_val_if_fail (glyphnum >= 0, NULL);

	g_return_val_if_fail (GVF_GET_INFO (vfont), NULL);
	g_return_val_if_fail (glyphnum < vfont->num_glyphs, NULL);

	/* Look into locked glyphs */

	glyph = g_hash_table_lookup (vfont->locked, GINT_TO_POINTER (glyphnum));

	if (glyph) return glyph;

	/* Look into cache */

	glyph = e_cache_lookup (vfont->glyphs, GINT_TO_POINTER (glyphnum));

	if (glyph) {
		gnome_font_glyph_ref (glyph);
	} else {
		glyph = gvf_glyph (vfont, glyphnum);
		g_return_val_if_fail (glyph != NULL, NULL);
		if (e_cache_insert (vfont->glyphs, GINT_TO_POINTER (glyphnum), glyph, 1)) {
			gnome_font_glyph_ref (glyph);
		}
	}

	/* We have glyph with exactly one free reference */

	g_hash_table_insert (vfont->locked, GINT_TO_POINTER (glyphnum), glyph);

	return glyph;
}

const ArtBpath *
gnome_vfont_get_glyph_stdoutline (const GnomeVFont * vfont, gint glyphnum)
{
	GnomeFontGlyph * glyph;

	g_return_val_if_fail (vfont != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_VFONT (vfont), NULL);
	g_return_val_if_fail (glyphnum >= 0, NULL);

	g_return_val_if_fail (GVF_GET_INFO (vfont), NULL);
	g_return_val_if_fail (glyphnum < vfont->num_glyphs, NULL);

	/* Look into locked glyphs */

	glyph = g_hash_table_lookup (vfont->locked, GINT_TO_POINTER (glyphnum));

	if (glyph) {
		return gnome_font_glyph_get_stdoutline (glyph);
	}

	/* Look into cache */

	glyph = e_cache_lookup (vfont->glyphs, GINT_TO_POINTER (glyphnum));

	if (glyph) {
		gnome_font_glyph_ref (glyph);
	} else {
		glyph = gvf_glyph (vfont, glyphnum);
		g_return_val_if_fail (glyph != NULL, NULL);
		if (e_cache_insert (vfont->glyphs, GINT_TO_POINTER (glyphnum), glyph, 1)) {
			gnome_font_glyph_ref (glyph);
		}
	}

	/* We have glyph with exactly one free reference */

	g_hash_table_insert (vfont->locked, GINT_TO_POINTER (glyphnum), glyph);

	return gnome_font_glyph_get_stdoutline (glyph);
}

/*
 * Locking multiple times == single locking
 */

void
gnome_vfont_glyph_lock (const GnomeVFont * vfont, gint glyphnum)
{
	GnomeFontGlyph * glyph;

	g_return_if_fail (vfont != NULL);
	g_return_if_fail (GNOME_IS_VFONT (vfont));
	g_return_if_fail (glyphnum >= 0);

	g_return_if_fail (GVF_GET_INFO (vfont));
	g_return_if_fail (glyphnum < vfont->num_glyphs);

	/* Look into locked glyphs */

	glyph = g_hash_table_lookup (vfont->locked, GINT_TO_POINTER (glyphnum));

	if (glyph) return;

	/* Look into cache */

	glyph = e_cache_lookup (vfont->glyphs, GINT_TO_POINTER (glyphnum));

	if (glyph) {
		gnome_font_glyph_ref (glyph);
	} else {
		glyph = gvf_glyph (vfont, glyphnum);
		g_return_if_fail (glyph != NULL);
		if (e_cache_insert (vfont->glyphs, GINT_TO_POINTER (glyphnum), glyph, 1)) {
			gnome_font_glyph_ref (glyph);
		}
	}

	/* We have glyph with exactly one free reference */

	g_hash_table_insert (vfont->locked, GINT_TO_POINTER (glyphnum), glyph);
}

/*
 * Unlocking multiple times == single unlock
 */

void
gnome_vfont_glyph_unlock (const GnomeVFont * vfont, gint glyphnum)
{
	GnomeFontGlyph * glyph;

	g_return_if_fail (vfont != NULL);
	g_return_if_fail (GNOME_IS_VFONT (vfont));
	g_return_if_fail (glyphnum >= 0);

	g_return_if_fail (GVF_GET_INFO (vfont));
	g_return_if_fail (glyphnum < vfont->num_glyphs);

	/* Look into locked glyphs */

	glyph = g_hash_table_lookup (vfont->locked, GINT_TO_POINTER (glyphnum));

	if (!glyph) return;

	g_hash_table_remove (vfont->locked, GINT_TO_POINTER (glyphnum));

	gnome_font_glyph_unref (glyph);
}

/*
 * Releases locked glyph
 */

static gboolean
gvf_locked_remove (gpointer key, gpointer value, gpointer data)
{
	gnome_font_glyph_unref (GNOME_FONT_GLYPH (value));

	return TRUE;
}

static gboolean
gvf_info (GnomeVFont * vfont)
{
	if (GNOME_VFONT_CLASS (GTK_OBJECT (vfont)->klass)->info) {
		vfont->has_info = (* GNOME_VFONT_CLASS (GTK_OBJECT (vfont)->klass)->info) (vfont);
	} else {
		g_warning ("VFont does not implement ::info() method");
	}

	return vfont->has_info;
}

/*
 * Invokes VFont ::glyph() method
 */

static GnomeFontGlyph *
gvf_glyph (const GnomeVFont * vfont, gint glyphnum)
{
	if (GNOME_VFONT_CLASS (GTK_OBJECT (vfont)->klass)->glyph) {
		return (* GNOME_VFONT_CLASS (GTK_OBJECT (vfont)->klass)->glyph) ((GnomeVFont *) vfont, glyphnum);
	} else {
		g_warning ("VFont does not implement ::glyph() method");
	}

	return NULL;
}


